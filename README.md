Idealogs is a crowdsourcing project for understanding complex, controversial topics. It is built in Django. You can find more info [here](https://tyfried.github.io/portfolio/idealogs), and some basic documentation [here](https://www.idealogs.org/@0x1).

## Development Server

1. Install [vagrant](https://www.vagrantup.com/downloads.html) and a virtual machine backend.  I use [VirtualBox](https://www.virtualbox.org/wiki/Downloads).  If you are new to vagrant, see this [tutorial](https://www.vagrantup.com/intro/getting-started/index.html).
2. Clone the repository.

```bash
git clone https://github.com/tyfried/idealogs.git
cd idealogs
```

3. Install and provision the virtual machine (this will take a few minutes).
```bash
vagrant up
```

4. Open the virtual machine.

```bash
vagrant ssh
```

The development server should start automatically; if not, run

```bash
python manage.py runserver 0.0.0.0:8000
```
