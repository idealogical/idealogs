var KeyHelper = libsignal.KeyHelper

var address = new libsignal.SignalProtocolAddress("xxx",1);
var store = new SignalProtocolStore()
var keyId = 1337;

var registrationId = KeyHelper.generateRegistrationId();
store.put('registrationId',registrationId);

KeyHelper.generatePreKey(keyId).then(function(preKey){
    store.storePreKey(preKey.keyId,preKey.keyPair)
});

KeyHelper.generateIdentityKeyPair().then(function(identityKeyPair){
    store.put('identityKey',identityKeyPair);
}).then(function(){
    return KeyHelper.generateSignedPreKey(store.getIdentityKeyPair(),keyId)
}).then(function(signedPreKey){
  store.storeSignedPreKey(signedPreKey.keyId,signedPreKey.keyPair);
}).then(function(){
  identity = store.getIdentityKeyPair()
  registration = store.getLocalRegistrationId()
  return {

  }
});
