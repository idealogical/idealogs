// var h1 = document.getElementsByClassName("title")[0];
// var button = document.createElement('button');
// button.innerText = "Publish"
// button.setAttribute("type","submit")
// button.setAttribute("onclick","return confirm('Are you sure you are ready to publish? By publishing on our platform you agree to our Terms of Service (TOS). In a nutshell: this document cannot be edited further after performing this action. See the TOS link in the header for more details.');")
// button.setAttribute("style","margin-bottom:1rem")
// h1.parentNode.insertBefore(button,h1)
// h1.innerHTML = document.getElementsByClassName("title")[0].innerText+" [<strong>PREVIEW</strong>]"


var h1 = document.getElementsByClassName("title")[0];
var button1 = document.createElement('button');
var button2 = document.createElement('button');
var div = document.createElement('div');

div.setAttribute('class','preview-button')
button1.innerText = "Publish"
button2.innerText = "Continue editing"
button1.setAttribute("type","submit")
button2.setAttribute("type","submit")
button1.setAttribute("name","yes")
button2.setAttribute("name","no")
button2.setAttribute("style","margin-right:.25rem")
button1.setAttribute("onclick","return confirm('Are you sure you are ready to publish? By publishing on our platform you agree to our Terms of Service (TOS). In a nutshell: this document cannot be edited further or unpublished after performing this action.');")
// button2.setAttribute("style","margin-bottom:1rem")

div.appendChild(button2)
div.appendChild(button1)
h1.parentNode.insertBefore(div,h1)
// h1.parentNode.insertBefore(button2,h1)
h1.innerHTML = document.getElementsByClassName("title")[0].innerText+" [<strong>PREVIEW</strong>]"
