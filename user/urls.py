from django.conf.urls import url,include
from django.contrib.auth import views as auth_views
from . import views
from django.contrib.auth.decorators import login_required
from user.models import Domain

app_name = "user"

urlpatterns = [

    url(r'^@Idealogs$',views.IdealogsHome.as_view(),name="idealogs-home"),
    url(r'^@(?P<id>'+Domain.DOMAIN+')$',views.DomainDetail.as_view(),name="domain"),
    url(r'^@(?P<id>'+Domain.DOMAIN+')/works$',views.DomainWorks.as_view(),name="works"),
    url(r'^@(?P<id>'+Domain.DOMAIN+')/contributors$',views.Contributors.as_view(),name="contributors"),
    url(r'^@(?P<id>'+Domain.DOMAIN+')/contributors/new$',login_required(views.NewContributor.as_view()),name="new-contributor"),
    url(r'^@(?P<id>'+Domain.DOMAIN+')/watch$',login_required(views.Watch.as_view()), name='watch'),
    url(r'^signup$', views.Signup.as_view(), name='signup'),
    url(r'^login$', views.Login.as_view(), name='login'),
    url(r'^@(?P<id>'+Domain.DOMAIN+')/panel$',login_required(views.Panel.as_view()),name='panel'),
    url(r'^panel$',views.PanelRedirectView.as_view(),name='panel-redirect'),
    url(r'^panel/timezone$', login_required(views.Timezone.as_view()), name='timezone'),
    url(r'^panel/hints$', login_required(views.Hints.as_view()), name='hints'),
    url(r'^panel/watching$',login_required(views.Watching.as_view()),name="watching"),
    url(r'^@(?P<id>'+Domain.DOMAIN+')/panel/bio$',login_required(views.Bio.as_view()), name='bio'),
    url(r'^workspace$',login_required(views.Workspace.as_view()), name='workspace'),
    url(r'^workspace/new$',login_required(views.NewWork.as_view()), name='new-work'),
    url(r'^domains$',login_required(views.Domains.as_view()), name='domains'),
    url(r'^domains/new$',login_required(views.NewDomain.as_view()), name='new-domain'),
    url(r'^notifications$',login_required(views.Nots.as_view()),name="nots"),
    url(r'^anon/(?P<ip>.+)$',views.AnonDetail.as_view(),name='anon'), #TODO improve regex for <ip>

    url(r'^panel/password_change/$',views.PasswordChangeView.as_view(),name="password_change"),
    url(r'^panel/password_change/done/$',views.PasswordChangeDoneView.as_view(),name="password_change_done")
]
