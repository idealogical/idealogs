from django.test import RequestFactory, TestCase#, TransactionTestCase
from django.contrib.auth.models import AnonymousUser, User
from idealogs.models import Article
from user.models import Domain
import pdb

class DomainTestCase(TestCase):

    def setUp(self):
        #domains
        self.mains = []
        for i in [Domain.DEF,Domain.TNS,Domain.INF,Domain.FIN]:
            c = Domain(id=i)
            c.save()
            self.mains.append(c)

        self.d1 = Domain.new('test')
        self.d2 = Domain.new('The Ender Times')

    def test_range(self):
        d = Domain.objects.get(id=Domain.DEF)
        self.assertEqual(d.range(),0)
        new_range = d.range()
        d.range(new_range)
        self.assertEqual(d.range(),1)
        self.assertEqual(d.next,2)

    def test_str(self):
        self.assertEqual(str(self.d1),'test')
        self.assertEqual(str(self.d2),'The.Ender.Times')

    def test_is_main(self):
        self.assertEqual(self.d1.is_main(),False)
        for m in self.mains:
            self.assertEqual(m.is_main(),True)

    def test_is_tns(self):
        self.assertEqual(self.mains[1].is_tns(),True)
        self.assertEqual(self.mains[0].is_tns(),False)

    def test_is_def(self):
        self.assertEqual(self.mains[0].is_def(),True)
        self.assertEqual(self.mains[1].is_def(),False)
