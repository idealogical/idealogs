from django.contrib import admin
from user.models import User, Domain, AnonEditor

admin.site.register(User)
admin.site.register(Domain)
admin.site.register(AnonEditor)
