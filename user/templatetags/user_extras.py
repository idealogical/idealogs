from django import template
from django.core.exceptions import ValidationError
import math
import pdb

register = template.Library()

@register.filter(is_safe=True)
def contribs(user,jrnl):
    return user.contrib_set.filter(jrnl=jrnl,resigned=None)

@register.simple_tag(takes_context=True)
def UTC(context,time):
    if context['UTC']:
        return time+" (UTC)"
    else:
        return time
