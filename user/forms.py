from django import forms
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth import get_user_model
from django.db import IntegrityError, transaction
from django.core.exceptions import ValidationError
from django.core.urlresolvers import reverse
from django.utils import timezone
from lib.utils import Object
from lib.tifdoc import Tifdoc
from lib.mixins import CreatorsFormMixin, NewCreatorFormMixin
from idealogs.models import Article, Commit
from user.models import Domain,Creator
from timezone_field import TimeZoneFormField
import bleach
import re
import pdb

class DomainMixin(forms.Form):
    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop('request', None)
        self.object = kwargs.pop('object', None)
        super().__init__(*args, **kwargs)

    def clean(self):
        cleaned_data = super().clean()
        for i in range(1,6):
            if "action"+str(i) in self.data:
                cleaned_data['action'] = i
                break
        return cleaned_data

class BioForm(DomainMixin,forms.Form):
    handle = forms.CharField(required=False)

    def clean(self):
        cleaned_data = super().clean()
        if not cleaned_data['handle']:
            self.object.bio = None
            self.object.save()
            return cleaned_data
        match = re.match(r'(?i)@([A-Za-z0-9\.]+)x([a-fA-F0-9]+)',cleaned_data['handle'])
        if not match:
            raise ValidationError('not a valid handle')
        elif match[1] != str(self.object):
            raise ValidationError('must be an article from @'+str(self.object))
        try:
            article = self.object.article_set.get(range=int(match[2],16))
        except:
            raise ValidationError('could not find that article')

        if article.status == Article.PRIVATE:
            raise ValidationError('that article is private')
        else:
            self.object.bio = article
            self.object.save()
        return cleaned_data

class ContributorsForm(CreatorsFormMixin,DomainMixin,forms.Form):

    def clean(self):
        cleaned_data = super().clean()
        cleaned_data['success_url'] = self.object.get_contributors_url()
        return cleaned_data

class NewCreatorForm(NewCreatorFormMixin,DomainMixin,forms.Form):
    handle = forms.CharField(label="Handle",widget=forms.TextInput(attrs={'size':40}))

    def clean(self):
        cleaned_data = super().clean()
        if self.object.creators.filter(user=cleaned_data['invitee'].user).exists():
            raise ValidationError("that user is already a member")
        creator, created = Creator.objects.get_or_create(user=cleaned_data['invitee'].user,role=Creator.INVITED)
        self.object.creators.add(creator)
        cleaned_data['success_url'] = self.object.get_contributors_url()
        cleaned_data['recipient'] = creator.user
        return cleaned_data

class NewDomainForm(DomainMixin,forms.Form):
    title = forms.CharField(widget=forms.TextInput(attrs={'style':'width:70%;'}))

    def clean_title(self):
        return bleach.clean(self.cleaned_data['title'])

    def clean(self):
        cleaned_data = super().clean()
        title = cleaned_data['title'].strip()
        Domain.validate(title)
        if not re.match(r'^The ',title):
            raise ValidationError("Titles must begin with 'The '")
        domain = Domain.new(title)
        creator, created = Creator.objects.get_or_create(user=self.request.user,role=Creator.OWNER)
        domain.creators.add(creator)
        cleaned_data['success_url'] = reverse('user:domains')
        return cleaned_data

class NewWorkForm(DomainMixin,forms.Form):
    domain = forms.ChoiceField()
    status = forms.ChoiceField(label="Visibility",choices=Article.Choices,)
    input = forms.CharField(label="Title",widget=forms.TextInput(attrs={'style':'width:70%;'}))

    def __init__(self,*args,**kwargs):
        queryset = kwargs.pop('queryset')
        super().__init__(*args,**kwargs)
        self.fields['domain'].choices = queryset

    def clean_input(self):
        return bleach.clean(self.cleaned_data['input'])

    def clean(self):
        cleaned_data = super().clean()
        domain = Domain.objects.get(id=cleaned_data['domain'])
        status = int(cleaned_data['status'])
        assert status in range(Article.PRIVATE,Article.OPEN+1)

        with transaction.atomic():
            object = Article(domain=domain,
                                        range=domain.range(),
                                        status=status,
                                        header={'title': cleaned_data['input']})
            object.initialize(doc_id=Tifdoc.DFT,
                                page=Commit.WORK,
                                request=self.request)
            object.initialize(doc_id=Tifdoc.TALK,
                                page=Commit.TALK,
                                request=self.request)
        cleaned_data['success_url'] = object.get_absolute_url()
        return cleaned_data

class AuthForm(AuthenticationForm):

    def clean_username(self):
        return self.cleaned_data['username'].lower()

class SignupForm(forms.ModelForm):
    error_messages = {
        'password_mismatch': "The two password fields didn't match.",
    }
    password1 = forms.CharField(label="Password",
        widget=forms.PasswordInput)
    password2 = forms.CharField(label="Password confirmation",
        widget=forms.PasswordInput,
        help_text="<br/><small>Enter the same password as above, for verification.</small>")
    pseudonym = forms.CharField(label="Pen name",
        help_text='<br/><small>No numbers or special characters; spaces are fine.</small>')

    class Meta:
        fields = ("email","pseudonym","password1","password2","timezone")
        model = get_user_model()
        labels = {
            "pseudonym": "Pen name"
        }

    def clean_email(self):
        return self.cleaned_data.get('email').lower()

    def clean_password2(self):
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError(
                self.error_messages['password_mismatch'],
                code='password_mismatch',
            )
        return password2

    def clean_pseudonym(self):
            pseudonym = bleach.clean(self.cleaned_data.get("pseudonym"))
            pseudonym = pseudonym.strip()
            Domain.validate(pseudonym)
            if re.match(r'^The ',pseudonym):
                raise ValidationError("pen name cannot begin with 'The '")
            elif not re.match(r'^([A-Za-z]{1}[a-z]*(\s|$))+$',pseudonym):
                raise ValidationError('must follow standard capitalization rules for names')
            return pseudonym

    def save(self, commit=True):
        user = super().save(commit=False)
        user.set_password(self.cleaned_data["password1"])
        if commit:
            user.save()
            user.initialize(self.cleaned_data["pseudonym"])
        return user
