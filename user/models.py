from django.db import models
from django.contrib.auth.models import BaseUserManager, PermissionsMixin, AbstractBaseUser
from django.conf import settings
from django.db.models import Q
from django.db import IntegrityError, transaction
from django.core.urlresolvers import reverse
from django.core.validators import RegexValidator
from django.core.exceptions import ValidationError
from django.core.mail import send_mail
from django.contrib.postgres.fields import ArrayField
from django.utils.translation import ugettext_lazy as _
from django.utils.safestring import mark_safe
from django.template.loader import get_template
from timezone_field import TimeZoneField
from datetime import datetime
from django.core.mail import EmailMessage
from urllib.parse import urlparse,unquote
from sortedcontainers import SortedList
from functools import reduce
from . import search
import operator
import hashlib
import re
import pdb

class AnonEditor(models.Model):
    ip = models.CharField(max_length=45,null=True,unique=True)
    is_blocked = models.BooleanField(default=False)

    def __str__(self):
        if self.ip:
            return self.ip
        else:
            return "Anonymous"

    def get_absolute_url(self):
        return reverse("user:anon",kwargs={"ip": self.ip})

class Domain(models.Model):
    DEF = '0'
    TNS = 'T'
    INF = 'I'
    FIN = 'F'
    MAIN = [DEF,TNS,INF,FIN]

    DOMAIN = r'[A-Za-z0-9\.]+'
    DOMAIN_INDEX = r'('+DOMAIN+')x([a-fA-F0-9]+)'

    id = models.CharField(max_length=32,default=DEF,primary_key=True,)
    stops = ArrayField(models.IntegerField(),default=list)
    creators = models.ManyToManyField('Creator')
    watchers = models.ManyToManyField(settings.AUTH_USER_MODEL,related_name="domains_watching")

    next = models.IntegerField(default=0)
    spares = ArrayField(models.IntegerField(),null=True)
    created = models.DateTimeField(auto_now_add=True)
    bio = models.OneToOneField('idealogs.Article',null=True,related_name="bio")

    def __str__(self):
        id = self.id
        for s in self.stops:
            id = id[:s]+'.'+id[s:]
        return id

    def str2(self):
        if self.is_main():
            if self.id == Domain.DEF:
                return mark_safe("@Definite<span style=\'font-size:1.2rem;\'> (<a href=\"#\">?</a>)</span>")
            elif self.id == Domain.TNS:
                return mark_safe("@Transfinite<span style=\'font-size:1.2rem;\'> (<a href=\"#\">?</a>)</span>")
            elif self.id == Domain.INF:
                return mark_safe("@Infinite<span style=\'font-size:1.2rem;\'> (<a href=\"#\">?</a>)</span>")
            elif self.id == Domain.FIN:
                return mark_safe("@Finite<span style=\'font-size:1.2rem;\'> (<a href=\"#\">?</a>)</span>")
        else:
            return "@"+str(self)

    def to_search(self):
        return search.Domain(meta={'id': self.id},
                                id=str(self),
                                created=self.created,)

    def new(input):
        id = input.replace(' ','')
        stops = [stop.start() for stop in re.finditer(' ',input)]
        domain = Domain(id=id,stops=stops)
        domain.save()
        return domain

    def validate(input):
        if not re.match(r'^[A-Z]{1}.*$',input):
            raise ValidationError('first character of pen name must be capitalized')
        elif len(input.replace(' ','')) < 3:
            raise ValidationError('must contain at least three non-whitespace characters')
        elif not re.match(r'^[A-Z\s]+[a-z][A-za-z\s]*$',input):
            raise ValidationError('must contain at least one lower space character')
        elif re.match(Domain.DOMAIN_INDEX,input):
            raise ValidationError('pen name cannot be formatted like an IDea (e.g. ExamplexAF23)')
        elif Domain.objects.filter(id__iexact=input.replace(' ','')).exists():
            raise ValidationError("that name is either taken or reserved")

    def is_main(self):
        return self.id in [Domain.DEF,Domain.TNS,Domain.INF,Domain.FIN]

    def is_tns(self):
        return self.id == Domain.TNS

    def is_def(self):
        return self.id == Domain.DEF

    def is_user(self):
        return hasattr(self,'user')
        # return self.creators.filter(role=Creator.SELF).exists()

    def is_owner(self,user):
        return self.creators.filter(user=user,role__in=[Creator.SELF,Creator.OWNER]).exists()

    def get_owner(self):
        if self.is_user():
            return self.user
        else:
            return self.creators.get(role=Creator.OWNER).user

    def get_objects(self,user):
        return self.article_set.filter(domain=self,creators__user=user,status__gt=0)#Article.PRIVATE

    def get_short_name(self):
        return re.sub('[a-z]','',self.str2())

    def get_absolute_url(self):
        return reverse("user:domain",kwargs={'id':str(self)})

    def get_notification_url(self):
        return self.get_contributors_url()

    def get_contributors_url(self):
        return reverse("user:contributors",kwargs={'id':str(self)})

    def get_works_url(self):
        return reverse("user:works",kwargs={'id':str(self)})

    def get_bio_url(self):
        return reverse("user:bio",kwargs={'id':str(self)})

    def get_new_contributor_url(self):
        return reverse("user:new-contributor",kwargs={'id':str(self)})

    def get_watch_url(self):
        return reverse("user:watch",kwargs={'id':str(self)})

    def get_panel_url(self):
        return reverse("user:panel",kwargs={'id':str(self)})

    def get_timezone_url(self):
        return reverse("user:timezone")

    def get_hints_url(self):
        return reverse("user:hints")

    def translate(x):
        if x == ',':
            return Domain.TNS
        elif x == '.':
            return Domain.INF
        elif x == '?':
            return Domain.FIN
        elif x == ";":
            return Domain.DEF

    def reverse_translate(x):
        if x == Domain.TNS:
            return ','
        elif x == Domain.INF:
            return '.'
        elif x == Domain.FIN:
            return '?'
        elif x == Domain.DEF:
            return ';'

    def translate2(x):
        if x == Domain.TNS:
            return '<strong>writing</strong> (e.g. essay, article, op-ed, book, etc.)'
        elif x == Domain.INF:
            return '<strong>statement</strong>'
        elif x == Domain.FIN:
            return '<strong>question</strong>'
        elif x == Domain.DEF:
            return '<strong>subject</strong> (e.g. World War 2, Climate Change, Ann Arbor, Alexander Hamilton, etc.)'

    def translate3(x):
        if x == Domain.TNS:
            return 'a transfinite (<strong>@Tx...</strong>)'
        elif x == Domain.INF:
            return 'an infinite (<strong>@Ix...</strong>)'
        elif x == Domain.FIN:
            return 'a finite (<strong>@Fx...</strong>)'
        elif x == Domain.DEF:
            return 'a definite (<strong>@0x...</strong>)'

    @transaction.atomic
    def range(self,range=None):
        if range is None:
            if self.spares:
                sl = SortedList(self.spares)
                range = sl.pop(0)
                assert range < self.next
                self.spares = list(iter(sl))
            else:
                range = self.next
                self.next += 1
            self.save()
            return range
        else:
            assert range < self.next
            sl = SortedList(self.spares)
            sl.add(range)
            self.spares = list(iter(sl))
            self.save()

class MyUserManager(BaseUserManager):
    use_in_migrations = True

    """
    A custom user manager to deal with emails as unique identifiers for auth
    instead of usernames. The default that's used is "UserManager"
    """
    def _create_user(self, email, pseudonym, password, **extra_fields):
        """
        Creates and saves a User with the given email and password.
        """
        if not email:
            raise ValueError('The Email must be set')
        if not pseudonym:
            raise ValueError('The Pseudonym must be set')
        email = self.normalize_email(email)
        user = self.model(email=email, pseudonym=pseudonym, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, email, pseudonym, password=None, **extra_fields):
        extra_fields.setdefault('is_superuser', False)
        return self._create_user(email, pseudonym, password, **extra_fields)

    def create_superuser(self, email, pseudonym, password, **extra_fields):
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)
        extra_fields.setdefault('is_active', True)

        if extra_fields.get('is_staff') is not True:
            raise ValueError('Superuser must have is_staff=True.')
        if extra_fields.get('is_superuser') is not True:
            raise ValueError('Superuser must have is_superuser=True.')
        return self._create_user(email, pseudonym, password, **extra_fields)

class User(AbstractBaseUser, PermissionsMixin):
    domain = models.OneToOneField(Domain,on_delete=models.CASCADE,null=True)
    email = models.EmailField(unique=True,)
    timezone = TimeZoneField(default='US/Eastern')
    hints = models.BooleanField(default=True)
    is_staff = models.BooleanField(
        _('staff status'),
        default=False,
        help_text=_('Designates whether the user can log into this admin site.'),
    )
    is_active = models.BooleanField(
        _('active'),
        default=True,
        help_text=_(
            'Designates whether this user should be treated as active. '
            'Unselect this instead of deleting accounts.'
        ),
    )
    USERNAME_FIELD = 'email'
    objects = MyUserManager()
    REQUIRED_FIELDS = ['pseudonym']

    def __str__(self):
        return "@"+str(self.domain)

    def initialize(self,pseudonym):
        self.domain = Domain.new(pseudonym)
        self.save()
        for role in [Creator.EDITOR,Creator.APPROVER,Creator.WRITER,Creator.SELF]:
            creator = Creator(user=self,role=role)
            creator.save()
        self.domain.creators.add(creator)

    def get_absolute_url(self):
        return self.domain.get_absolute_url()

    def get_full_name(self):
        return str(self)

    def get_short_name(self):
        return str(self)

class Creator(models.Model):
    NONE = 0
    REQUESTER = 1
    READER = 2
    WRITER = 3
    APPROVER = 4
    EDITOR = 5
    INVITED = 6
    MEMBER = 7
    MANAGER = 8
    OWNER = 9
    SELF = 10

    MUTATERS = [REQUESTER,WRITER,APPROVER,EDITOR]
    Choices = ((WRITER,"writer"),(EDITOR,"editor"),)

    Choices1 = ((READER,"reader"),
                (WRITER,"writer"),
                (EDITOR,"editor"),)
    Choices2 = ((APPROVER,"approver"),
                (READER,"reader"),
                (WRITER,"writer"),
                (EDITOR,"editor"),)

    user = models.ForeignKey(settings.AUTH_USER_MODEL,on_delete=models.CASCADE)
    role = models.IntegerField(default=SELF)

    def __str__(self):
        return str(self.user.domain)
