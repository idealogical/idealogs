from django.conf import settings
from .models import Domain
from django.db.models.signals import post_save, pre_delete
from django.dispatch import receiver
from elasticsearch_dsl.connections import connections
import pdb

@receiver(post_save,sender=Domain)
def update_search(sender,instance,**kwargs):
    connections.create_connection(hosts=settings.ELASTICSEARCH_IPS)
    instance.to_search().save()

@receiver(pre_delete,sender=Domain)
def delete_search(sender,instance,**kwargs):
    connections.create_connection(hosts=settings.ELASTICSEARCH_IPS)
    instance.to_search().delete()
