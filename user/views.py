from django.conf import settings
from django.shortcuts import render,redirect,resolve_url
from formtools.preview import FormPreview
from django.views.generic import DetailView,UpdateView,CreateView,ListView,FormView,TemplateView,DeleteView
from django.views.generic.base import RedirectView
from django.views.generic.edit import FormMixin
from django.core.urlresolvers import reverse_lazy,reverse
from django.contrib.auth import login, authenticate, get_user_model, views as auth_views
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.db.models import Max, Case, When, DateTimeField, CharField, Count
from idealogs.models import Article, Commit, Link
from idealogs.forms import WatchForm
from user.models import AnonEditor, Domain, Creator
from user.forms import SignupForm, NewWorkForm, NewDomainForm, NewCreatorForm, ContributorsForm, BioForm, AuthForm
from lib.utils import Object, get_subtitle
from lib.tifdoc import Tifdoc
from lib.mixins import NewMixin, DomainMixin, OwnerMixin, SearchMixin, CacheMixin
from functools import reduce
from itertools import chain
from django.db.models import Q, F
from django.utils import timezone
from elasticsearch import Elasticsearch
from elasticsearch_dsl import Search
from elasticsearch_dsl.query import Q as ES_Q
from notifications.signals import notify
import notifications.views
import operator
import hashlib
import pdb

class NewContributor(DomainMixin,OwnerMixin,NewMixin,FormView):
    form_class = NewCreatorForm

    def get_context_data(self,**kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = "@"+str(self.object)
        context['subtitle'] = get_subtitle([['Home',self.object.get_absolute_url()],['Contributors',self.object.get_contributors_url()],['New']])
        return context

    def get_initial(self):
        initial = super().get_initial()
        initial['handle'] = "@"
        return initial

    def form_valid(self,form):
        valid = super().form_valid(form)
        notify.send(self.request.user,
                    recipient=form.cleaned_data['recipient'],
                    verb="invited you to join",
                    target=self.object,
                    description='contributor '+str(self.object)+' '+str(self.request.user),
                )
        return valid

class NewWork(NewMixin,FormView):
    form_class = NewWorkForm

    def get_context_data(self,**kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = str(self.request.user)
        context['subtitle'] = get_subtitle([['Home',self.request.user.get_absolute_url()],['Panel',self.request.user.domain.get_panel_url()],['Workspace',reverse('user:workspace')],['New']])
        return context

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['queryset'] = [(self.request.user.domain.id, 'self')]
        for domain in Domain.objects.filter(creators__user=self.request.user).exclude(creators__role=Creator.SELF):
            kwargs['queryset'].append((domain.id,str(domain)))
        return kwargs

class NewDomain(NewMixin,FormView):
    form_class = NewDomainForm

    def get_context_data(self,**kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = str(self.request.user)
        context['subtitle'] = get_subtitle([['Home',self.request.user.get_absolute_url()],['Panel',self.request.user.domain.get_panel_url()],['Domains',reverse('user:domains')],['New']])
        return context

class IdealogsHome(CacheMixin,SearchMixin,TemplateView):
    template_name = 'list.html'

    def get_context_data(self,**kwargs):
        es = Elasticsearch(hosts=settings.ELASTICSEARCH_IPS)
        s = Search(using=es,index='article') \
            .filter(~ES_Q("match",domain=Domain.TNS) & ~ES_Q("match",domain=Domain.INF) & ~ES_Q("match",domain=Domain.FIN) & ~ES_Q("match",domain=Domain.DEF)) \
            .filter("range",status={'gte':Article.PUBLISHED}) \
            .sort('-modified')
        if self.q not in [None,""]:
            s = s.query("match",document=self.q).highlight('document',fragment_size=50)
        s = s[self.start:self.end]
        self.response = s.execute()
        template = 'feed/article_es.html'
        context = super().get_context_data(**kwargs)
        context['title'] = "@Idealogs"
        context['placeholder'] = 'Filter'
        context['action'] = reverse("user:idealogs-home")
        context['template'] = template
        return context

class DomainWorks(CacheMixin,DomainMixin,SearchMixin,TemplateView):
    template_name = 'list.html'

    def get_context_data(self,**kwargs):
        es = Elasticsearch(hosts=settings.ELASTICSEARCH_IPS)
        s = Search(using=es,index='article') \
            .filter("match",domain=str(self.object)) \
            .filter("range",status={'gte':Article.PUBLIC}) \
            .sort('-modified')
        if self.q not in [None,""]:
            s = s.query("match",document=self.q).highlight('document',fragment_size=50)
        s = s[self.start:self.end]
        self.response = s.execute()
        template = 'feed/article_es.html'

        context = super().get_context_data(**kwargs)
        context['title'] = self.object.str2()
        context['subtitle'] = get_subtitle([['Home',self.object.get_absolute_url()],['Works']])
        context['placeholder'] = 'Filter'
        context['action'] = self.object.get_works_url()
        context['template'] = template
        context['home'] = True
        return context

class DomainDetail(CacheMixin,DomainMixin,SearchMixin,TemplateView):
    template_name = 'list.html'

    def get_context_data(self,**kwargs):
        es = Elasticsearch(hosts=settings.ELASTICSEARCH_IPS)
        s = Search(using=es,index='commit') \
            .filter("range",committed={'gte':0}) \
            .filter(ES_Q("range",commit_id={'lt':0}) | ES_Q("range",commit_id={'gt':0})) \
            .filter("range",status={'gte':Commit.REQUESTED}) \
            .filter("range",article_status={'gte':Article.PUBLIC}) \
            .sort('-committed')
        if self.object.is_user():
            s = s.filter("match",user=str(self.object))
        else:
            s = s.filter("match",domain=str(self.object))

        if self.q not in [None,""]:
            s = s.query("match",document=self.q).highlight('document',fragment_size=50)
        s = s[self.start:self.end]
        self.response = s.execute()

        context = super().get_context_data(**kwargs)
        context['title'] = self.object.str2()
        if self.object.is_main():
            context['subtitle'] = get_subtitle([['Home']],[['works',self.object.get_works_url()]])
        else:
            if self.request.user.is_authenticated:
                if self.object.is_owner(self.request.user):
                    context['subtitle'] = get_subtitle([['Home']],[['works',self.object.get_works_url()],['contributors',self.object.get_contributors_url()],['panel',self.object.get_panel_url()]])
                else:
                    context['subtitle'] = get_subtitle([['Home']],[['works',self.object.get_works_url()],['contributors',self.object.get_contributors_url()],['watch',self.object.get_watch_url()]])
            else:
                context['subtitle'] = get_subtitle([['Home']],[['works',self.object.get_works_url()],['contributors',self.object.get_contributors_url()]])

        if self.object.is_user() and self.object.bio:
            context['subaction_href'] = self.object.bio.get_absolute_url()
            context['subaction_title'] = str(self.object.bio)
        context['placeholder'] = 'Filter'
        context['action'] = self.object.get_absolute_url()
        context['template'] = 'feed/activity.html'
        context['home'] = True
        return context

class Watch(DomainMixin,FormView):
    template_name =  'form.html'
    form_class = WatchForm

    def get_context_data(self,**kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = "@"+str(self.object)
        context['subtitle'] = get_subtitle([['Home',self.object.get_absolute_url()],['Watch']])
        return context

    def get_initial(self):
        initial = super().get_initial()
        initial['watch'] = self.request.user.domains_watching.filter(id=self.object.id).exists()
        return initial

    def get_success_url(self):
        return self.object.get_absolute_url()

class Timezone(UpdateView):
    model = get_user_model()
    fields = ('timezone',)
    template_name = 'form.html'

    def get_object(self):
        return self.request.user

    def get_context_data(self,**kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = str(self.object)
        context['subtitle'] = get_subtitle([['Home',self.object.domain.get_absolute_url()],['Panel',self.object.domain.get_panel_url()],['Timezone']])
        return context

    def get_success_url(self):
        return self.object.domain.get_panel_url()

class Hints(UpdateView):
    model = get_user_model()
    fields = ('hints',)
    template_name = 'form.html'

    def get_object(self):
        return self.request.user

    def get_context_data(self,**kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = str(self.object)
        context['subtitle'] = get_subtitle([['Home',self.object.domain.get_absolute_url()],['Panel',self.object.domain.get_panel_url()],['Hints']])
        return context

    def get_success_url(self):
        return self.object.domain.get_panel_url()

class Bio(DomainMixin,OwnerMixin,FormView):
    template_name = 'form.html'
    form_class = BioForm

    def get_context_data(self,**kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = '@'+str(self.object)
        context['subtitle'] = get_subtitle([['Home',self.object.get_absolute_url()],['Panel',self.object.get_panel_url()],['Bio']])
        return context

    def get_initial(self):
        initial = super().get_initial()
        if self.object.bio is not None:
            initial['handle'] = "@"+self.object.bio.idea
        else:
            initial['handle'] = "@"+str(self.object)+"x"
        return initial

    def get_success_url(self):
        return self.object.get_panel_url()

class Panel(DomainMixin,OwnerMixin,SearchMixin,TemplateView):
    template_name = 'list.html'

    def get_context_data(self,**kwargs):
        result = [['Bio',
                self.object.get_bio_url(),
                'Set the bio article'],]
        if self.object.is_user():
            result = result + \
                [['Domains',
                    reverse('user:domains'),
                    'Your domains'],
                ['Hints',
                    self.object.get_hints_url(),
                    'Turn hints on/off'],
                ['Password',
                    reverse('user:password_change'),
                    'Change your password'],
                ['Timezone',
                    self.object.get_timezone_url(),
                    'Change your timezone'],
                ['Watching',
                    reverse('user:watching'),
                    'Articles that you are watching'],
                ['Workspace',
                    reverse('user:workspace'),
                    'Articles that you are actively working on'],]
        self.response = result
        context = super().get_context_data(**kwargs)
        context['title'] = "@"+str(self.object)
        context['subtitle'] = get_subtitle([['Home',self.object.get_absolute_url()],['Panel']])
        context['placeholder'] = 'Filter'
        context['action'] = self.object.get_panel_url()
        context['template'] = 'feed/meta.html'
        context['object'] = self.request.user
        return context

class Contributors(DomainMixin,SearchMixin,NewMixin,FormMixin,TemplateView):
    template_name = 'list.html'
    form_class = ContributorsForm

    def get_context_data(self,**kwargs):
        result = self.object.creators.select_related('user','user__domain').all()#.annotate(num_commits=Count())

        if self.q:
            query_list = self.q.split()
            result = result.filter(
                reduce(operator.and_,
                       (Q(user__domain__id__icontains=q) for q in query_list))
            )
        self.response = result
        context = super().get_context_data(**kwargs)
        context['title'] = "@"+str(self.object)
        context['is_user'] = self.object.is_user()
        context['is_owner'] = self.request.user.is_authenticated and self.object.is_owner(self.request.user)
        if context['is_owner']:
            context['subtitle'] = get_subtitle([['Home',self.object.get_absolute_url()],['Contributors']],[['new',self.object.get_new_contributor_url()]])
        else:
            context['subtitle'] = get_subtitle([['Home',self.object.get_absolute_url()],['Contributors']])
        context['placeholder'] = 'Filter'
        context['action'] = self.object.get_contributors_url()
        context['template'] = 'feed/contributors.html'
        return context


    def form_invalid(self, form):
        self.object_list = self.get_queryset()
        return self.render_to_response(self.get_context_data(form=form))

class Workspace(SearchMixin,TemplateView):
    template_name = 'list.html'

    def get_context_data(self,**kwargs):
        active_set = Article.objects.select_related('domain').filter(Q(commits__creator__user=self.request.user) & ((Q(status__lte=Article.OPEN) & ~Q(commits__page=Commit.TALK)) | Q(commits__status__lte=Commit.SAVED))).distinct().annotate(max_date=Case(When(commits__creator__user=self.request.user,then=Max('commits__modified')),output_field=DateTimeField())).annotate(page=Case(When(commits__creator__user=self.request.user,then=F('commits__page')),output_field=CharField()))
        private_set = Article.objects.select_related('domain').filter(Q(status__range=(Article.PRIVATE,Article.OPEN)) & (Q(creators__user=self.request.user,creators__role__range=(Creator.REQUESTER,Creator.EDITOR)) | Q(domain__creators__user=self.request.user,domain__creators__role=Creator.OWNER))).exclude(commits__creator__user=self.request.user).annotate(max_date=F('modified')).annotate(page=F('head__page'))
        if self.q:
            query_list = self.q.split()
            active_set = active_set.filter(
                reduce(operator.and_,
                       (Q(header__title__icontains=q) for q in query_list)) |
                reduce(operator.and_,
                       (Q(domain__id__icontains=q) for q in query_list))
            )
            private_set = private_set.filter(
                reduce(operator.and_,
                       (Q(header__title__icontains=q) for q in query_list)) |
                reduce(operator.and_,
                       (Q(domain__id__icontains=q) for q in query_list))
            )
        self.response = active_set.union(private_set).order_by('-max_date')
        context = super().get_context_data(**kwargs)
        context['title'] = str(self.request.user)
        context['subtitle'] = get_subtitle([['Home',self.request.user.get_absolute_url()],['Panel',self.request.user.domain.get_panel_url()],['Workspace']],[['new',reverse('user:new-work')]])
        context['placeholder'] = 'Filter'
        context['projects'] = True
        context['action'] = reverse("user:workspace")
        context['new'] = reverse("user:new-work")
        context['template'] = 'feed/workspace.html'
        context['object'] = self.request.user
        context['header'] = 'Workspace'
        return context



class Domains(SearchMixin,TemplateView):
    template_name = 'list.html'

    def get_context_data(self,**kwargs):
        result = Domain.objects.filter(creators__user=self.request.user,creators__role__in=range(Creator.INVITED,Creator.OWNER+1)).annotate(role=F('creators__role'))

        if self.q:
            query_list = self.q.split()
            result = result.filter(
                reduce(operator.and_,
                       (Q(id__icontains=q) for q in query_list))
            )
        self.response = result
        context = super().get_context_data(**kwargs)
        context['title'] = str(self.request.user)
        context['subtitle'] = get_subtitle([['Home',self.request.user.get_absolute_url()],['Panel',self.request.user.domain.get_panel_url()],['Domains']],[['new',reverse('user:new-domain')]])
        context['placeholder'] = 'Filter'
        context['domains'] = True
        context['action'] = reverse("user:domains")
        context['new'] = reverse("user:new-domain")
        context['template'] = 'feed/domain.html'
        context['object'] = self.request.user
        context['header'] = 'Domains'
        return context

class AnonDetail(SearchMixin,TemplateView):
    template_name = 'list.html'

    #TODO mechanism for when ip doesn't exist
    def dispatch(self,request,*args,**kwargs):
        try:
            self.object = AnonEditor.objects.get(ip=self.kwargs.get('ip'))
            return super().dispatch(request,*args,**kwargs)
        except:
            return redirect('index')

    def get_context_data(self,**kwargs):
        es = Elasticsearch(hosts=settings.ELASTICSEARCH_IPS)
        s = Search(using=es,index='commit') \
            .filter("range",committed={'gte':0}) \
            .filter(ES_Q("range",commit_id={'lt':0}) | ES_Q("range",commit_id={'gt':0})) \
            .filter("range",status={'gte':Commit.REQUESTED}) \
            .filter("range",article_status={'gte':Article.PUBLIC}) \
            .sort('-committed') \
            .filter("match",user=self.object.ip)

        if self.q not in [None,""]:
            s = s.query("match",document=self.q).highlight('document',fragment_size=50)
        s = s[self.start:self.end]
        self.response = s.execute()
        context = super().get_context_data(**kwargs)
        context['title'] = str(self.object)
        context['subtitle'] = 'Home'
        context['placeholder'] = 'Filter'
        context['template'] = 'feed/activity.html'
        context['action'] = reverse("user:anon",kwargs={'ip':self.object.ip})
        context['anondetail'] = True
        return context

class Nots(SearchMixin,TemplateView):
    template_name = 'list.html'
    paginate_by = 10

    def get_context_data(self,**kwargs):
        result = self.request.user.notifications.all()
        if self.q:
            query_list = self.q.split()
            result = result.filter(
                reduce(operator.and_,
                    (Q(description__icontains=q) for q in query_list))
            )
        self.response = result.order_by('-timestamp')
        context = super().get_context_data(**kwargs)
        context['title'] = str(self.request.user)
        context['subtitle'] = get_subtitle([['Home',self.request.user.get_absolute_url()],['Panel',self.request.user.domain.get_panel_url()],['Notifications']],)#[['watching',reverse('user:watching')]])
        context['placeholder'] = 'Filter'
        context['action'] = reverse('user:nots')
        context['template'] = 'feed/notification.html'
        context['object'] = self.request.user
        page = result[self.start:self.end]
        unreads = [p for p in page if p.unread]
        context['count'] = len(unreads)

        if context['count'] > 0:
            for i in range(0,context['count']):
                page[i].unread = False
                page[i].save()
        return context

class Watching(SearchMixin,TemplateView):
    template_name = 'list.html'

    def get_context_data(self,**kwargs):
        result1 = self.request.user.domains_watching.all()
        result2 = self.request.user.articles_watching.all()
        if self.q:
            query_list = self.q.split()
            result1 = result1.filter(
                reduce(operator.and_,
                    (Q(id__icontains=q) for q in query_list))
            )
            result2 = result2.filter(
                reduce(operator.and_,
                    (Q(header__title__icontains=q) for q in query_list))
            )

        self.response = sorted(chain(result1,result2),key=lambda obj: str(obj))
        context = super().get_context_data(**kwargs)
        context['title'] = str(self.request.user)
        context['subtitle'] = get_subtitle([['Home',self.request.user.get_absolute_url()],['Panel',self.request.user.domain.get_panel_url()],['Watching']])
        context['placeholder'] = 'Filter'
        context['action'] = reverse('user:watching')
        context['template'] = 'feed/watching.html'
        context['object'] = self.request.user
        return context

class Login(auth_views.LoginView):
    form_class = AuthForm

    def dispatch(self,request,*args,**kwargs):
        if self.request.user.is_authenticated:
            return redirect(self.request.user)
        else:
            return super().dispatch(request,*args,**kwargs)

    def get_context_data(self,**kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Idealogs'
        context['subtitle'] = get_subtitle([['Index',reverse('index')],['Login']])
        context['header'] = 'Login'
        return context

class Signup(CreateView):
    form_class = SignupForm
    template_name = "registration/signup.html"

    def dispatch(self,request,*args,**kwargs):
        if request.user.is_authenticated():
            return redirect(request.user)
        return super().dispatch(request,*args,**kwargs)

    def get_context_data(self,**kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Idealogs'
        context['subtitle'] = get_subtitle([['Index',reverse('index')],['Sign up']])
        context['header'] = 'Sign up'
        return context

    def form_valid(self,form):
        valid = super().form_valid(form) #saves user

        #authenticate and login
        email = form.cleaned_data.get('email')
        raw_password = form.cleaned_data.get('password1')
        user = authenticate(email=email,password=raw_password)
        login(self.request,user)
        self.success_url = user.get_absolute_url()
        return valid

class PasswordChangeView(auth_views.PasswordChangeView):
    template_name = 'form.html'

    def get_context_data(self,**kwargs):
        context = super().get_context_data(**kwargs)
        self.object = self.request.user
        context['object'] = self.object
        context['title'] = str(self.object)
        context['subtitle'] = get_subtitle([['Home',self.object.domain.get_absolute_url()],['Panel',self.object.domain.get_panel_url()],['Change password']])
        return context

class PasswordChangeDoneView(auth_views.PasswordChangeDoneView):

    def get_context_data(self,**kwargs):
        context = super().get_context_data(**kwargs)
        self.object = self.request.user
        context['object'] = self.object
        context['title'] = str(self.object)
        context['subtitle'] = get_subtitle([['Home',self.object.domain.get_absolute_url()],['Panel',self.object.domain.get_panel_url()],['Success']])
        return context

class PanelRedirectView(RedirectView):

    def get_redirect_url(self,*args,**kwargs):
        if self.request.user.is_authenticated:
            return self.request.user.domain.get_panel_url()
        else:
            return reverse('user:login')+"?next=/panel"
