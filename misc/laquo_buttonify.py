    def get_laquo_commit(self,changes=False):
        soup = BeautifulSoup("",'html5lib')
        button = soup.new_tag("button")
        button.string = "&laquo;"
        button['type'] = 'button'
        try:
            if self.status <= Commit.STAGED:
                laquo = self.article.get_head(cat=self.category)
            elif self.commit_id > 0:
                laquo = self.article.commits.select_related('article','article__domain').get(status__gte=Commit.COMMITTED,category=self.category,commit_id=self.commit_id-1)
            elif self.article.commits.filter(status=Commit.REQUESTED,category=self.category,commit_id__lt=self.commit_id).exists():
                laquo = self.article.commits.select_related('article','article__domain').filter(status=Commit.REQUESTED,category=self.category,commit_id__lt=self.commit_id).order_by('-commit_id')[0]
            else:
                assert False
            if changes:
                button['onclick'] = 'location.href=\''+laquo.get_changes_url()+'\';'
            else:
                button['onclick'] = 'location.href=\''+laquo.get_detail_url()+'\';'
        except:
            button['disabled'] = ""
        return button.prettify(formatter=None)

    def get_raquo_commit(self,changes=False,request=None):
        soup = BeautifulSoup("",'html5lib')
        button = soup.new_tag("button")
        button.string = "&raquo;"
        button['type'] = 'button'
        try:
            if self.status <= Commit.SAVED:
                assert False
            elif self.status >= Commit.HEAD:
                raquo = self.article.get_commit(cat=self.category,user=request.user)
                assert raquo.status <= Commit.SAVED
            elif self.commit_id >= -1:
                raquo = self.article.commits.select_related('article','article__domain').get(status__gte=Commit.COMMITTED,category=self.category,commit_id=self.commit_id+1)
            else:
                raquo = self.article.commits.select_related('article','article__domain').filter(status__in=[Commit.REQUESTED,Commit.COMMITTED],category=self.category,commit_id__range=(self.commit_id,0)).order_by('commit_id')[0]

            if changes:
                button['onclick'] = 'location.href=\''+raquo.get_changes_url()+'\';'
            else:
                button['onclick'] = 'location.href=\''+raquo.get_detail_url()+'\';'
        except:
            button['disabled'] = ""
        return button.prettify(formatter=None)

    def get_laquo_object(self,request=None):
        soup = BeautifulSoup("",'html5lib')
        button = soup.new_tag("button")
        button.string = "&laquo;"
        button['type'] = 'button'
        try:
            domain = self.article.domain
            range = self.article.range
            if request.user.is_authenticated:
                laquo = Article.objects.select_related('head','domain').filter(Q(domain=domain,range__lt=range) & (Q(status__gte=Article.PUBLIC) | Q(creators__user=request.user))).order_by('-range')[0].get_head(cat=self.category)
            else:
                laquo = Article.objects.select_related('head','domain').filter(domain=domain,range__lt=range,status__gte=Article.PUBLIC).order_by('-range')[0].get_head(cat=self.category)
        except:
            button['disabled'] = ""
        return button.prettify(formatter=None)

    def get_raquo_object(self,request=None):
        soup = BeautifulSoup("",'html5lib')
        button = soup.new_tag("button")
        button.string = "&raquo;"
        button['type'] = 'button'
        try:
            domain = self.article.domain
            range = self.article.range
            if request.user.is_authenticated:
                raquo = Article.objects.select_related('head','domain').filter(Q(domain=domain,range__gt=range) & (Q(status__gte=Article.PUBLIC) | Q(creators__user=request.user))).order_by('range')[0].get_head(cat=self.category)
            else:
                raquo = Article.objects.select_related('head','domain').filter(domain=domain,range__gt=range,status__gte=Article.PUBLIC)[0].get_head(cat=self.category)
        except:
            button['disabled'] = ""
        return button.prettify(formatter=None)
