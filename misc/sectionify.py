import pdb

def sectionify(document):
    temp = document.split("## ")
    sections = []
    header = "## "
    for i,section in enumerate(temp):
        if i == 0:
            sections.append(section)
        elif i == 1 and len(temp[i-1]) >= 4 and temp[i-1][-4:] == '---\n': #special case when ## directly follows yaml
            sections.append(header+section)
        elif i >= 1 and len(temp[i-1]) > 1 and temp[i-1][-2:] != '\n\n':
            sections[-1] = sections[-1]+header+temp[i]
        else:
            sections.append(header+section)
    return sections

t1 = """
---
title: hello
---
hello this is a test


## Bomb diggity

asdfasdfasd lasdfasdflkjasdfl kajsdf lakjsdf lasjdf laskdf jlasjdf asd f.

## Okey doke

asdfasdf asdf asdf asdf asdf asdf asfoweiruweoriu zx,v asdf asdf asdf asdf .

## Baller

asdf asdf asdf asdf lasdkf jalsdfjk alsdjkf alskdjf laskdjf alsdkjf alskdjf
"""

t2 = """
---
title: hello
---
hello this is a test


## Bomb diggity

asdfasdfasd lasdfasdflkjasdfl kajsdf lakjsdf lasjdf laskdf jlasjdf asd f.


## Okey doke

asdfasdf asdf asdf asdf asdf asdf asfoweiruweoriu zx,v asdf asdf asdf asdf .

## Baller

asdf asdf asdf asdf lasdkf jalsdfjk alsdjkf alskdjf laskdjf alsdkjf alskdjf
"""

t3 = """
---
title: hello
---
hello this is a test


## Bomb diggity

asdfasdfasd lasdfasdflkjasdfl kajsdf lakjsdf lasjdf laskdf jlasjdf asd f.


##Okey doke

asdfasdf asdf asdf asdf asdf asdf asfoweiruweoriu zx,v asdf asdf asdf asdf .

## Baller

asdf asdf asdf asdf lasdkf jalsdfjk alsdjkf alskdjf laskdjf alsdkjf alskdjf
"""

t4 = """
---
title: hello
---
## Bomb diggity

asdfasdfasd lasdfasdflkjasdfl kajsdf lakjsdf lasjdf laskdf jlasjdf asd f.

## Okey doke

asdfasdf asdf asdf asdf asdf asdf asfoweiruweoriu zx,v asdf asdf asdf asdf .

## Baller

asdf asdf asdf asdf lasdkf jalsdfjk alsdjkf alskdjf laskdjf alsdkjf alskdjf
"""

t5 = """


## Bomb diggity

asdfasdfasd lasdfasdflkjasdfl kajsdf lakjsdf lasjdf laskdf jlasjdf asd f.

## Okey doke

asdfasdf asdf asdf asdf asdf asdf asfoweiruweoriu zx,v asdf asdf asdf asdf .

## Baller

asdf asdf asdf asdf lasdkf jalsdfjk alsdjkf alskdjf laskdjf alsdkjf alskdjf
"""

t6 = """
---
title: hello
---

### Bomb diggity

asdfasdfasd lasdfasdflkjasdfl kajsdf lakjsdf lasjdf laskdf jlasjdf asd f.

## Okey doke

asdfasdf asdf asdf asdf asdf asdf asfoweiruweoriu zx,v asdf asdf asdf asdf .

## Baller

asdf asdf asdf asdf lasdkf jalsdfjk alsdjkf alskdjf laskdjf alsdkjf alskdjf
"""

t7 = """
---
title: hello
---

## Bomb diggity

asdfasdfasd lasdfasdflkjasdfl kajsdf lakjsdf lasjdf laskdf jlasjdf asd f.

## Okey doke

asdfasdf asdf asdf asdf asdf asdf asfoweiruweoriu zx,v asdf asdf asdf asdf .

### artichoke

asdfasdf asdf asdf .

## Baller

asdf asdf asdf asdf lasdkf jalsdfjk alsdjkf alskdjf laskdjf alsdkjf alskdjf
"""

t8 = """
---
title: hello
---

## Bomb diggity

asdfasdfasd lasdfasdflkjasdfl kajsdf lakjsdf lasjdf laskdf jlasjdf asd f.

## Okey doke

asdfasdf asdf asdf asdf asdf asdf asfoweiruweoriu zx,v asdf asdf asdf asdf .

### artichoke

asdfasdf asdf asdf .## test it out

## Baller

asdf asdf asdf asdf lasdkf jalsdfjk alsdjkf alskdjf laskdjf alsdkjf alskdjf
"""

t9 = """
---
title: hello
---
hello this is a test


## Bomb diggity

asdfasdfasd lasdfasdflkjasdfl kajsdf lakjsdf lasjdf laskdf jlasjdf asd f.
## Okey doke

asdfasdf asdf asdf asdf asdf asdf asfoweiruweoriu zx,v asdf asdf asdf asdf .

## Baller

asdf asdf asdf asdf lasdkf jalsdfjk alsdjkf alskdjf laskdjf alsdkjf alskdjf
"""

t10 = """
---
title: hello
---
hello this is a test

### Bomb diggity

asdfasdfasd lasdfasdflkjasdfl kajsdf lakjsdf lasjdf laskdf jlasjdf asd f.

## Okey doke

asdfasdf asdf asdf asdf asdf asdf asfoweiruweoriu zx,v asdf asdf asdf asdf .

## Baller

asdf asdf asdf asdf lasdkf jalsdfjk alsdjkf alskdjf laskdjf alsdkjf alskdjf
"""

t5 = """

asdfasdfasdfasdfasdfasdfasdf
asdf asdf asdf asdf lasdkf jalsdfjk alsdjkf alskdjf laskdjf alsdkjf alskdjf
"""

# print(sectionify(t1))
# print(sectionify(t2))
# print(sectionify(t3))
# print(sectionify(t4))
# print(sectionify(t5))
# print(sectionify(t6))
# print(sectionify(t7))
# print(sectionify(t8))
# print(sectionify(t9))
# print(sectionify(t9))
# print(sectionify(t10))
# print(t6)

print(sectionify(t9))
print(''.join(sectionify(t9)))
