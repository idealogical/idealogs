from django import forms
from django.core.exceptions import ValidationError
from django.core.validators import validate_email
from django.core.mail import EmailMessage
from django.contrib.auth import get_user_model
from django.utils.safestring import mark_safe
from django.db import transaction
from django.db.models import Q
from django.conf import settings
from .models import Article, Commit, AnonEditor, get_uri, get_netloc
from user.models import Domain,Creator
from lib.tifdoc import Tifdoc
from lib.utils import Object, validate
from notifications.signals import notify
from idealogs.templatetags.idealogs_extras import role
import bleach
import requests
import re
import pdb

class IdealogsMixin(forms.Form):
    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop('request', None)
        self.object = kwargs.pop('object',None)
        self.commit = kwargs.pop('commit',None)
        self.head = kwargs.pop('head',None)
        self.role = kwargs.pop('role',None)
        self.fubar = kwargs.pop('fubar',None)
        self.target = kwargs.pop('target',None)
        self.section = kwargs.pop('section',None)
        super().__init__(*args, **kwargs)

    def clean(self):
        cleaned_data = super().clean()
        for i in range(1,7):
            if "action"+str(i) in self.data:
                cleaned_data['action'] = i
                break
        return cleaned_data

class UpdateMixin(forms.Form):
    document = forms.CharField(required=False,widget=forms.Textarea())
    head_id = forms.IntegerField(required=False,widget=forms.HiddenInput())

class ChangesForm(IdealogsMixin,forms.Form):
    temp = forms.HiddenInput()

class HistoryForm(IdealogsMixin,forms.Form):
    commits = forms.MultipleChoiceField(widget=forms.CheckboxSelectMultiple())

    def __init__(self,*args,**kwargs):
        queryset = kwargs.pop('queryset',None)
        super().__init__(*args,**kwargs)
        self.fields['commits'].choices = queryset

    def clean(self):
        cleaned_data = super().clean()
        if len(cleaned_data['commits']) != 2:
            raise ValidationError("you must select two commits to diff")
        elif cleaned_data['commits'][0] == cleaned_data['commits'][1]:
            raise ValidationError("you must select two different commits to diff")
        elif cleaned_data['commits'][0] == 'None':
            cleaned_data['commits'][0] = None
        return cleaned_data

class NewForm(IdealogsMixin,forms.Form):
    identifier = forms.CharField()
    identifier2 = forms.CharField()

    def clean_identifier(self):
        return bleach.clean(self.cleaned_data['identifier'])

    def clean(self):
        cleaned_data = super().clean()
        identifier = cleaned_data['identifier']
        domain = Domain.translate(identifier[-1])
        if domain is None:
            raise ValidationError('must end with one of , . ? ;')
        if domain in [Domain.DEF,Domain.TNS]: identifier = identifier[:-1]
        article = Article()
        article.submit(identifier=identifier,request=self.request,domain=domain)
        cleaned_data['article'] = article
        return cleaned_data

class DetailForm(IdealogsMixin,forms.Form):
    status = forms.IntegerField(required=False,widget=forms.HiddenInput())
    validate_message = "not a valid request"

    def clean(self):
        cleaned_data = super().clean()
        if cleaned_data['status'] == Commit.READONLY or (cleaned_data['status'] <= Commit.SAVED and self.commit.page == Commit.WORK and self.object.status >= Article.PUBLISHED):
            validate(self.commit.status <= Commit.SAVED and self.commit.was_created_by(self.request.user),self.validate_message)
            if cleaned_data['action'] == 1:
                success_url = self.commit.get_changes_url(section=self.section)
            elif cleaned_data['action'] == 2:
                Tifdoc(status=Commit.DELETED,
                        article=self.object,
                        head=self.head,
                        commit=self.commit,
                        request=self.request,
                        document=self.commit.document,
                        message=self.commit.message)
                success_url = self.commit.get_absolute_url()
        elif (cleaned_data['status'] == Commit.INVALID or (cleaned_data['status'] <= Commit.REJECTED and self.fubar)) and self.commit.is_mutable():
            validate(self.commit.status <= Commit.INVALID and self.commit.was_created_by(self.request.user),self.validate_message)
            if cleaned_data['action'] == 1:
                success_url = self.commit.get_changes_url(section=self.section)
            elif cleaned_data['action'] == 2:
                success_url = self.commit.get_patch_url()
            elif cleaned_data['action'] == 3:
                success_url = self.commit.get_merge_url()
            elif cleaned_data['action'] == 4:
                Tifdoc(status=Commit.DELETED,
                        article=self.object,
                        head=self.head,
                        commit=self.commit,
                        request=self.request,
                        document=self.commit.document,
                        message=self.commit.message)
                success_url = self.commit.get_absolute_url()
        elif cleaned_data['status'] <= Commit.REJECTED and not self.fubar and self.commit.is_mutable():
            self.commit.status <= Commit.REJECTED and self.commit.was_created_by(self.request.user)
            success_url = self.commit.get_absolute_url()
            if cleaned_data['action'] == 1:
                success_url = self.commit.get_changes_url(section=self.section)
            elif cleaned_data['action'] == 2:
                Tifdoc(status=Commit.SAVED,
                        article=self.object,
                        head=self.head,
                        commit=self.commit,
                        request=self.request,
                        document=self.commit.document,
                        message=self.commit.message)
            elif cleaned_data['action'] == 3:
                Tifdoc(status=Commit.DELETED,
                        article=self.object,
                        head=self.head,
                        commit=self.commit,
                        request=self.request,
                        document=self.commit.document,
                        message=self.commit.message)
        elif cleaned_data['status'] ==  Commit.REQUESTED and self.object.status == Article.PUBLIC and self.role == Creator.EDITOR:
            validate(self.commit.status == Commit.REQUESTED and not self.commit.was_created_by(self.request.user),self.validate_message)
            if cleaned_data['action'] == 1:
                success_url = self.commit.get_changes_url(section=self.section)
            elif cleaned_data['action'] == 2:
                success_url = self.commit.get_patch_url()
            elif cleaned_data['action'] == 3:
                success_url = self.commit.get_merge_url()
            elif cleaned_data['action'] == 4:
                success_url = self.commit.get_reject_url()
        elif cleaned_data['status'] == Commit.SAVED:
            validate(self.commit.status == Commit.SAVED and self.commit.was_created_by(self.request.user),self.validate_message)
            if cleaned_data['action'] == 1:
                success_url = self.commit.get_edit_url(section=self.section)
            elif cleaned_data['action'] == 2:
                success_url = self.commit.get_changes_url(section=self.section)
            elif cleaned_data['action'] == 3:
                validate(self.commit.document != self.head.document,self.validate_message)
                success_url = self.commit.get_stage_url()
            elif cleaned_data['action'] == 4:
                Tifdoc(status=Commit.DELETED,
                        article=self.object,
                        head=self.head,
                        commit=self.commit,
                        request=self.request,
                        document=self.commit.document,
                        message=self.commit.message)
                success_url = self.commit.get_absolute_url()
        elif cleaned_data['status'] == Commit.REQUESTED and self.object.status == Article.PUBLIC:
            if cleaned_data['action'] == 1:
                success_url = self.commit.get_changes_url(section=self.section)
            if cleaned_data['action'] == 2:
                validate(self.commit.was_created_by(self.request.user),self.validate_message)
                Tifdoc(status=Commit.RECALLED,
                        article=self.object,
                        head=self.head,
                        commit=self.commit,
                        request=self.request,
                        document=self.commit.document,
                        message=self.commit.message)
                success_url = self.commit.get_absolute_url()
        elif self.role == Creator.APPROVER and not self.object.domain.is_user() and self.commit.page == Commit.WORK and cleaned_data['status'] in [Commit.HEAD,Commit.APPROVED]:
            if cleaned_data['action'] == 1:
                success_url = self.commit.get_edit_url(section=self.section)
            elif cleaned_data['action'] == 2:
                success_url = self.commit.get_changes_url(section=self.section)
            else:
                success_url = self.commit.get_absolute_url()
                if cleaned_data['status'] == Commit.HEAD:
                    validate(self.commit.status == Commit.HEAD,self.validate_message)
                    if cleaned_data['action'] == 3:
                        Tifdoc(status=Commit.APPROVED,
                                article=self.object,
                                head=self.head,
                                commit=self.commit,
                                request=self.request,
                                document=self.commit.document,
                                message=self.commit.message)
                elif cleaned_data['status'] == Commit.APPROVED:
                    validate(self.commit.status == Commit.APPROVED,self.validate_message)
                    if cleaned_data['action'] == 3:
                        Tifdoc(status=Commit.COMMITTED, #hack for revoking, Commit.COMMITTED is not used anywhere else
                                article=self.object,
                                head=self.head,
                                commit=self.commit,
                                request=self.request,
                                document=self.commit.document,
                                message=self.commit.message)
        elif self.role == Creator.EDITOR and self.commit.page == Commit.WORK and not self.object.has_working_copy(page=self.commit.page,user=self.request.user) and cleaned_data['status'] in [Commit.HEAD,Commit.APPROVED,Commit.PUBLISHED]:
            if cleaned_data['action'] == 1:
                success_url = self.commit.get_edit_url(section=self.section)
            elif cleaned_data['action'] == 2:
                success_url = self.commit.get_changes_url(section=self.section)
            else:
                success_url = self.commit.get_absolute_url()
                if self.object.status in range(Article.PRIVATE,Article.OPEN+1):
                    if cleaned_data['action'] == 3:
                        validate((cleaned_data['status'] == Commit.APPROVED and not self.object.domain.is_user()) or (cleaned_data['status'] == Commit.HEAD and self.object.domain.is_user()),self.validate_message)
                        validate(self.object.creators.filter(user=self.request.user,role=self.role).exists() and not self.commit.publish.editors.filter(user=self.request.user,role=self.role).exists(),self.validate_message)
                        Tifdoc(status=Commit.PUBLISHED,
                                article=self.object,
                                head=self.head,
                                commit=self.commit,
                                request=self.request,
                                document=self.commit.document,
                                message=self.commit.message)
                elif self.object.status == Article.PUBLISHED:
                    if cleaned_data['status'] == Commit.PUBLISHED:
                        validate(self.commit.status == Commit.PUBLISHED,self.validate_message)
                        if cleaned_data['action'] == 3:
                            Tifdoc(status=Commit.SUBMITTED,
                                    article=self.object,
                                    head=self.head,
                                    commit=self.commit,
                                    request=self.request,
                                    document=self.commit.document,
                                    message=self.commit.message)
                            self.object.refresh_from_db()
                            success_url = self.object.hash.get_absolute_url()
        elif cleaned_data['status'] >= Commit.COMMITTED:
            validate(self.commit.status >= Commit.COMMITTED,self.validate_message)
            if cleaned_data['action'] == 1:
                success_url = self.commit.get_changes_url(section=self.section)
            elif cleaned_data['action'] == 2:
                validate(self.commit.is_mutable() and (self.object.status < Article.FULL or self.request.user.is_staff) and self.commit.status == Commit.COMMITTED,self.validate_message)
                if self.request.user.is_authenticated:
                    validate(self.role in Creator.MUTATERS and not self.object.has_working_copy(page=self.commit.page,user=self.request.user),self.validate_message)
                    Tifdoc(status=Commit.SAVED,
                            article=self.object,
                            head=self.head,
                            commit=self.head,
                            request=self.request,
                            document=self.commit.document,
                            message='Reverting back to Commit '+str(self.commit.commit_id))
                    success_url = self.head.get_stage_url()
                else:
                    success_url = self.object.get_anonit_url(page=self.commit.page)+"?revert="+str(self.commit.commit_id)
            elif cleaned_data['action'] == 3:
                validate(self.commit.is_mutable() and (self.object.status < Article.FULL or self.request.user.is_staff) and self.commit.status == Commit.HEAD,self.validate_message)
                if self.request.user.is_authenticated:
                    validate(self.role in Creator.MUTATERS and not self.object.has_working_copy(page=self.commit.page,user=self.request.user),self.validate_message)
                    success_url = self.head.get_edit_url(section=self.section)
                else:
                    success_url = self.object.get_anonit_url(page=self.commit.page)
        cleaned_data['success_url'] = success_url
        return cleaned_data

class AuthUpdateForm(UpdateMixin,IdealogsMixin,forms.Form):

    def clean(self):
        cleaned_data = super().clean()
        return cleaned_data

class AnonUpdateForm(UpdateMixin,IdealogsMixin,forms.Form):
    message = forms.CharField(widget=forms.Textarea())

    def clean(self):
        cleaned_data = super().clean()

        document = cleaned_data['document'].replace('\r\n','\n').rstrip()
        if self.section is not None:
            sections = self.commit.sectionify()
            count = len(sections)
            sections[self.section] = document
            if self.section < count - 1:
                sections[self.section] += "\n\n"
            document = ''.join(sections)

        if cleaned_data['action'] == 4:
            return cleaned_data
        elif cleaned_data['action'] == 1 and document == self.commit.document and self.commit.status != -0.5:
            raise ValidationError({'message':"no changes to commit"})
        elif cleaned_data['action'] == 1 and not cleaned_data['message']:
            raise ValidationError({'message':'a commit message is required'})
        elif cleaned_data['action'] == 2:
            raise ValidationError({'message':"let's edit some more!"})
        elif self.head.commit_id != cleaned_data['head_id']:
            raise ValidationError({'message':mark_safe("Oy vey! Someone committed while you were editing! Please record your contribution somewhere offsite and then submit it from <a href=\""+self.object.get_edit_url()+"\">here</a> instead.")})

        try:
            tifdoc = Tifdoc(status=Commit.STAGED if cleaned_data['action'] == 1 else Commit.HEAD,
                            article=self.object,
                            head=self.head,
                            commit=self.head,
                            request=self.request,
                            document=document,
                            message=cleaned_data['message'],)
        except:
            raise ValidationError({'message':'formatting error (I think)'})
        cleaned_data['body'] = tifdoc.commit.body
        cleaned_data['citation'] = tifdoc.commit.citation
        return cleaned_data

class ActionForm(IdealogsMixin,forms.Form):
    document = forms.CharField(required=False)

    def clean(self):
        cleaned_data = super().clean()
        if cleaned_data['action'] == 2:
            Tifdoc(status=Commit.SAVED if self.target == self.head else self.target.status,
                    article=self.object,
                    head=self.head,
                    commit=self.commit,
                    request=self.request,
                    document=cleaned_data['document'],
                    message=self.target.message if self.target.status == Commit.INVALID else None,
                    parent=self.target.parent if self.target.status == Commit.INVALID else None)
            if self.target != self.head: self.target.delete()
        elif cleaned_data['action'] == 3:
            Tifdoc(status=Commit.HEAD,
                    article=self.object,
                    head=self.head,
                    commit=self.commit,
                    request=self.request,
                    document=cleaned_data['document'],
                    message=self.commit.message,)
            if self.commit.creator:
                notify.send(self.request.user,
                        recipient=self.commit.creator.user,
                        verb="accepted your pull request for",
                        target=self.commit,
                        description='accept '+self.commit.str2()+' '+str(self.request.user),
                    )
        return cleaned_data

class RejectForm(IdealogsMixin,forms.Form):
    message = forms.CharField(required=False,widget=forms.Textarea())

    def clean(self):
        cleaned_data = super().clean()
        if cleaned_data['action'] == 1:
            return cleaned_data
        elif cleaned_data['action'] == 2 and not cleaned_data['message']:
            raise ValidationError('a commit message is required')
        else:
            Tifdoc(status=Commit.REJECTED,
                    article=self.object,
                    head=self.head,
                    commit=self.commit,
                    request=self.request,
                    document=self.commit.document,
                    message=cleaned_data['message'])

class StageForm(IdealogsMixin,forms.Form):
    message = forms.CharField(required=False,widget=forms.Textarea())
    validate_message = "validation check failed"

    def clean(self):
        cleaned_data = super().clean()
        if cleaned_data['action'] == 1:
            return cleaned_data
        elif cleaned_data['action'] == 2 and not cleaned_data['message']:
            raise ValidationError('a commit message is required')
        else:
            if cleaned_data['action'] == 2:
                validate(self.commit.page == Commit.TALK or (self.role == Creator.EDITOR or (self.commit.is_workable() and self.role in [Creator.WRITER,Creator.APPROVER])),self.validate_message)
                status = Commit.HEAD
            else:
                validate(cleaned_data['action'] == 3 and self.commit.page == Commit.WORK and self.role == Creator.REQUESTER,self.validate_message)
                status = Commit.REQUESTED
            Tifdoc(status=status,
                    article=self.object,
                    head=self.head,
                    commit=self.commit,
                    request=self.request,
                    document=self.commit.document,
                    message=cleaned_data['message'],)
            return cleaned_data

class MoveForm(forms.Form):
    junk = forms.CharField(required=False,widget=forms.HiddenInput())


class DeleteForm(IdealogsMixin,forms.Form):
    nothing = forms.CharField(required=False,widget=forms.HiddenInput())

BIBTEX = 'bibtex'
BIBLATEX = 'biblatex'
COPAC = 'copac'
JSON = 'json'
YAML = 'yaml'
ENDNOTE = 'endnote'
ENDNOTEXML = 'endnotexml'
ISI = 'isi'
MEDLINE = 'medline'
MODS = 'mods'
NBIB = 'nbib'
RIS = 'ris'

BIB_CHOICES = (
    (BIBTEX, 'BibTeX'),
    (BIBLATEX, 'BibLaTeX'),
    (COPAC, 'Copac'),
    (JSON, 'CSL JSON'),
    (YAML, 'CSL YAML'),
    (ENDNOTE, 'EndNote'),
    (ENDNOTEXML, 'EndNote XML'),
    (ISI, 'ISI'),
    (MEDLINE, 'MEDLINE'),
    (MODS, 'MODS'),
    (NBIB, 'NBIB'),
    (RIS, 'RIS'),
)

class B2YForm(forms.Form):
    format = forms.ChoiceField(label="Format",choices=BIB_CHOICES,required=True)
    document = forms.CharField(required=False,widget=forms.Textarea(attrs={'id': 'target','style':'width:100%;','rows':'20'},))

from lib.mixins import CreatorsFormMixin, NewCreatorFormMixin
class CreatorsForm(CreatorsFormMixin,IdealogsMixin,forms.Form):

    def clean(self):
        cleaned_data = super().clean()
        cleaned_data['success_url'] = self.commit.get_creators_url()
        return cleaned_data

class NewCreatorForm(NewCreatorFormMixin,IdealogsMixin,forms.Form):
    creator = forms.ChoiceField()
    role = forms.ChoiceField()

    def __init__(self,*args,**kwargs):
        queryset1 = kwargs.pop('queryset1')
        queryset2 = kwargs.pop('queryset2')
        super().__init__(*args,**kwargs)
        self.fields['creator'].choices = queryset1
        self.fields['role'].choices = queryset2

    def clean(self):
        self.cleaned_data['handle'] = str(Creator.objects.get(id=int(self.cleaned_data['creator'])).user)
        cleaned_data = super().clean()

        if int(cleaned_data['role']) == Creator.APPROVER and self.object.domain.is_user():
            raise ValidationError("approvers are only for projects published under journals")
        elif int(cleaned_data['role']) == Creator.READER and self.object.status == Article.PUBLIC:
            raise ValidationError("readers are only for private projects")

        creator, created = Creator.objects.get_or_create(user=cleaned_data['invitee'].user,role=int(cleaned_data['role']))

        with transaction.atomic():
            if self.object.creators.filter(user=cleaned_data['invitee'].user).exists():
                if creator.role == Creator.NONE and self.object.is_removable(creator):
                    raise ValidationError("remove on 'Creators' page instead")
                remover = self.object.creators.get(user=cleaned_data['invitee'].user)
                self.object.creators.remove(remover)
            elif creator.role == Creator.NONE:
                raise ValidationError("don't do that; 'none' is a tool to block users from public and open projects who are not following the rules")
            self.object.creators.add(creator)

            commits = self.object.commits.select_for_update().filter(Q(creator__user=creator.user) & (Q(status__lte=Commit.SAVED) | Q(status=Commit.REQUESTED)))
            for commit in commits:
                if creator.role == Creator.NONE:
                    commit.delete()
                elif commit.status != Commit.REQUESTED:
                    if creator.role == Creator.READER:
                        commit.status = Commit.READONLY
                    commit.creator = creator
                    commit.save()

            if self.request.user != creator.user:
                notify.send(self.request.user,
                            recipient=creator.user,
                            verb="made you a(n) "+role(creator.role)+" for",
                            target=self.object,
                            description=role(creator.role)+' '+self.object.str2()+' '+str(self.request.user),
                        )
        cleaned_data['success_url'] = self.commit.get_creators_url()
        return cleaned_data

class WatchForm(IdealogsMixin,forms.Form):
    watch = forms.BooleanField(required=False)

    def clean(self):
        cleaned_data = super().clean()
        if cleaned_data['watch']:
            self.object.watchers.add(self.request.user)
        else:
            self.object.watchers.remove(self.request.user)
        return cleaned_data

class ContactForm(forms.Form):
    subject = forms.CharField()
    document = forms.CharField()
    email = forms.CharField()

    def clean(self):
        raise ValidationError("This feature was temporarily disabled on Oct 1st 2019 at 12p Central Time. It will be up and running by tomorrow, Oct 2nd 2019 at 12p Central Time.  If you need to reach me in the meantime please send me an email at tyler@idealogs.org")
        cleaned_data = super().clean()
        validate_email(cleaned_data['email'])

        requests.post(
		"https://api.mailgun.net/v3/mg.idealogs.org/messages",
		auth=("api", settings.MAILGUN_API_KEY),
		data={"from": "Idealogs <no-reply@mg.idealogs.org>",
			"to": ["tyler@idealogs.org"],
			"subject": cleaned_data['subject'],
			"text": cleaned_data['document'],
            "h:Reply-To" : [cleaned_data['email']]})

        requests.post(
		"https://api.mailgun.net/v3/mg.idealogs.org/messages",
		auth=("api", settings.MAILGUN_API_KEY),
		data={"from": "Idealogs <no-reply@mg.idealogs.org>",
			"to": [cleaned_data['email']],
			"subject": "Message Received",
			"text": "Thanks for reaching out. I will be in touch as soon as possible.\n\nBest,\nTyler",})

        # email = EmailMessage(cleaned_data['subject'],
        #                         cleaned_data['document'],
        #                         to=['tyler@idealogs.org'],
        #                         reply_to=[cleaned_data['email']])
        # email.send()

        return cleaned_data
