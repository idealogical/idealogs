from django import template
from django.core.exceptions import ValidationError
from django.core.cache import cache
from idealogs.models import Article, Commit
from user.models import Domain
from user.models import Creator
from lib.utils import Object
import re
import math
import pdb
import datetime
import bleach

register = template.Library()

@register.filter(is_safe=True)
def get_role(object,user):
    return object.get_role(user)

@register.filter(is_safe=True)
def wbr(s):
    return str(s).replace('.','.<wbr>')

@register.filter(is_safe=True)
def rangeify(range):
    return hex(int(range))[2:].upper()

@register.filter(is_safe=True)
def strptime(time):
    return datetime.datetime.strptime(time[:-6],'%Y-%m-%dT%H:%M:%S.%f')

@register.filter(is_safe=True)
def highlightify(highlight):
    text = "...".join(highlight).replace("<em>","<strong>").replace("</em>","</strong>")
    return bleach.clean(text,
                        tags=['strong'])

@register.filter(is_safe=True)
def ideaify(link,side):
    if side == 'l':
        return "@"+link.l_domain+"x"+hex(link.l_range)[2:].upper()
    elif side == 'r':
        return "@"+link.r_domain+"x"+hex(link.r_range)[2:].upper()

@register.filter(is_safe=True)
def is_removable(object,creator):
    return object.is_removable(creator)

@register.filter(is_safe=True)
def pending(commit,request):
    if not request.user.is_authenticated: return False
    creator = Creator.objects.get(user=request.user,role=Creator.EDITOR)
    if commit.is_workable() and ((commit.status == Commit.APPROVED and not commit.article.domain.is_user()) or (commit.status == Commit.HEAD and commit.article.domain.is_user())):
        x = commit.publish.editors.count()
        return creator in commit.article.creators.all() and x < commit.article.creators.count() and x > 0
    else:
        return False


@register.simple_tag(takes_context=True)
def get_commit(context):
    if context['object'].page == Commit.TALK:
        head = context['object'].talk_head
    else:
        head = context['object'].head
    # head = context['object'].head
    commit = context['object'].get_commit(page=head.page,user=context['request'].user,head=head)
    return commit

@register.filter(is_safe=True)
def get_commit(commit,request):
    return commit.article.get_commit(page=commit.page,user=request.user)

@register.filter(is_safe=True)
def get_commits(commit,user):
    return commit.get_commits(user)

@register.filter(is_safe=True)
def get_objects(domain,user):
    return domain.get_objects(user)

@register.filter(is_safe=True)
def laquo_commit(commit,request):
    if '/changes' in request.path:
        return commit.get_laquo_commit(changes=True)
    else:
        return commit.get_laquo_commit()

@register.filter(is_safe=True)
def raquo_commit(commit,request):
    if '/changes' in request.path:
        return commit.get_raquo_commit(changes=True,request=request)
    else:
        return commit.get_raquo_commit(request=request)

@register.filter(is_safe=True)
def laquo_object(commit,request):
    return commit.get_laquo_object(request=request)

@register.filter(is_safe=True)
def raquo_object(commit,request):
    return commit.get_raquo_object(request=request)

@register.filter(is_safe=True)
def astatus(status):
    if status == Article.PRIVATE:
        return 'private'
    elif status == Article.PUBLIC:
        return 'public'
    elif status == Article.OPEN:
        return 'open'
    elif status == Article.PUBLISHED:
        return 'published'
    elif status == Article.SUBMITTED:
        return 'submitted'
    elif status == Article.DEFAULT:
        return 'default'
    elif status == Article.SEMI:
        return 'semi'
    elif status == Article.PENDING:
        return 'pending'
    elif status == Article.FULL:
        return 'full'

@register.filter(is_safe=True)
def status(status):
    if status == -5:
        return 'none'
    elif status == Commit.READONLY:
        return 'read-only'
    elif status == Commit.RECALLED:
        return 'recalled'
    elif status == Commit.REJECTED:
        return 'REJECTED'
    elif status == Commit.INVALID:
        return 'INVALID'
    elif status == -0.5:
        return 'reverting'
    elif status == Commit.SAVED:
        return 'saved'
    elif status == Commit.STAGED:
        return 'staged'
    elif status == Commit.REQUESTED:
        return 'requested'
    elif status == Commit.COMMITTED:
        return 'committed'
    elif status == Commit.HEAD:
        return 'head'
    elif status == Commit.APPROVED:
        return 'APPROVED'
    elif status == Commit.PUBLISHED:
        return 'published'
    elif status == Commit.SUBMITTED:
        return 'submitted'

@register.filter(is_safe=True)
def role(role):
    if role == Creator.NONE:
        return 'none'
    elif role == Creator.REQUESTER:
        return 'requester'
    elif role == Creator.READER:
        return 'reader'
    elif role == Creator.WRITER:
        return 'writer'
    elif role == Creator.APPROVER:
        return 'approver'
    elif role == Creator.EDITOR:
        return 'editor'
    elif role == Creator.INVITED:
        return 'invited'
    elif role == Creator.MEMBER:
        return 'member'
    elif role == Creator.MANAGER:
        return 'manager'
    elif role == Creator.OWNER:
        return 'owner'
    elif role == Creator.SELF:
        return 'self'
