from django.db import models
from django.conf import settings
from django.core.cache import cache
from django.core.exceptions import ValidationError
from django.shortcuts import redirect
from django.db.models import Q, Case, When, Count
from django.db import IntegrityError, transaction
from django.core.validators import RegexValidator
from django.contrib.postgres.fields import ArrayField, JSONField
from django.utils import timezone
from django.utils.safestring import mark_safe
from user.models import AnonEditor, Domain, Creator
from . import search
from lib.utils import Object, unwrap
from django.core.urlresolvers import reverse
from bs4 import BeautifulSoup
from urllib.parse import unquote, urlparse
from ipware import get_client_ip
from statistics import mean
from elasticsearch import Elasticsearch
from elasticsearch_dsl import Search
import copy
import base64
import pickle
import requests
import datetime
import bleach
import subprocess
import json
import tempfile
import math
import statistics
import yaml
# from lib.utils import MyYAML
import re
import pdb
headers = [{"Content-Type": "text/plain"},{"Content-Type": "application/json"}]

class Article(models.Model):
    PRIVATE = 0 #only creators can see, commit to
    PUBLIC = 1 #anyone can see and make pull requests, creators can commit to
    OPEN = 2 #anyone can see, commit to
    PUBLISHED = 3 #published so can no longer be changed, but still not in sunesiary
    SUBMITTED = 4
    DEFAULT = 5 #standard, third tab if: pub exists, or linked to @asdfasdf
    SEMI = 6
    PENDING = 7
    FULL = 8 #only is_staff users can edit

    Choices = ((PRIVATE,"private"),
                (PUBLIC,"public"),
                (OPEN,"open"))
    IDea = r'^(?i)(?P<IDea>@'+Domain.DOMAIN_INDEX+')'

    #IDea
    domain = models.ForeignKey(Domain,on_delete=models.PROTECT,null=True)
    range = models.IntegerField(null=True)
    hash = models.OneToOneField('self',on_delete=models.SET_NULL,null=True,)
    lede = models.TextField(default="")
    document = models.TextField(default="")

    modified = models.DateTimeField(auto_now=True)
    created = models.DateTimeField(auto_now_add=True)
    status = models.IntegerField(default=DEFAULT)
    creators = models.ManyToManyField(Creator)
    watchers = models.ManyToManyField(settings.AUTH_USER_MODEL,related_name="articles_watching")

    head = models.OneToOneField('Commit',null=True,related_name='head2')
    talk_head = models.OneToOneField('Commit',null=True,related_name='head3')
    header = JSONField(null=True,default=dict)
    citation = models.TextField(null=True)

    class Meta:
        unique_together = (("domain","range"))
        ordering = ['domain','range']

    def __str__(self):
        return self.get_title()

    def str2(self):
        return "@"+self.idea

    def to_search(self):
        return search.Article(meta={'id': self.id},
                                document=self.document,
                                lede=self.lede,
                                status=self.status,
                                title=self.header['title'],
                                domain=str(self.domain),
                                range=self.range,
                                modified=self.modified,)

    @property
    def idea(self):
        return str(self.domain)+"x"+hex(self.range)[2:].upper()

    @property
    def ideat(self):
        return "@"+self.idea

    def queryset(self):
        return self.__class__.objects.filter(id=self.id)

    def submit(self,identifier=None,request=None,temp=False,domain=None):
        self.request = request
        if domain == Domain.TNS:
            # yaml = MyYAML()
            try:
                self.zbib = requests.post(settings.TRANSLATION_SERVER+"web",data=identifier,headers=headers[0],timeout=5)
                assert self.zbib.status_code == 200
            except:
                self.zbib = requests.post(settings.TRANSLATION_SERVER+"search",data=identifier,headers=headers[0],timeout=5)

            if self.zbib.status_code == 200:
                self.export = requests.post(settings.TRANSLATION_SERVER+"export?format=csljson",json=self.zbib.json(),headers=headers[1],timeout=5)
                fp = tempfile.NamedTemporaryFile()
                fp.write(json.dumps(self.export.json()).encode())
                fp.seek(0)
                x = subprocess.run(['pandoc-citeproc','-y','-f','json',fp.name],stdout=subprocess.PIPE,encoding='utf-8')
                fp.close()
                self.header = yaml.safe_load(x.stdout)['references'][0]
                if 'id' in self.header: self.header.pop('id')
                if 'accessed' in self.header: self.header.pop('accessed')

            if 'title' not in self.header:
                self.header['title'] = identifier
            if temp:
                return
            self.set_container()
        else:
            self.header['title'] = identifier

        with transaction.atomic():
            self.domain = Domain.objects.get(id=domain)
            self.range = self.domain.range()
            self.status = Article.DEFAULT
            self.initialize(doc_id=domain,
                            page=Commit.KEN,
                            request=self.request)
            self.initialize(doc_id=Tifdoc.TALK,
                            page=Commit.TALK,
                            request=self.request)

    def set_prefix_cache(self):
        if 'prefix' in self.header:
            cache.set(self.ideat,self.header['prefix'],None)
        else:
            cache.delete(self.ideat)

    def set_container(self):
        if 'ISSN' in self.header:
            container_id = self.header['ISSN']
        elif 'URL' in self.header:
            container_id = get_netloc(self.header['URL'])
        elif 'container-title' in self.header:
            container_id = self.header['container_title']
        elif 'publisher' in self.header:
            container_id = self.header['publisher']
        else:
            return

        try:
            es = Elasticsearch(hosts=settings.ELASTICSEARCH_IPS)
            s = Search(using=es,index='article').filter("match",domain=Domain.DEF).query("match",document=container_id)[0:1] #in theory, the best match for this query is the container we want
            response = s.execute()
            self.header['container'] = "@"+response[0].domain+"x"+hex(response[0].range)[2:].upper()
        except:
            pass

    def initialize(self,doc_id=None,page=None,request=None):
        Tifdoc(status=Commit.HEAD,
                article=self,
                doc_id=doc_id,
                request=request,
                page=page,)

    def get_IDea(self,domain=False,range=False,em=False,toggle=False):
        soup = BeautifulSoup("","html5lib")
        if domain:
            a = soup.new_tag("a")
            a['href'] = self.domain.get_absolute_url()
            a.string = "@"+str(self.domain)
            domain = str(a)
        else:
            domain = str(self.domain)
        if range:
            a = soup.new_tag("a")
            a['href'] = self.get_absolute_url()
            if toggle:
                a['onclick'] = 'toggleLog(event);'
            if em:
                em = soup.new_tag("em")
                em.string = hex(self.range).upper()[2:]
                a.append(em)
            else:
                a.string = hex(self.range).upper()[2:]
            span = soup.new_tag('span')
            span.string = 'x'
            span['style'] = 'margin-left:.05rem;margin-right:.05rem;'
            return domain+str(span)+str(a)
        elif self.range is not None:
            return domain+"x"+hex(self.range).upper()[2:]
        else:
            return domain+"x"

    def get_handle(self):
        return "@"+self.get_IDea()

    def get_handle_linked(self,toggle=False):
        soup = BeautifulSoup("","html5lib")
        handle = "@"+self.idea
        a = soup.new_tag("a",href="/"+handle)
        a.string = handle
        if toggle:
            a['onclick'] = 'toggleLog(event);'
        return str(a)

    def get_handle_double_linked(self,toggle=False):
        return self.get_IDea(domain=True,range=True,toggle=toggle)

    def get_handle_double_linked_em(self):
        return self.get_IDea(domain=True,range=True,em=True)

    def get_title(self):
        return bleach.clean(self.header['title'])

    def get_issued(self):
        current_tz = timezone.get_current_timezone()
        if 'year' in self.header['issued'][0] and 'month' in self.header['issued'][0] and 'day' in self.header['issued'][0] and 'hour' in self.header['issued'][0] and 'minute' in self.header['issued'][0] and 'second' in self.header['issued'][0]:
            d = datetime.datetime(self.header['issued'][0]['year'],
                                    self.header['issued'][0]['month'],
                                    self.header['issued'][0]['day'],
                                    self.header['issued'][0]['hour'],
                                    self.header['issued'][0]['minute'],
                                    self.header['issued'][0]['second'])
            return current_tz.localize(d)
        elif 'year' in self.header['issued'][0] and 'month' in self.header['issued'][0] and 'day' in self.header['issued'][0]:
            d = datetime.datetime(self.header['issued'][0]['year'],
                                    self.header['issued'][0]['month'],
                                    self.header['issued'][0]['day'],)
            return current_tz.localize(d)
        else:
            return "fix date"

    def get_prefix(self):
        soup = BeautifulSoup("","html5lib")
        span = soup.new_tag("span")
        try:
            if 'container' in self.header:
                IDea = self.header['container']
                article = Article.get_article(Object(),{'IDea':IDea}).object
            else:
                assert False
            a = soup.new_tag("a")
            a['href'] = article.get_absolute_url()
            a.string = article.header['prefix']
            span.append(a)
            span.append(":")
        except:
            pass
        return span

    def get_htitle(self):
        span = self.get_prefix()
        span.append(self.get_title())
        return span

    def get_ptitle(self):
        # if self.range is None:
        #     return self.get_htitle()
        # span = self.get_prefix()
        soup = BeautifulSoup("","html5lib")
        span = soup.new_tag("a")
        a = soup.new_tag("a")
        a['href'] = self.get_absolute_url()
        a.string = self.get_title()
        span.append(a)
        return span

    def get_head(self,page=None):
        assert page is not None
        if page == Commit.TALK:
            return self.talk_head
        else:
            return self.head

    def get_tail(self,page=None):
        assert page is not None
        return self.commits.select_related('article','article__domain').filter(page=page).order_by('commit_id').first()

    def get_commit(self,page=None,user=None,id=None,head=None):
        assert page is not None
        if id is None:
            try:
                return self.commits.select_related('article','article__domain').get(page=page,creator__user=user,status__lte=Commit.SAVED)
            except (Commit.DoesNotExist,TypeError):
                return head if head is not None else self.get_head(page=page)
            except Commit.MultipleObjectsReturned:
                return self.commits.select_related('article','article__domain').filter(page=page,creator__user=user,status__lte=Commit.SAVED).order_by('status').first()
        elif user is None:
            return self.commits.select_related('article','article__domain').get(page=page,commit_id=id)

    def get_role(self,user):
        if user.is_authenticated:
            if self.status in [Article.PUBLISHED,Article.SUBMITTED]:
                return Creator.EDITOR
            else:
                try:
                    return self.creators.get(user=user).role
                except:
                    if self.status >= Article.DEFAULT:
                        return Creator.EDITOR
                    elif self.status == Article.OPEN:
                        return Creator.WRITER
                    elif self.status == Article.PUBLIC:
                        return Creator.REQUESTER
                    elif self.status == Article.PRIVATE:
                        assert self.is_owner(user)
                        return Creator.READER
        else:
            return None

    def has_working_copy(self,page=None,user=None):
        assert page is not None
        try:
            return self.commits.filter(page=page,status__lte=Commit.SAVED,creator__user=user).exists()
        except:
            return False

    def is_main(self):
        return self.domain.is_main()

    def is_owner(self,user):
        return self.domain.is_owner(user)

    def is_removable(self,creator):
        return not self.commits.filter(page=Commit.WORK,creator__user=creator.user,status__gte=Commit.REQUESTED).exists()

    def is_deletable(self,user,role):
        if self.status >= Article.DEFAULT:
            return not Link.objects.filter( \
                (Q(direction=Link.REWARD) & (Q(left=self) | Q(right=self))) | \
                Q(direction=Link.FORWARD,right=self) | \
                Q(direction=Link.BACKWARD,left=self) \
                ).exists()
        elif self.status == Article.PUBLISHED and self.domain.is_user() and self.domain.is_owner(user) and self.hash is None:
            return True
        elif self.status <= Article.OPEN:
            return role == Creator.EDITOR
        else:
            return False

    #TODO improve with user classes
    def is_movable(self,request):
        return True

    def is_visible(self,request):
        return self.status >= Article.PUBLIC or (request.user.is_authenticated and (self.is_owner(request.user) or self.creators.filter(user=request.user,role__range=(Creator.READER,Creator.EDITOR)).exists()))

    def get_absolute_url(self):
        if self.domain.is_main():
            return self.get_ken_url()
        else:
            return self.get_work_url()

    def get_ken_url(self):
        if self.domain.is_main() or not self.hash:
            kwargs = {"IDea": self.get_handle()}
        else:
            kwargs = {"IDea": self.hash.get_handle()}
        return reverse("detail",kwargs=kwargs)

    def get_talk_url(self):
        if self.domain.is_main() or not self.hash:
            kwargs = {"IDea": self.get_handle(), "page": Commit.TALK}
        else:
            kwargs = {"IDea": self.hash.get_handle(), "page": Commit.TALK}
        return reverse("detail",kwargs=kwargs)

    def get_work_url(self):
        if self.domain.is_main():
            assert self.hash
            kwargs = {"IDea": self.hash.get_handle()}
        else:
            kwargs = {"IDea": self.get_handle()}
        return reverse("detail",kwargs=kwargs)

    def get_watch_url(self):
        return self.head.get_watch_url()

    def get_notification_url(self):
        return self.get_absolute_url()

    def get_edit_url(self,page=None):
        assert page != Commit.WORK
        kwargs = {"IDea": self.get_handle(), "page": page}
        return reverse("edit",kwargs=kwargs)

    def get_anonit_url(self,page=None):
        # assert page != Commit.WORK
        kwargs = {"IDea": self.get_handle()}
        if page == Commit.TALK: kwargs['page'] = page
        return reverse("anon-edit",kwargs=kwargs)

    def get_move_url(self):
        return reverse("move",kwargs={"IDea": self.get_handle()})

    def get_pub_history_url(self):
        kwargs = {"IDea": self.get_handle()}
        return reverse("pub-history",kwargs=kwargs)

    def get_delete_url(self):
        return reverse("delete",kwargs={"IDea": self.get_handle()})

    def get_article(self,kwargs,select_related=True):
        match = re.match(r'^(?i)@(T|I|F|0)x([a-fA-F0-9]+)',kwargs.get('IDea'))
        if match:
            domain = Domain.objects.get(id__iexact=match[1])
            range = int(match[2],16)
            if select_related:
                self.object = Article.objects.select_related('head','domain').get(domain=domain,range=range)
            else:
                self.object = Article.objects.get(domain=domain,range=range)
            self.page = kwargs.get('page')
            if self.page is None: self.page = Commit.KEN
            else: assert self.page == Commit.TALK
            return self

        match = re.match(r'^(?i)@([A-Za-z0-9\.]+)x([a-fA-F0-9]+)',kwargs.get('IDea'))
        if match:
            domain = Domain.objects.get(id__iexact=match[1].replace('.',''))
            range = int(match[2],16)
            try:
                if select_related:
                    self.object = Article.objects.select_related('head','domain').get(domain=domain,range=range)
                else:
                    self.object = Article.objects.get(domain=domain,range=range)
            except:
                pass
                # self.object = PubHash.objects.get(domain=domain,range=range).article
            self.page = kwargs.get('page')
            if self.page is None: self.page = Commit.WORK
            else: assert self.page == Commit.TALK
            return self
            # if kwargs.get('page') is None:
            #     self.object.page = self.page = Commit.WORK
            #     return self
        raise ValidationError("article not found")

def get_netloc(string):
    uri = urlparse(string)
    return uri.netloc

def get_uri(string):
    uri = urlparse(string)
    if uri.scheme != 'https':
        raise ValidationError("links must use https")
    elif uri.netloc in ["www.youtube.com",]:
        return string
    else:
        return '{uri.scheme}://{uri.netloc}{uri.path}'.format(uri=uri)

class Commit(models.Model):
    article = models.ForeignKey(Article,on_delete=models.CASCADE,related_name="commits")
    commit_id = models.IntegerField(null=True)
    status = models.IntegerField(default=0)
    page = models.CharField(max_length=4)
    parent = models.IntegerField(null=True)

    document = models.TextField(default="")
    body = models.TextField(default="")
    message = models.TextField(null=True)
    citation = models.TextField(null=True)
    header = JSONField(null=True,default=dict)

    creator = models.ForeignKey(Creator,null=True,related_name="commits")
    anoncreator = models.ForeignKey(AnonEditor,null=True)
    approver = models.ForeignKey(settings.AUTH_USER_MODEL,null=True)

    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)
    committed = models.DateTimeField(null=True)
    published = models.DateTimeField(null=True)

    DELETED = -5
    READONLY = -4
    RECALLED = -3
    REJECTED = -2
    INVALID = -1
    SAVED = 0
    STAGED = 1
    REQUESTED = 2
    COMMITTED = 3
    HEAD = 4
    APPROVED = 5
    PUBLISHED = 6
    SUBMITTED = 7

    KEN = 'ken'
    TALK = 'talk'
    WORK = 'work'

    class Meta:
        unique_together = (("article","page","commit_id"))
        ordering = ['article','page','commit_id']

    def __str__(self):
        if self.page == Commit.TALK:
            return "Commit "+str(self.commit_id)+" (talk)"
        else:
            return "Commit "+str(self.commit_id)

    def str2(self):
        return self.article.str2()+": "+str(self)

    @property
    def idea(self):
        return self.article.idea

    def to_search(self):
        return search.Commit(meta={'id': self.id},
                            idea=self.idea,
                            domain=str(self.article.domain),
                            article_status=self.article.status,
                            page=self.page,
                            commit_id=self.commit_id,
                            status=self.status,
                            user=str(self.creator.user.domain) if self.creator is not None else self.anoncreator.ip,
                            role=self.creator.role if self.creator is not None else None,
                            document=self.document,
                            title = self.get_title(),
                            message=self.message,
                            modified=self.modified,
                            committed=self.committed)

    def queryset(self):
        return self.__class__.objects.filter(id=self.id)

    def statusify(self):
        if self.commit_id is None or self.status >= 4:
            return "["+status(self.status)+"]"
        else:
            return "Commit "+str(self.commit_id)

    def log(self,wc,log):
        if log == 'True':
            return True
        elif log == 'False':
            return False
        elif self.status <= Commit.COMMITTED:
            return True
        elif self.status == Commit.APPROVED:
            return True
        elif self.status == Commit.HEAD and wc:
            return True
        else:
            return False
        # else:
        #     return self.status <= Commit.COMMITTED or (self.status == Commit.HEAD and (wc or not self.article.domain.is_main())) or self.status == Commit.APPROVED

    def set_creator(self,request):
        if request and request.user.is_authenticated:
            if self.article.status == Article.PRIVATE:
                self.creator = self.article.creators.get(user=request.user)
            elif self.article.status == Article.PUBLIC:
                try:
                    self.creator = self.article.creators.get(user=request.user)
                except:
                    self.creator,created = Creator.objects.get_or_create(user=request.user,role=Creator.REQUESTER)
            elif self.article.status == Article.OPEN:
                try:
                    self.creator = self.article.creators.get(user=request.user)
                except:
                    self.creator,created = Creator.objects.get_or_create(user=request.user,role=Creator.WRITER)
            else:
                assert self.article.status >= Article.DEFAULT
                self.creator = Creator.objects.get(user=request.user,role=Creator.EDITOR)
        else:
             client_ip, is_routable = get_client_ip(request)
             ac, created = AnonEditor.objects.get_or_create(ip=client_ip)
             ac.save()
             self.anoncreator = ac

    def sectionify(self):
        temp = self.document.split("## ")
        sections = []
        header = "## "
        for i,section in enumerate(temp):
            if i == 0:
                sections.append(section)
            elif i == 1 and len(temp[i-1]) >= 4 and temp[i-1][-4:] == '---\n': #special case when ## directly follows yaml
                sections.append(header+section)
            elif i >= 1 and len(temp[i-1]) > 1 and temp[i-1][-2:] != '\n\n':
                sections[-1] = sections[-1]+header+temp[i]
            else:
                sections.append(header+section)
        return sections

    def get_title(self):
        if self.page != Commit.TALK:
            return bleach.clean(self.header['title'])
        else:
            return str(self.article) #self.article.header['title']

    def get_citation(self):
        soup = BeautifulSoup("","html5lib")
        p = soup.new_tag("p")

        #editors
        lead = self.article.get_commit(page=self.page,id=0).creator.user
        editors = []
        a = soup.new_tag("a")
        a.string = str(lead)
        a['href'] = lead.get_absolute_url()
        editors.append(a)
        others = self.article.creators.filter(role=Creator.EDITOR).exclude(user=lead).annotate(num_commits=Count(Case(When(commits__article=self.article,commits__page=Commit.WORK,then=1),output_field=models.IntegerField()))).order_by('-num_commits')
        for editor in others:
            a = soup.new_tag("a")
            a.string = str(editor.user)
            a['href'] = editor.user.get_absolute_url()
            editors.append(a)
        for i, editor in enumerate(editors[0:-1]):
            p.append(editor)
            p.append(", ")
            if editors[i] == editors[-2]:
                p.append("and ")
        p.append(editors[-1])
        p.append(". ")

        #year
        p.append('{dt.year}. '.format(dt=self.published))

        #title
        p.append("\""+self.header['title'])
        if self.header['title'][-1] != '.':
            p.append(".")
        p.append("\" ")

        #container
        em = soup.new_tag("em")
        a = soup.new_tag("a")
        if self.article.domain.is_user():
            a.string = "@Idealogs"
            a['href'] = '/@Idealogs'
        else:
            a.string = self.article.domain.str2()
            a['href'] = self.article.domain.get_absolute_url()
        em.append(a)
        p.append(em)
        p.append(", ")

        #date
        p.append('{dt:%B} {dt.day}, {dt.year}. '.format(dt=self.published))

        #href
        a = soup.new_tag("a")
        a.string = self.article.get_absolute_url()[1:]
        a['href'] = self.article.get_absolute_url()
        a['class'] = 'uri'
        p.append(a)
        p.append(".")
        return base64.b64encode(pickle.dumps(p))

    def get_commits(self,user):
        return self.article.commits.filter(page=self.page,creator__user=user,status__gte=Commit.REQUESTED)

    def get_laquo_section(self,section,request):
        sections = self.sectionify()
        soup = BeautifulSoup("",'html5lib')
        span = soup.new_tag('span')
        span['class'] = 'aquo aquo-enabled'
        span['style'] = "margin-right:.25rem;"
        if section is None or len(sections) == 1:
            span.string = '&laquo;'
            return str(span).replace("&amp;laquo;","&laquo;")
        elif section == 0:
            laquo = self.get_edit_url(request=request)
        else:
            assert section >= 1
            laquo = self.get_edit_url(request=request,section=section-1)
        a = soup.new_tag("a")
        a.string = '&laquo;'
        a['href'] = laquo
        span.append(a)
        return str(span).replace("&amp;laquo;","&laquo;")

    def get_raquo_section(self,section,request):
        sections = self.sectionify()
        soup = BeautifulSoup("",'html5lib')
        span = soup.new_tag('span')
        span['class'] = 'aquo aquo-enabled'
        span['style'] = "margin-left:.25rem;"
        if section is None:
            if len(sections) == 1:
                span.string = '&raquo;'
                return str(span).replace("&amp;raquo;","&raquo;")
            else:
                raquo = self.get_edit_url(request=request,section=0)
        elif section == len(sections) - 1:
            span.string = '&raquo;'
            return str(span).replace("&amp;raquo;","&raquo;")
        else:
            raquo = self.get_edit_url(request=request,section=section+1)
        a = soup.new_tag("a")
        a.string = '&raquo;'
        a['href'] = raquo
        span.append(a)
        return str(span).replace("&amp;raquo;","&raquo;")

    def get_laquo_commit(self,changes=False):
        soup = BeautifulSoup("",'html5lib')
        div = soup.new_tag("div")
        div['style'] = 'display:inline-block;font-family:serif;font-size:1.25rem;'
        try:
            if self.status <= Commit.STAGED:
                laquo = self.article.get_head(page=self.page)
            elif self.commit_id > 0:
                laquo = self.article.commits.select_related('article','article__domain').get(status__gte=Commit.COMMITTED,page=self.page,commit_id=self.commit_id-1)
            elif self.article.commits.filter(status=Commit.REQUESTED,page=self.page,commit_id__lt=self.commit_id).exists():
                laquo = self.article.commits.select_related('article','article__domain').filter(status=Commit.REQUESTED,page=self.page,commit_id__lt=self.commit_id).order_by('-commit_id')[0]
            else:
                assert False
            a = soup.new_tag("a")
            a.string = "&laquo;"
            if changes:
                a['href'] = laquo.get_changes_url()
            else:
                a['href'] = laquo.get_detail_url()+"?log=True"
            div.append(a)
        except:
            div.string = '&laquo;'
        return div.prettify(formatter=None)

    def get_raquo_commit(self,changes=False,request=None):
        soup = BeautifulSoup("",'html5lib')
        div = soup.new_tag("div")
        div['style'] = 'display:inline-block;font-family:serif;font-size:1.25rem;'
        try:
            assert self.status > Commit.SAVED
            if self.status >= Commit.HEAD:
                raquo = self.article.get_commit(page=self.page,user=request.user)
                assert raquo.status <= Commit.SAVED
            elif self.commit_id >= -1:
                raquo = self.article.commits.select_related('article','article__domain').get(status__gte=Commit.COMMITTED,page=self.page,commit_id=self.commit_id+1)
            else:
                raquo = self.article.commits.select_related('article','article__domain').filter(status__in=[Commit.REQUESTED,Commit.COMMITTED],page=self.page,commit_id__range=(self.commit_id,0)).order_by('commit_id')[0]
            a = soup.new_tag("a")
            a.string = "&raquo;"
            if changes:
                a['href'] = raquo.get_changes_url()
            else:
                a['href'] = raquo.get_detail_url()+"?log=True"
            div.append(a)
        except:
            div.string = "&raquo;"
        return div.prettify(formatter=None)

    def get_laquo_object(self,request=None):
        soup = BeautifulSoup("",'html5lib')
        div = soup.new_tag("div")
        div['style'] = 'display:inline-block;font-family:serif;font-size:1.25rem;'
        try:
            domain = self.article.domain
            range = self.article.range
            if request.user.is_authenticated:
                laquo = Article.objects.select_related('head','domain').filter(Q(domain=domain,range__lt=range) & (Q(status__gte=Article.PUBLIC) | Q(creators__user=request.user))).order_by('-range')[0].get_head(page=self.page)
            else:
                laquo = Article.objects.select_related('head','domain').filter(domain=domain,range__lt=range,status__gte=Article.PUBLIC).order_by('-range')[0].get_head(page=self.page)
            soup = BeautifulSoup("",'html5lib')
            a = soup.new_tag("a")
            a.string = "&laquo;"
            a['href'] = laquo.get_absolute_url()+"?log=True"
            div.append(a)
        except:
            div.string = '&laquo;'
        return div.prettify(formatter=None)

    def get_raquo_object(self,request=None):
        soup = BeautifulSoup("",'html5lib')
        div = soup.new_tag("div")
        div['style'] = 'display:inline-block;font-family:serif;font-size:1.25rem;'
        try:
            domain = self.article.domain
            range = self.article.range
            if request.user.is_authenticated:
                raquo = Article.objects.select_related('head','domain').filter(Q(domain=domain,range__gt=range) & (Q(status__gte=Article.PUBLIC) | Q(creators__user=request.user))).order_by('range')[0].get_head(page=self.page)
            else:
                raquo = Article.objects.select_related('head','domain').filter(domain=domain,range__gt=range,status__gte=Article.PUBLIC)[0].get_head(page=self.page)
            soup = BeautifulSoup("",'html5lib')
            a = soup.new_tag("a")
            a.string = "&raquo;"
            a['href'] = raquo.get_absolute_url()+"?log=True"
            div.append(a)
        except:
            div.string = '&raquo;'
        return div.prettify(formatter=None)

    def get_invalid_message(self,invalid):
        return "Someone has published <a href=\""+self.get_diff_invalid_url()+"\">Commit "+str(self.commit_id)+"</a> which may conflict with your <a href=\""+self.get_diff_url(older_id=invalid.parent)+"\">uncommitted changes</a>."

    def get_url(self,action,id=None,section=None,params=""):
        kwargs = {"IDea": self.article.get_handle()}
        if self.page == Commit.TALK: kwargs["page"] = self.page
        if id is not None:
            kwargs['commit_id'] = id
        if section is not None:
            kwargs['section'] = section
        return reverse(action,kwargs=kwargs)+params

    def get_absolute_url(self):
        return self.get_url("detail")

    def get_detail_url(self,section=None):
        return self.get_url("detail",id=self.commit_id,section=section)

    def get_settings_url(self):
        return self.get_url("settings")

    def get_notification_url(self):
        return self.get_changes_url()

    def get_watch_url(self):
        return self.get_url("watch")

    def get_delete_url(self):
        return self.get_url("delete")

    def get_edit_url(self,request=None,section=None):
        if request is None or request.user.is_authenticated:
            return self.get_url('edit',section=section)
        else:
            return self.get_url('anon-edit',section=section)

    def get_source_url(self):
        return self.get_url('source')

    def get_history_url(self):
        return self.get_url('history')

    def get_changes_url(self,section=None):
        return self.get_url('changes',id=self.commit_id,section=section)

    def get_admin_url(self):
        return self.get_url('admin')

    def get_stage_url(self):
        return self.get_url('stage')

    def get_reject_url(self):
        return self.get_url('reject',id=self.commit_id)

    def get_patch_url(self):
        id = self.commit_id if self.status in [Commit.REJECTED,Commit.REQUESTED] else None
        return self.get_url('action',params="?type=patch",id=id)

    def get_merge_url(self):
        id = self.commit_id if self.status in [Commit.REJECTED,Commit.REQUESTED] else None
        return self.get_url('action',params="?type=merge",id=id)

    def get_diff_url(self,newer_id=None,older_id=None):
        kwargs = {"IDea": self.article.get_handle()}
        if self.page == Commit.TALK: kwargs["page"] = self.page
        if older_id is not None:
            kwargs['older_id'] = older_id
        else:
            kwargs['older_id'] = self.article.get_head().commit_id
        if newer_id is not None:
            kwargs["newer_id"] = newer_id
        return reverse("diff",kwargs=kwargs)

    def get_diff_invalid_url(self):
        return self.get_url('diff')

    def get_creators_url(self):
        return self.get_url('creators')

    def get_new_creator_url(self):
        return self.get_url('modify-creator')

    def get_namespace_url(self):
        return self.article.get_namespace_url()

    def get_ptitle(self):
        return self.article.get_ptitle(self)

    def is_editable(self,request=None,role=None,wc=None):
        # return self.article.status not in [Article.PUBLISHED,Article.SUBMITTED]
        return (self.status == Commit.SAVED or (self.status in [Commit.HEAD,Commit.APPROVED] and not wc)) and self.article.status not in [Article.PUBLISHED,Article.SUBMITTED] and (self.article.status < Article.FULL or request.user.is_staff) and (not request.user.is_authenticated or role in Creator.MUTATERS)

    def is_mutable(self):
        return self.article.status <= Article.OPEN or self.page != Commit.WORK

    def is_ugly(self):
        return re.search(r'<<<<<<<(.|\n)+=======(.|\n)+>>>>>>>',self.document) is not None

    def is_workable(self):
        return self.page == Commit.WORK and self.article.status <= Article.OPEN

    def is_fubar(self,user=None):
        return self.status < Commit.INVALID and self.article.commits.filter(Q(page=self.page) & (Q(creator__user=user,status__in=[Commit.SAVED,Commit.INVALID]) | Q(status__in=[Commit.HEAD,Commit.APPROVED],parent__gte=self.parent))).exists()
        # return self.status < Commit.INVALID and self.article.commits.filter(page=self.page,creator__user=user,status__in=[Commit.SAVED,Commit.INVALID]).exists()

    def was_created_by(self,user):
        return self.creator.user == user

    def get_subtitle(self,links=True):
        if self.page == Commit.TALK:
            soup = BeautifulSoup("","html5lib")
            a = soup.new_tag("a",href=self.get_detail_url())
            a.string = "Talk"
            a['onclick'] = 'toggleLog(event);'
            return str(a) if links else a
        elif self.page == Commit.WORK:
            a = self.article.get_handle_linked(toggle=True)
            if links:
                return str(a)
            else:
                soup = BeautifulSoup(a,"html5lib")
                soup.body.wrap(soup.new_tag("span"))
                soup.body.unwrap()
                return soup.span
        elif self.page == Commit.KEN:
            soup = BeautifulSoup(self.article.get_handle_linked(toggle=True),"html5lib")
            soup.body.wrap(soup.new_tag("span"))
            soup.body.unwrap()
            if self.article.domain.is_tns() and links:
                span = soup.new_tag("span")
                span['style'] = 'font-style:normal;'
                span.string = '/'
                if self.article.hash:
                    soup.span.append(span)
                    soup.span.append(unwrap(self.article.hash,double=True))
                elif 'URL' in self.article.header:
                    soup.span.append(span)
                    a = soup.new_tag("a")
                    a.string = 'Work'
                    a['href'] = self.article.header['URL']
                    soup.span.append(a)
                elif 'DOI' in self.article.header:
                    soup.span.append(span)
                    a = soup.new_tag("a")
                    a.string = 'Work'
                    a['href'] = "https://doi.org/"+self.article.header['DOI']
                    soup.span.append(a)
            if not links:
                return soup.span
            else:
                unis = Link.objects.filter(Q(direction=Link.FORWARD,right=self.article) | Q(direction=Link.BACKWARD,left=self.article)).order_by('created')
                len_unis = len(unis)
                if  len_unis > 0:
                    span = soup.new_tag("span")
                    span['style'] = 'font-style:normal;font-size:1.2rem;'
                    span.append(" &larr; ")
                    for i,link in enumerate(unis,1):
                        if link.direction == Link.FORWARD:
                            obj = link.left
                        elif link.direction == Link.BACKWARD:
                            obj = link.right
                        assert obj != self.article
                        a = soup.new_tag("a")
                        a['href'] = "/@"+obj.idea
                        a.string = "@"+obj.idea
                        span.append(a)
                        if i < len_unis:
                            span.append(", ")
                    soup.span.append(span)
                return str(soup.span).replace("&amp;larr;","&larr;")


    def process_body(self,request,section,role,wc):
        soup = BeautifulSoup(self.body,"html5lib")
        editable = self.is_editable(request=request,role=role,wc=wc)
        if editable:
            h2s = soup.find_all("h2")
            for i,h2 in enumerate(h2s,1):
                span = soup.new_tag("span")
                span['style'] = "font-style:normal;font-size:1rem;"
                a = soup.new_tag("a")
                a['href'] = self.get_edit_url(request=request,section=i)
                a.string = "edit"
                span.append("[")
                span.append(a)
                span.append("]")
                h2.append(" ")
                h2.append(span)

        sections = soup.find_all("section",{"class":"level2"})
        if section is not None and section > 0:
            anchors = soup.find(id="TOC").select("div.toc > ul > li > a")
            for i,s in enumerate(sections,1):
                if i == section:
                    anchors[i-1]['class'] = 'toc-active'
                else:
                    s['style'] = 'display:none;'

        num_sections = len(sections)
        if num_sections:
            span = soup.find(id="TOC").find("span",id="context-heading")
            a = soup.new_tag("a")
            a['href'] = "#TOC"
            a['onclick'] = 'contextSwitch(event,0,true)'
            if section is None or section == 0:
                span['class'] = 'toc-active'
            a.string = "Context"
            span.append(a)

        math = soup.find_all("span",{'class':'math'})
        for span in math:
            if span.parent.name != 'p':
                span.wrap(soup.new_tag("p"))

        div = soup.new_tag("div")
        div['class'] = "context-nav context-nav-hidden"
        soup.div.append(div)
        return str(soup.div)

    HELPTEXT = '(briefly describe your changes)'

class Publish(models.Model):
    commit = models.OneToOneField('Commit',on_delete=models.CASCADE)
    editors = models.ManyToManyField(Creator)

class Link(models.Model):
    NOWARD = 0 #noward == deleted. don't use for now, remembering deleted links will require a D.O.U.S. (database of unusual size)
    FORWARD = 1
    BACKWARD = 2
    REWARD = 3 #reward == bidirectional. "re-" in the sense of "in return; mutually"
    WARD = {
        NOWARD: 'x',
        FORWARD: '->',
        BACKWARD: '<-',
        REWARD: '<->'
    }

    left = models.ForeignKey('Article',on_delete=models.CASCADE,related_name='lefts') #always the article with id = min(thing1.id,thing2.id)
    right = models.ForeignKey('Article',on_delete=models.CASCADE,related_name='rights') #always the article with id = max(thing1.id,thing2.id)
    direction = models.IntegerField(null=True)
    score = models.FloatField(null=True)

    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)
    rewarded = models.DateTimeField(null=True)

    class Meta:
        unique_together = (("left","right"))

    def __str__(self):
        return str(self.left)+" "+self.WARD[self.direction]+" "+str(self.right)

    def promote(self):
        self.direction = Link.REWARD
        self.rewarded = timezone.now() if self.rewarded is None else self.rewarded
        d = datetime.datetime(2003,9,9,13,21,17)
        the_beginning = timezone.get_current_timezone().localize(d)

        re1 = Link.objects.filter(Q(direction=Link.REWARD) & (Q(left=self.left) | Q(right=self.left))).count() #is >= 1
        uni1 = Link.objects.filter(Q(direction=Link.FORWARD,left=self.left) | Q(direction=Link.BACKWARD,right=self.left)).count()
        left = re1 - uni1

        re2 = Link.objects.filter(Q(direction=Link.REWARD) & (Q(left=self.right) | Q(right=self.right))).count() #is >= 1
        uni2 = Link.objects.filter(Q(direction=Link.FORWARD,left=self.right) | Q(direction=Link.BACKWARD,right=self.right)).count()
        right = re2 - uni2

        arg = statistics.mean([left,right])
        self.score = math.log(max(arg,1),4) + math.copysign(1,min(left,right))*(self.rewarded-the_beginning).total_seconds()/27000

    @classmethod
    def create(cls,source,target):
        assert source != target
        if source.domain == target.domain:
            source_count = Link.objects.filter(Q(direction=cls.REWARD) & (Q(left=source) | Q(right=source))).count()
            target_count = Link.objects.filter(Q(direction=cls.REWARD) & (Q(left=target) | Q(right=target))).count()
            if source_count < target_count:
                link = cls(left=source,right=target,direction=cls.FORWARD)
            else:
                link = cls(left=target,right=source,direction=cls.BACKWARD)
        else:
            if source.domain.id == Domain.TNS:
                if target.domain.id in [Domain.FIN,Domain.DEF]:
                    link = cls(left=source,right=target,direction=cls.FORWARD)
                elif target.domain.id == Domain.INF:
                    link = cls(left=target,right=source,direction=cls.BACKWARD)
            elif source.domain.id == Domain.INF:
                if target.domain.id in [Domain.TNS,Domain.DEF]:
                    link = cls(left=source,right=target,direction=cls.FORWARD)
                elif target.domain.id == Domain.FIN:
                    link = cls(left=target,right=source,direction=cls.BACKWARD)
            elif source.domain.id == Domain.FIN:
                if target.domain.id in [Domain.INF,Domain.DEF]:
                    link = cls(left=source,right=target,direction=cls.FORWARD)
                elif target.domain.id == Domain.TNS:
                    link = cls(left=target,right=source,direction=cls.BACKWARD)
            else:
                assert source.domain.id == Domain.DEF
                link = cls(left=target,right=source,direction=cls.BACKWARD)
        return link

    def demote(self,source=None):
        if self.left == source:
            self.direction = self.BACKWARD
        else:
            assert self.right == source
            self.direction = self.FORWARD


    #TODO fix me
    def to_search(self):
        return search.Link(meta={'id': self.id},
                            score=self.score,
                            direction=self.direction,
                            modified=self.modified,
                            l_doc=self.right.head.document,
                            l_domain=self.right.domain.id,
                            l_range=self.right.range,
                            l_title=self.right.header.get('title'),
                            l_lede=self.right.lede,
                            r_doc=self.left.head.document,
                            r_domain=self.left.domain.id,
                            r_range=self.left.range,
                            r_title=self.left.header.get('title'),
                            r_lede=self.left.lede,)

from lib.tifdoc import Tifdoc
from idealogs.templatetags.idealogs_extras import status
