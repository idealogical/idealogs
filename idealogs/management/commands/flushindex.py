from django.core.management.base import BaseCommand, CommandError
from elasticsearch import Elasticsearch
from elasticsearch_dsl.connections import connections
from elasticsearch.helpers import bulk
import idealogs.search
import user.search
import pdb

class Command(BaseCommand):
    help = 'flushes the index'

    def handle(self, *args, **options):
        idealogs.search.flush('article')
        idealogs.search.flush('commit')
        idealogs.search.flush('link')
        user.search.flush('domain')
        self.stdout.write(self.style.SUCCESS('Successfully flushed the indices'))
