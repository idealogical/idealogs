from django.core.management.base import BaseCommand, CommandError
from django.core.cache import cache
import pdb

class Command(BaseCommand):
    help = 'flushes the index'

    def handle(self, *args, **options):
        cache.clear()
        self.stdout.write(self.style.SUCCESS('Successfully cleared the cache'))
