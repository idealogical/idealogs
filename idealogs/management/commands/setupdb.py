from django.core.management.base import BaseCommand, CommandError
from user.models import Domain
import pdb

class Command(BaseCommand):
    help = 'initializes the index and feeds it all articles in the database'

    def handle(self, *args, **options):
        for i in [Domain.DEF,Domain.TNS,Domain.INF,Domain.FIN]:
            c = Domain(id=i)
            c.save()
        self.stdout.write(self.style.SUCCESS('Initialized the database'))
