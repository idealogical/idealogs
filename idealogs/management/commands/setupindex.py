from django.core.management.base import BaseCommand, CommandError
from elasticsearch import Elasticsearch
from elasticsearch_dsl.connections import connections
from elasticsearch.helpers import bulk
import idealogs.search
import idealogs.models
import user.search
import user.models
import pdb

class Command(BaseCommand):
    help = 'initializes the index and feeds it all articles in the database'

    def handle(self, *args, **options):
        idealogs.search.setup('article',idealogs.search.Article,idealogs.models.Article)
        idealogs.search.setup('commit',idealogs.search.Commit,idealogs.models.Commit)
        idealogs.search.setup('link',idealogs.search.Link,idealogs.models.Link)
        user.search.setup('domain',user.search.Domain,user.models.Domain)
        self.stdout.write(self.style.SUCCESS('Initialized the search indices'))
