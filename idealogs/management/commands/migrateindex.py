from django.core.management.base import BaseCommand, CommandError
from elasticsearch import Elasticsearch
from elasticsearch_dsl.connections import connections
from elasticsearch.helpers import bulk
from idealogs import models, search
import pdb

class Command(BaseCommand):
    help = 'migrates the index'

    def handle(self, *args, **options):
        search.migrate()
        self.stdout.write(self.style.SUCCESS('Successfully migrated the index'))
