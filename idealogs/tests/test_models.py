from django.test import RequestFactory, TestCase#, TransactionTestCase
from django.contrib.auth.models import AnonymousUser, User
from idealogs.models import Article
from user.models import Domain
import pdb

class ArticleTestCase(TestCase):

    def setUp(self):
        #domains
        for i in [Domain.DEF,Domain.TNS,Domain.INF,Domain.FIN]:
            c = Domain(id=i)
            c.save()

        #request
        self.factory = RequestFactory()
        request = self.factory.get('/')
        request.user = AnonymousUser()

        #articles
        article = Article()
        article.submit(identifier='Idealogs',
                        request=request,
                        domain=Domain.DEF)

    def test_article_submit(self):
        first = Article.objects.order_by('created')[0]
        self.assertEqual(first.domain.id,Domain.DEF)
        self.assertEqual(first.range,0)
