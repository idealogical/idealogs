from django.conf import settings
from django.views.generic import TemplateView, RedirectView, FormView, ListView
from django.views.generic.base import View
from django.views.generic.edit import FormMixin
from django.shortcuts import render, redirect
from django.db.models import Q, Max, F, Count
from django.contrib.auth import get_user_model
from django.core.exceptions import ValidationError
from django.core.urlresolvers import reverse
from django.http import Http404, JsonResponse
from django.utils import timezone
from functools import reduce
from .models import Domain, Article, Commit, Link, AnonEditor
from .forms import B2YForm, NewForm, MoveForm, DeleteForm, StageForm, CreatorsForm, NewCreatorForm, WatchForm, ContactForm
from user.models import Creator
from lib.gaykaken import gaykaken
from lib.utils import merge, patch, Object, get_subtitle, HINTS
from lib.mixins import (DetailMixin, UpdateMixin, StageMixin, HistoryMixin,
                        ChangesMixin, DiffMixin, SourceMixin, IdealogsMixin,
                        ActionMixin,buttonify, RejectMixin, CreatorsMixin,
                        NewMixin, OwnerMixin, WorkableMixin, HintMixin,
                        JSONResponseMixin,SaveMixin,QueryMixin,NewArticleMixin,
                        SearchMixin,DeleteMixin,CacheMixin)
from elasticsearch_dsl.query import MultiMatch
from elasticsearch_dsl import Search as ESearch
from elasticsearch_dsl.query import Q as ES_Q
from elasticsearch import Elasticsearch
from bs4 import BeautifulSoup
from formtools.preview import FormPreview
from notifications.signals import notify
from ipware import get_client_ip
import diff_match_patch as dmp_module
import idealogs.search as search
import bleach
import operator
import math
import subprocess
import tempfile
import re
import pdb
import random

class New(NewArticleMixin,HintMixin,FormView):
    template_name = 'new.html'

    def get_context_data(self,**kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Idealogs'
        context['subtitle'] = get_subtitle([['Index',reverse('index')],['New article']])
        return context

class Index(CacheMixin,SearchMixin,HintMixin,TemplateView):
    template_name = 'index.html'
    paginate_by = 30

    def get_context_data(self,**kwargs):
        es = Elasticsearch(hosts=settings.ELASTICSEARCH_IPS)
        s = ESearch(using=es,index='link')
        if self.q not in [None,""]:
            domain = Domain.translate(self.q[-1])
            if domain is not None:
                if domain in [Domain.DEF,Domain.TNS]: self.q = self.q[:-1]
                s = s.filter("match",r_domain=domain)
            s = s.query("match",r_doc=self.q).highlight('r_doc',fragment_size=50)
        s = s.filter("match",direction=Link.REWARD).sort('-score')[self.start:self.end]
        self.response = s.execute()
        context = super().get_context_data(**kwargs)
        context['noheader'] = True
        context['title'] = 'Idealogs'
        context['subtitle'] = get_subtitle([['Index']],[['new',reverse('new')]])
        context['action'] = reverse('index')
        context['placeholder'] = 'Search'
        context['template'] = 'feed/link.html'
        return context

class Search(SearchMixin,TemplateView):
    template_name = 'list.html'

    def get_context_data(self,**kwargs):
        if self.q in [None,""]:
            self.response = []
            template = 'feed/article_es.html'
        elif re.match(r'(?i)@([A-Za-z0-9\.]+)x([a-fA-F0-9]+)',self.q):
            match = re.match(r'(?i)@([A-Za-z0-9\.]+)x([a-fA-F0-9]+)',self.q)
            es = Elasticsearch(hosts=settings.ELASTICSEARCH_IPS)
            s = ESearch(using=es,index='article') \
                .filter("range",status={'gte':1}) \
                .filter("match",domain=match[1]) \
                .filter("match",range=int(match[2],16))[self.start:self.end]
            self.response = s.execute()
            template = 'feed/article_es.html'
        elif re.match(r'(?i)@([A-Za-z0-9\.]+)\s+(.+)',self.q):
            match = re.match(r'(?i)@([A-Za-z0-9\.]+)\s+(.+)',self.q)
            es = Elasticsearch(hosts=settings.ELASTICSEARCH_IPS)
            s = ESearch(using=es,index='article') \
                .highlight('document',fragment_size=50) \
                .filter("range",status={'gte':1}) \
                .filter("match",domain=match[1]) \
                .query("match",document=match[2])[self.start:self.end]
            self.response = s.execute()
            template = 'feed/article_es.html'
        elif re.match(r'(?i)@([A-Za-z0-9\.]+)\s*$',self.q):
            match = re.match(r'(?i)@([A-Za-z0-9\.]+)\s*$',self.q)
            es = Elasticsearch(hosts=settings.ELASTICSEARCH_IPS)
            s = ESearch(using=es,index='domain') \
                .query("match",id=match[1])[self.start:self.end]
            self.response = s.execute()
            template = 'feed/domain_es.html'
        else:
            es = Elasticsearch(hosts=settings.ELASTICSEARCH_IPS)
            domain = Domain.translate(self.q[-1])
            if domain is not None:
                if domain in [Domain.DEF,Domain.TNS]: self.q = self.q[:-1]
                s = ESearch(using=es,index='article').highlight('document',fragment_size=50) \
                    .filter("match",domain=domain) \
                    .query("match",document=self.q)[self.start:self.end]
            else:
                s = ESearch(using=es,index='article').highlight('document',fragment_size=50) \
                    .filter(ES_Q("match",domain=Domain.TNS) | ES_Q("match",domain=Domain.INF) | ES_Q("match",domain=Domain.FIN) | ES_Q("match",domain=Domain.DEF)) \
                    .query("match",document=self.q)[self.start:self.end]
            self.response = s.execute()
            template = 'feed/article_es.html'
        context = super().get_context_data(**kwargs)
        context['title'] = 'Idealogs'
        context['subtitle'] = get_subtitle([['Meta',reverse('meta')],['Search']])
        context['placeholder'] = 'Search'
        context['action'] = reverse('search')
        context['template'] = template
        context['autofocus'] = True
        return context


class Meta(CacheMixin,SearchMixin,TemplateView):
    template_name = 'list.html'

    def get_context_data(self,**kwargs):
        self.response = [
            ['About',
                reverse('about'),
                'A good place to start'],
            ['Chopping Block',
                reverse('chopping-block'),
                'Articles that need some TLC...or need to be deleted'],
            ['Contact',
                reverse('contact'),
                'Get in touch'],
            ['Convert',
                reverse('convert'),
                'Convert bibliographic metadata to yaml'],
            ['Developers',
                reverse('developers'),
                'How to contribute'],
            ['Donate',
                reverse('donate'),
                'Idealogs is free to use, but relies on donations and grants to do so'],
            ['FAQ',
                reverse('index'),
                'All your questions, insufficiently answered'],
            ['Privacy',
                reverse('privacy'),
                'Read our Privacy Policy'],
            ['Search',
                reverse('search'),
                'Search idealogs'],
            ['Terms of Service',
                reverse('tos'),
                'Read our Terms of Service'],
        ]
        context = super().get_context_data(**kwargs)
        context['title'] = 'Idealogs'
        context['subtitle'] = 'Meta'
        context['placeholder'] = 'Filter'
        context['action'] = reverse('meta')
        context['template'] = 'feed/meta.html'
        return context


class ChoppingBlock(CacheMixin,SearchMixin,TemplateView):
    template_name = 'list.html'

    def get_context_data(self,**kwargs):
        # result = Article.objects.filter(domain__id__in=Domain.MAIN) \
        #         .exclude(Q(lefts__direction=Link.REWARD) | Q(rights__direction=Link.REWARD)) \
        #         .order_by('created')
        result = Article.objects.filter(domain__id__in=Domain.MAIN) \
                .annotate(link_count=Count('lefts') + Count('rights')) \
                .filter(link_count__lt=5) \
                .order_by('link_count','head__modified')
        if self.q:
            query_list = self.q.split()
            result = result.filter(
                reduce(operator.and_,
                       (Q(header__title__icontains=q) for q in query_list)) |
                reduce(operator.and_,
                       (Q(lede__icontains=q) for q in query_list))
            )
        self.response = result
        context = super().get_context_data(**kwargs)
        context['title'] = 'Idealogs'
        context['subtitle'] = get_subtitle([['Meta',reverse('meta')],['Chopping Block']])
        context['placeholder'] = 'Filter'
        context['action'] = reverse('chopping-block')
        context['template'] = 'feed/article_dj_delete.html'
        return context

class About(RedirectView):

    def get_redirect_url(self,*args,**kwargs):
        return '/@0x0'

class Contact(CacheMixin,FormView):
    form_class = ContactForm
    template_name = 'contact.html'

    def get_context_data(self,**kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Idealogs'
        context['subtitle'] = get_subtitle([['Meta',reverse('meta')],['Contact']])
        return context

    def get_success_url(self):
        return reverse('meta')

class History(CacheMixin,IdealogsMixin,HistoryMixin,SearchMixin,FormView):
    template_name = 'idealogs/history.html'

class Settings(IdealogsMixin,SearchMixin,TemplateView):
    template_name = 'list.html'

    def get_context_data(self,**kwargs):
        result = []
        if self.commit.page != Commit.TALK and self.request.user.is_authenticated:
            result.append(['Watch',
                            self.commit.get_watch_url(),
                            'Control whether you receive notifications when this article is updated'])
        if self.object.status <= Article.SUBMITTED:
            result.append(['Creators',
                            self.commit.get_creators_url(),
                            'Manage/See the people who helped create this article'])
        if self.object.is_deletable(self.request.user,self.role):
            result.append(['Delete',
                            self.commit.get_delete_url(),
                            'Delete this article'])
        self.response = result
        context = super().get_context_data(**kwargs)
        context['title'] = str(self.object)
        context['subtitle'] = get_subtitle([[self.commit.get_subtitle(links=False)],['Settings']])
        context['placeholder'] = 'Filter'
        context['action'] = self.commit.get_settings_url()
        context['template'] = 'feed/meta.html'
        context['idealogs'] = True
        context['settings'] = True
        return context

class Watch(IdealogsMixin,FormView):
    template_name =  'form.html'
    form_class = WatchForm

    def get_context_data(self,**kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = str(self.object)
        context['subtitle'] = get_subtitle([[self.commit.get_subtitle(links=False)],['Settings',self.object.head.get_settings_url()],['Watch']])
        context['idealogs'] = True
        return context

    def get_initial(self):
        initial = super().get_initial()
        initial['watch'] = self.request.user.articles_watching.filter(id=self.object.id).exists()
        return initial

    def get_success_url(self):
        return self.commit.get_settings_url()

class Delete(IdealogsMixin,DeleteMixin,FormView):
    template_name = 'idealogs/confirm_delete.html'

    def get_context_data(self,**kwargs):
        context = super().get_context_data(**kwargs)
        context['subtitle'] = get_subtitle([[self.commit.get_subtitle(links=False)],['Settings',self.object.head.get_settings_url()],['Delete']])
        return context

class Diff(CacheMixin,IdealogsMixin,DiffMixin,TemplateView):
    template_name = 'idealogs/diff.html'

class Action(IdealogsMixin,ActionMixin,FormView):
    template_name = 'idealogs/action.html'

class Changes(CacheMixin,IdealogsMixin,ChangesMixin,FormView):
    template_name = 'idealogs/changes.html'

class Query(QueryMixin,JSONResponseMixin,View):
    none = None

class Save(IdealogsMixin,SaveMixin,JSONResponseMixin,View):
    none = None

class Update(IdealogsMixin,UpdateMixin,HintMixin,FormView):
    template_name = 'idealogs/update.html'

class Stage(IdealogsMixin,StageMixin,FormView):
    template_name = 'idealogs/stage.html'

    def dispatch(self,request,*args,**kwargs):
        if not request.user.is_authenticated:
            return redirect(reverse('detail',kwargs=kwargs))
        return super().dispatch(request,*args,**kwargs)

class Reject(IdealogsMixin,RejectMixin,FormView):
    template_name = 'idealogs/stage.html'

class UpdatePreview(FormPreview):
    form_template = 'idealogs/update.html'
    preview_template = 'idealogs/preview.html'

    #redirects possibly in here?
    def preview_get(self, request):
        "Displays the form"
        if request.user.is_authenticated:
            return redirect(self.commit.get_edit_url())
        elif self.object.status < Article.DEFAULT:
            return redirect(reverse('user:login')+"?next="+self.commit.get_edit_url())
        elif self.commit.page == Commit.KEN and self.object.status == Article.FULL:
            return redirect(self.commit)
        f = self.form(auto_id=self.get_auto_id(),
                      initial=self.get_initial(request))
        return render(request, self.form_template, self.get_context(request, f))

    def preview_post(self, request):
        """
        Validates the POST data. If valid, displays the preview page.
        Else, redisplays form.
        """
        if request.user.is_authenticated:
            return redirect(self.commit.get_edit_url())
        elif self.object.status < Article.DEFAULT:
            return redirect(reverse('user:login')+"?next="+self.commit.get_edit_url())
        f = self.form(request.POST,
                        auto_id=self.get_auto_id(),
                        request=request,
                        object=self.object,
                        commit=self.commit,
                        head=self.head,
                        section=self.section)
        context = self.get_context(request, f)
        if 'action4' in f.data:
            return redirect(self.commit.get_detail_url(section=self.section)+"?log=True")
        elif f.is_valid():
            self.process_preview(request, f, context)
            context['hash_field'] = self.unused_name('hash')
            context['hash_value'] = self.security_hash(request, f)
            return render(request, self.preview_template, context)
        else:
            context['error'] = f.errors['message'][0]
            return render(request, self.form_template, context)

    def post_post(self, request):
        """
        Validates the POST data. If valid, calls done(). Else, redisplays form.
        """
        form = self.form(request.POST,
                        auto_id=self.get_auto_id(),
                        request=request,
                        object=self.object,
                        commit=self.commit,
                        head=self.head,
                        section=self.section)
        if form.is_valid():
            if not self._check_security_hash(
                    request.POST.get(self.unused_name('hash'), ''),
                    request, form):
                return self.failed_hash(request)  # Security hash failed.
            return self.done(request, form.cleaned_data)
        else:
            return render(request, self.form_template, self.get_context(request, form))

    def parse_params(self, request, *args, **kwargs):
        try:
            self = Article.get_article(self,kwargs)
        except:
            raise Http404("Article does not exist")
        self.section = int(kwargs.get('section')) if kwargs.get('section') is not None else None
        self.head = self.object.get_head(page=self.page)
        self.revert = request.GET.get('revert')
        if self.revert is not None:
            self.commit = self.object.get_commit(page=self.page,id=self.revert)
            self.commit.commit_id = None
            self.commit.set_creator(request)
            self.commit.message = ""
            self.commit.status = -0.5
        else:
            self.commit = self.head
        if self.section is not None:
            try:
                self.document = self.commit.sectionify()[self.section].rstrip()
            except:
                return redirect(self.commit.get_anonit_url())
        else:
            self.document = self.commit.document.rstrip()

    def process_preview(self,request,form,context):
        context['commit'].citation = form.cleaned_data['citation']
        context['commit'].status = Commit.STAGED
        self.commit.body = form.cleaned_data['body']
        context['body'] = self.commit.process_body(request,None,None,False)
        specs = [{'string': 'Continue editing',
                    'type': 'submit',
                    'name': 'action2',
                    'style': 'margin-top:1rem;margin-right:.25rem;',
                    'onclick': 'required(false)',},
                 {'string': 'Commit',
                    'type': 'submit',
                    'name': 'action3',
                    'style': 'margin-top:1rem;margin-right:.25rem;',
                    'onclick': 'required(true)',},
                ]
        a, b = buttonify(specs)
        context['button_top'], context['button_bottom'] = str(a), str(b)

    def get_initial(self,request):
        initial = super().get_initial(request)
        initial['head_id'] = self.head.commit_id
        initial['document'] = self.document
        if self.revert is not None:
            initial['message'] = "Reverting back to Commit "+str(self.revert)
        return initial

    def get_context(self,request,form):
        context = super().get_context(request,form)
        context['anonedit'] = True
        context['feed'] = Article.objects.all()
        context['object'] = self.object
        context['commit'] = self.commit
        context['laquo'] = self.commit.get_laquo_section(self.section,request)
        context['raquo'] = self.commit.get_raquo_section(self.section,request)
        context['hints'] = HINTS['Update']
        specs = [{'string': 'Read',
                    'type': 'submit',
                    'name': 'action4',
                    'style': 'margin-top:1rem;margin-bottom:1rem;margin-right:.25rem;',
                    'onclick': 'required(false);',},
                {'string': 'Preview',
                    'type': 'submit',
                    'name': 'action1',
                    'style': 'margin-top:1rem;margin-bottom:1rem;margin-right:.25rem;',
                    'onclick': 'required(true);',},
                ]
        a, b = buttonify(specs)
        context['button_top'], context['button_bottom'] = str(a), str(b)
        return context

    def done(self,request,cleaned_data):
        return redirect(self.commit)

class NotFound(TemplateView):
    template_name = 'meta/notfound.html'

    def dispatch(self,request,*args,**kwargs):
        try:
            self = Article.get_article(self,kwargs)
            if self.object.is_visible(request):
                return redirect(self.object)
        except:
            pass
        return super().dispatch(request,*args,**kwargs)


    def get_context_data(self,**kwargs):
        context = super().get_context_data(**kwargs)
        context['gaykaken'] = gaykaken()
        return context

class Detail(CacheMixin,IdealogsMixin,DetailMixin,FormMixin,TemplateView):
    template_name = 'idealogs/detail.html'

    def post(self, request, *args, **kwargs):
        form = self.get_form()
        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)


class Convert(FormView):
    form_class = B2YForm
    template_name = 'meta/convert.html'

    def get_context_data(self,**kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Idealogs'
        context['subtitle'] = get_subtitle([['Meta',reverse('meta')],['Convert']])
        return context


#TODO use temporaryfile isntead of pipe? actually no, this is probs faster
class B2YJson(JSONResponseMixin,View):

    def get(self,request,*args,**kwargs):
        return self.render_to_response(request.GET)

    def get_data(self, context):
        document = context.get('document', None)
        format = context.get('format', None)
        if document:
            fp = tempfile.NamedTemporaryFile()
            fp.write(document.strip().encode())
            fp.seek(0)
            x = subprocess.run(['pandoc-citeproc','-f',format,'-y',fp.name],
                                stdout=subprocess.PIPE,encoding='utf-8')
            data = {
                'yaml': x.stdout
            }
        else:
            data = {
                'yaml': ''
            }
        return data

def error_404(request):
        context = {'gaykaken': gaykaken()}
        return render(request,'404.html', context)

def error_500(request):
        context = {'gaykaken': gaykaken()}
        return render(request,'500.html', context)

def error_400(request):
        context = {'gaykaken': gaykaken()}
        return render(request,'400.html', context)

def error_403(request):
        context = {'gaykaken': gaykaken()}
        return render(request,'403.html', context)

class Donate(CacheMixin,TemplateView):
    template_name = 'meta/donate.html'

class Developers(CacheMixin,TemplateView):
    template_name = 'meta/developers.html'

class Cookies(CacheMixin,TemplateView):
    template_name = 'meta/cookies.html'

class TOS(CacheMixin,TemplateView):
    template_name = 'meta/tos.html'

class Privacy(CacheMixin,TemplateView):
    template_name = 'meta/privacy.html'

class Confirmation(TemplateView):
    template_name = 'meta/confirmation.html'

class Source(IdealogsMixin,SourceMixin,TemplateView):
    template_name = 'idealogs/source.html'

class Creators(CacheMixin,IdealogsMixin,CreatorsMixin,SearchMixin,NewMixin,FormView):
    form_class = CreatorsForm
    template_name = 'list.html'

    def get_context_data(self,**kwargs):
        self.response =  self.object.creators.all()
        context = super().get_context_data(**kwargs)
        context['title'] = str(self.object)
        context['is_owner'] = self.request.user.is_authenticated and self.object.domain.is_owner(self.request.user)
        if context['is_owner'] and self.commit.is_workable():
            context['subtitle'] = get_subtitle([[self.commit.get_subtitle(links=False)],['Settings',self.object.head.get_settings_url()],['Creators']],[['modify',self.commit.get_new_creator_url()]])
        else:
            context['subtitle'] = get_subtitle([[self.commit.get_subtitle(links=False)],['Settings',self.object.head.get_settings_url()],['Creators']])
        context['action'] = self.commit.get_creators_url()
        context['placeholder'] = 'Filter'
        context['idealogs'] = True
        context['creators'] = True
        context['template'] = 'feed/creators.html'
        return context

class ModifyCreator(IdealogsMixin,OwnerMixin,CreatorsMixin,WorkableMixin,NewMixin,FormView):
    form_class = NewCreatorForm
    template_name = 'idealogs/modify_creator.html'

    def get_context_data(self,**kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = self.object.get_handle()
        context['subtitle'] = get_subtitle([[self.commit.get_subtitle(links=False)],['Settings',self.object.head.get_settings_url()],['Creators',self.object.head.get_creators_url()],['Modify']])
        context['idealogs'] = True
        return context

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['queryset1'] = []
        # creator_set = self.object.domain.creators.all()
        # creator_set = creator_set.union(self.object.creators.all())
        # creator_set = creator_set.order_by('-user').distinct('-user')
        creator_set = Creator.objects.filter(Q(article=self.object) | Q(domain=self.object.domain)).order_by('user').distinct('user')
        for creator in creator_set.iterator():
            if not self.object.creators.filter(user=creator.user,role=Creator.EDITOR).exists():
                kwargs['queryset1'].append((creator.id,str(creator.user)))

        kwargs['queryset2'] = []
        if not self.object.domain.is_user():
            kwargs['queryset2'].append((Creator.APPROVER,"approver"))
        if self.object.status == Article.PRIVATE:
            kwargs['queryset2'].append((Creator.READER,"reader"))
        elif self.object.status == Article.PUBLIC:
            kwargs['queryset2'].append((Creator.REQUESTER,"requester"))
        kwargs['queryset2'].append((Creator.WRITER,"writer"))
        kwargs['queryset2'].append((Creator.EDITOR,"editor"))
        kwargs['queryset2'].append((Creator.NONE,"none"))
        return kwargs
