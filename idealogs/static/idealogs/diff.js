var limit = 2;
$('input.commit').on('change', function(evt) {
   if ($("input[name='commits']:checked").length > limit) {
       this.checked = false;
   }
});

function mySubmit(){
  if ($("input[name='commits']:checked").length != 2) {
      return false;
  }
  return true;
}

$('.changes-button').click(function(){
  $('.changes-form').submit()
});
