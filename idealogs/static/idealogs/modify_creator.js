function newCreator(){
  if (document.getElementById("id_role").value == "5"){
    return confirm('Are you sure you want to add this user as an editor? An editor cannot be removed and their role cannot be modified.');
  }
  else if (document.getElementById("id_role").value == "0"){
    return confirm('Are you sure you sure you want to remove all privileges for this user? The user will no longer have access to the project. Also, any uncommitted changes \(including requests\) will be deleted.');
  }
}
