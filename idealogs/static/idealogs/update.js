autosize($('#id_document'));

$(".save-button").click(function (e) {
  e.preventDefault();
  var form = $(this).closest("form");
  disable()
  $.ajax({
    type: "POST",
    url: form.attr("save-url"),
    data: form.serialize(),
    dataType: 'json',
    success: function (data) {
      if (data.success){
        if (data.reload){
          location.reload();
        }
        else if (data.redirect){
          window.location.href = data.redirect;
        }
        DOCUMENT = data.document;
        $(".expandingArea").removeClass('save-invalid-body')
        $(".update-search").removeClass('save-invalid-search')
        $(".commit-message").addClass('commit-message-hidden')
      }
      else{
        enable();
        $(".expandingArea").addClass('save-invalid-body')
        $(".update-search").addClass('save-invalid-search')
        $(".commit-message-error").text(" **formatting error (I think)**")
        $(".commit-message").removeClass('commit-message-hidden')
      }
      if (data.updatelog){
        log();
      }
    }
  });
});

function get_document(){
  return $("#id_document").val();
}

function log(){
  $(".commit-message").addClass('commit-message-hidden');
  $(".commit-id").text("None");
  $(".commit-status").text("saved");
  $(".commit-status").css("color","red");
}

function hide(){
  SAVE = HIDDEN
  $(".save-button").each(function(){
    $(this).css('visibility','hidden')
    $(this).attr('disabled',false)
  });
  $(".aquo").each(function(){
    $(this).removeClass("aquo-disabled");
    $(this).addClass("aquo-enabled");
  });
}

function enable(){
  SAVE = ENABLED
  $(".save-button").each(function(){
    $(this).attr('disabled',false);
  });
  $(".read-button").each(function(){
    $(this).attr('disabled',true);
  });
  $(".aquo").each(function(){
    $(this).removeClass("aquo-enabled");
    $(this).addClass("aquo-disabled");
  });
}

function disable(){
  SAVE = DISABLED
  $(".save-button").each(function(){
    $(this).attr('disabled',true)
  });
  $(".read-button").each(function(){
    $(this).attr('disabled',false);
  });
  $(".aquo").each(function(){
    $(this).removeClass("aquo-disabled");
    $(this).addClass("aquo-enabled");
  });
}


var ENABLED = 0
var DISABLED = 1
var SAVE = DISABLED
var DOCUMENT = get_document()
function save(area){
  if (SAVE === ENABLED && area.value === DOCUMENT){ disable() }
  else if (SAVE === DISABLED && area.value != DOCUMENT){ enable() }
}

function submitConfirm(){
  if (SAVE == ENABLED){
    return confirm('It looks like you have some unsaved changes. If you leave before saving, your changes will be lost.')
  }
  else{
    return true;
  }
}

$(document).ready(function () {
  $(window).bind('keydown', function(event) {
      if (event.ctrlKey || event.metaKey) {
          switch (String.fromCharCode(event.which).toLowerCase()) {
          case 's':
              event.preventDefault();
              if (SAVE == ENABLED){
                $(".save-button").first().click()
              }
              break;
          }
      }
  });
});


  // var area = container.querySelectorAll('textarea')[1];
  var area = document.getElementById('id_document');
//   var span = container.querySelector('span');
  if (area.addEventListener) {
  area.addEventListener('input', function() {
  save(area)
//   span.textContent = area.value;
  }, false);
//   span.textContent = area.value;
  } else if (area.attachEvent) {
//   // IE8 compatibility
  area.attachEvent('onpropertychange', function() {
  save(area)
//   span.innerText = area.value;
  });
//   span.innerText = area.value;
   }

function required(value){
  $('.required-field').each(function(){
    $(this).prop('required',value)
  });
  // document.getElementById("id_document").required = value;
}
