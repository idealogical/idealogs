SEARCHING = false;
var QUERY = "";
var FINISHED;
var FOUND;
var RESULTS;
var CURRENT;
var PAGE;
var PAGE_SIZE;
var NEW;

function reset(){
  FINISHED = false;
  RESULTS = [];
  CURRENT = 0;
  PAGE = 0;
  if (location.pathname === "/new"){
      NEW = 1;
  }
  else{
    NEW = 0;
  }

}


function getMore(){
  $.ajax({
    type: "GET",
    url: "/query",
    data: {'query': QUERY,
            'page': PAGE,
            'new': NEW},
    dataType: 'json',
    success: function (data) {
      RESULTS = RESULTS.concat(data.results)
      FINISHED = data.finished;
    }
  });
}

function buttonify(index){
  //aquo
  button = $("<button>");
  button.attr("type","button");
  button.attr("onclick","setResult("+(index-1).toString()+")");
  button.append("&laquo;");
  if (index == 0){
    button.attr("disabled","");
  }
  $(".query-laquo").html(button);
  button = $("<button>");
  button.attr("type","button");
  button.attr("onclick","setResult("+(index+1).toString()+")");
  button.append("&raquo;");
  if (index == (RESULTS.length - 1)){
    button.attr("disabled","");
  }
  $(".query-raquo").html(button);
}

function titleify(index,idea){
  var span = $("<span>");
  var a = $("<a>");
  a.attr("href","/"+idea);
  a.text(RESULTS[index].title)
  span.html(a);
  $(".query-item").html(span);
}

function confirmArticle(event){
  if (!confirm("Are you sure you want to create this article?  Idealogs demands editors who create and edit articles in good faith.  With great power comes great responsibility :)")){
      event.preventDefault();
  }
}

function setResult(index){
  if (index > (RESULTS.length-1) || index < 0){
    return;
  }

  if (FOUND){
    //category
    var button = $("<button>");
    button.addClass("query-domain");
    if (NEW && index == 0){
      button.attr("type","submit");
      button.text("submit");
      button.attr("onclick","confirmArticle(event);");
    }
    else{
      var idea = "@"+RESULTS[index].domain+"x"+RESULTS[index].range;
      button.attr("type","button");
      button.attr("onclick","copyStringToClipboard(event)");
      button.text(idea);
    }
    $(".query-item-category").html(button);

    //buttons
    buttonify(index);

    //title
    if (NEW && index ==0){
      $(".query-item").text(RESULTS[index].title);
    }
    else{
      titleify(index,idea);
    }

    //description
    $(".query-item-description").html(RESULTS[index].description);
  }
  else{
    $(".query-item-category").html("");
    buttonify(index);
    $(".query-item").text("");
    $(".query-item-description").html(RESULTS[index].description);
  }

  //get more results
  if ((RESULTS.length - index) <= PAGE_SIZE/2 && !FINISHED){
    PAGE = PAGE + 1;
    getMore()
  }
  CURRENT = index
}

function queryDisable(){
  $( "#query" ).removeClass('query-enabled');
  $( "#query" ).addClass('query-disabled');
}

function queryEnable(){
  $( "#query" ).removeClass('query-disabled');
  $( "#query" ).addClass('query-enabled');
}

$( "#id_search" ).keydown(function(e) {
  if (e.keyCode == 13) {
      e.preventDefault();
      var temp = $( this ).val().trim()
      if (temp && QUERY != temp){
        reset()
        QUERY = temp;
        SEARCHING = true;
        $.ajax({
          type: "GET",
          url: "/query",
          data: {'query': QUERY,
                  'page': PAGE,
                  'new': NEW,},
          dataType: 'json',
          success: function (data) {
            RESULTS = data.results;
            FINISHED = data.finished;
            FOUND = data.found;
            PAGE_SIZE = data.page_size;
            NEW = data.new;
            setResult(0)
            queryEnable()
          }
        });
      }
    }
});

$( "#id_search" ).keyup(function(e) {
  if (!$( this ).val().trim() && SEARCHING){
    QUERY = ""
    SEARCHING = false;
    queryDisable()
  }
});

function copyStringToClipboard (event) {
   // Create new element
   var el = document.createElement('textarea');
   // Set value (string to be copied)
   el.value = event.target.textContent;
   // Set non-editable to avoid focus and move outside of view
   el.setAttribute('readonly', '');
   el.style = {position: 'absolute', left: '-9999px'};
   document.body.appendChild(el);
   // Select text inside element
   el.select();
   // Copy text to clipboard
   document.execCommand('copy');
   // Remove temporary element
   document.body.removeChild(el);
}

if ($("#id_search").val() != ""){
  var e = jQuery.Event("keydown");
  e.keyCode = 13; // # Some key code value
  $("#id_search").trigger(e);
}
