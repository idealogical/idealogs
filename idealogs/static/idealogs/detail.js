var ANCHORS;
var ANCHORS2;
var SECTIONS;
var SECTION;
$(document).ready(function (event) {
  ANCHORS = $("#TOC > div.toc > ul > li > a")
  ANCHORS2 = $("#TOC > div.toc > ul > li > ul > li")
  if (ANCHORS){
    SECTIONS = $(".level2")
    var i = 1;
    ANCHORS.each(function() {
      $( this ).attr("onclick","contextSwitch(event,"+i.toString()+",true)");
      i += 1;
    });
    ANCHORS2.each(function(){
    $( this ).attr("onclick","contextSwitch(event,"+($( this ).parent().parent().index()+1).toString()+",false)");
    });
  }

  if (location.pathname.includes('/context/')){
    SECTION = parseInt(location.pathname.match(/\/context\/(\d+)/)[1]);
  }
  else{
    SECTION = 0;
  }
  if (SECTION > 0 && !location.href.includes("#")){
    // location.href += ANCHORS.eq(SECTION-1).attr('href'); // This is a hack, for some reason just clicking by itself doesn't work as expected
    history.scrollRestoration = 'manual';
    ANCHORS.eq(SECTION-1).click()
  }
});

function contextSwitchPrev(event){

  contextSwitch(event,SECTION-1,true)
}
function contextSwitchNext(event){
  contextSwitch(event,SECTION+1,true)
}

function contextSwitch(event,c,prevent){
  if (c < 0 || c > ANCHORS.length){ event.preventDefault();return; }
  if (prevent){ event.preventDefault() }

  if (ANCHORS){
    SECTION = c
    if (c==0){
      SECTIONS.each(function() { $( this ).show(); });
      ANCHORS.each(function() { $( this ).removeClass("toc-active")})
      $("#context-heading").addClass("toc-active")
      var path = location.pathname.replace(/\/context\/\d*/,"");
      history.replaceState(null,null,path)
      $(".context-nav").addClass("context-nav-hidden");
      $(".context-nav").empty();
    }
    else{
      $("#context-heading").removeClass("toc-active")
      toggleNav(c);
      SECTIONS.each(function() {
        if ($( this ).attr('id') === SECTIONS[c-1].id){
          $( this ).show()
        }
        else{
          $( this ).hide()
        }
      });
      ANCHORS.each(function() {
        if ($( this ).attr('href') === "#"+SECTIONS[c-1].id){
          $( this ).addClass("toc-active")
        }
        else{
          $( this ).removeClass("toc-active")
        }
      });

      //replaceState
      var path;
      path = location.pathname.replace(/\/context\/\d*/,"")+"/context/"+c.toString();
      history.replaceState(null,null,path)
      $(document).scrollTop( $("#"+SECTIONS[c-1].id).offset().top + 15 );
    }
  }
}

function toggleNav(c){
  $(".context-nav").empty();
  a = $("<a>");
  a.attr("href","#TOC");
  a.attr("onclick","contextSwitch(event,"+(c-1).toString()+",true)");
  a.append("&laquo;")
  if (c == 1){
    a.attr("class","aquo-disabled")
  }
  $(".context-nav").append(a)
  a = $("<a>");
  a.attr("href","#TOC")
  a.attr("onclick","myScrollTop(event);")
  a.attr("class","context-nav-top")
  a.append("Top")
  $(".context-nav").append(a)
  a = $("<a>");
  a.attr("href","#TOC");
  a.attr("onclick","contextSwitch(event,"+(c+1).toString()+",true)");
  a.append("&raquo;")
  if (c == ANCHORS.length){
    a.attr("class","aquo-disabled")
  }
  $(".context-nav").append(a)
  $(".context-nav").removeClass("context-nav-hidden");
}

function toggleLog(event){
  event.preventDefault()
  $('.log-info').each(function(){
    if ($(this).hasClass('log-inactive')){
      $(this).removeClass('log-inactive')
    }
    else{
      $(this).addClass('log-inactive')
    }
  });
  if ($('.log-info').first().hasClass('log-inactive')){
    path = location.pathname.replace(/\/\?log=(True|False)/,"")+"?log=False";
  }
  else{
    path = location.pathname.replace(/\/\?log=(True|False)/,"")+"?log=True";
  }
  history.replaceState(null,null,path)
}

function myScrollTop(event){
  event.preventDefault();
  $(document).scrollTop(0);
}
