from django.conf import settings
from django.utils import timezone
from elasticsearch import Elasticsearch
from elasticsearch_dsl import Document, Date, Integer, Float, Keyword, Text, Index
from elasticsearch_dsl.connections import connections
from fnmatch import fnmatch
from . import models
import pdb

# ALIAS = 'article'
# PATTERN = ALIAS + '-*'

class Article(Document):
    document = Text(analyzer='english')
    lede = Text()
    status = Integer()
    title = Text(fields={'raw': Keyword()})
    domain = Text()
    range = Integer()
    modified = Date()


    @classmethod
    def _matches(cls, hit):
        # override _matches to match indices in a pattern instead of just ALIAS
        # hit is the raw dict as returned by elasticsearch
        return fnmatch(hit['_index'], 'article-*')

    class Index:
        name = 'article'
        settings = {
          "number_of_shards": 2,
          "number_of_replicas": 1,
        }

class Commit(Document):
    idea = Text()
    domain = Text()
    article_status = Integer()
    page = Text()
    commit_id = Integer()
    status = Text()
    user = Text()
    role = Text()
    document = Text(analyzer='english')
    title = Text()
    message = Text(analyzer='english')
    modified = Date()
    committed = Date()

    @classmethod
    def _matches(cls, hit):
        # override _matches to match indices in a pattern instead of just ALIAS
        # hit is the raw dict as returned by elasticsearch
        return fnmatch(hit['_index'], 'commit-*')

    class Index:
        name = 'commit'
        settings = {
          "number_of_shards": 2,
          "number_of_replicas": 1,
        }

class Link(Document):
    score = Float()
    direction = Integer()
    modified = Date()
    l_doc = Text(analyzer='english')
    l_domain = Text()
    l_range = Integer()
    l_title = Text()
    l_lede = Text()
    r_doc = Text(analyzer='english')
    r_domain = Text()
    r_range = Integer()
    r_title = Text()
    r_lede = Text()

    @classmethod
    def _matches(cls, hit):
        # override _matches to match indices in a pattern instead of just ALIAS
        # hit is the raw dict as returned by elasticsearch
        return fnmatch(hit['_index'], 'link-*')

    class Index:
        name = 'link'
        settings = {
          "number_of_shards": 2,
          "number_of_replicas": 1,
        }

def setup(ALIAS,search_cls,model_cls):
    """
    Create the index template in elasticsearch specifying the mappings and any
    settings to be used. This can be run at any time, ideally at every new code
    deploy.
    """
    PATTERN = ALIAS + '-*'

    # initiate the default connection to elasticsearch
    connections.create_connection(hosts=settings.ELASTICSEARCH_IPS)

    # create an index template
    index_template = search_cls._index.as_template(ALIAS, PATTERN)

    # upload the template into elasticsearch
    # potentially overriding the one already there
    index_template.save()

    # create the first index if it doesn't exist
    if not search_cls._index.exists():
        migrate(ALIAS,move_data=False)

        #TODO can't seem to get bulk to work, keep getting error: 'Article' object has no attribute 'copy'
        # bulk(es,(a.to_search() for a in model_cls.objects.all().iterator()))
        for a in model_cls.objects.all().iterator():
            a.to_search().save()

def migrate(ALIAS,move_data=True, update_alias=True):
    """
    #TODO doesn't migrate the way I think it should (if you delete/modify a field from the Document, it is still there), not sure what purpose this is for, ask on stackoverflow
    Upgrade function that creates a new index for the data. Optionally it also can
    (and by default will) reindex previous copy of the data into the new index
    (specify ``move_data=False`` to skip this step) and update the alias to
    point to the latest index (set ``update_alias=False`` to skip).
    Note that while this function is running the application can still perform
    any and all searches without any loss of functionality. It should, however,
    not perform any writes at this time as those might be lost.
    """
    PATTERN = ALIAS + '-*'

    # construct a new index name by appending current timestamp
    next_index = PATTERN.replace('*', timezone.now().strftime('%Y%m%d%H%M%S%f'))

    # get the low level connection
    es = Elasticsearch(hosts=settings.ELASTICSEARCH_IPS)

    # create new index, it will use the settings from the template
    es.indices.create(index=next_index)

    if move_data:
        # move data from current alias to the new index
        es.reindex(
            body={"source": {"index": ALIAS}, "dest": {"index": next_index}},
            request_timeout=3600
        )
        # refresh the index to make the changes visible
        es.indices.refresh(index=next_index)

    if update_alias:
        # repoint the alias to point to the newly created index
        es.indices.update_aliases(body={
            'actions': [
                {"remove": {"alias": ALIAS, "index": PATTERN}},
                {"add": {"alias": ALIAS, "index": next_index}},
            ]
        })

def flush(ALIAS):
    PATTERN = ALIAS + '-*'
    connections.create_connection(hosts=settings.ELASTICSEARCH_IPS)
    Index(PATTERN).delete()
