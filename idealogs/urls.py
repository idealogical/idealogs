from django.conf import settings
from django.conf.urls import url, include
from django.contrib import admin
from django.contrib.auth import urls as auth_urls
from django.views.defaults import page_not_found
from . import views
from .forms import AnonUpdateForm
from .models import Article
from django.views.generic.base import RedirectView
from django.conf.urls import handler404, handler500, handler403, handler400
from django.contrib.auth.decorators import login_required
import notifications.urls
import pdb

#TODO implement signup redirect to welcome page

favicon_view = RedirectView.as_view(url='/static/favicon.ico', permanent=True)

urlpatterns = [
    # url(r'^404$',page_not_found,kwargs={'exception': Exception('Page not Found')},name="404"),
    url(r'^$',views.Index.as_view(),name="index"),
    url(r'^new$',views.New.as_view(),name="new"),
    url(r'^inbox/notifications/', include(notifications.urls, namespace='notifications')),
    url(r'^privacy$',views.Privacy.as_view(),name="privacy"),
    url(r'^tos$',views.TOS.as_view(),name="tos"),
    url(r'^cookies$',views.Cookies.as_view(),name="cookies"),
    url(r'^developers$',views.Developers.as_view(),name="developers"),
    url(r'^donate$',views.Donate.as_view(),name="donate"),
    url(r'^donate/confirmation$',views.Confirmation.as_view(),name="confirmation"),
    url(r'^b2yjson$', views.B2YJson.as_view(), name='b2yjson'),
    url(r'^convert$', views.Convert.as_view(), name='convert'),
    url(r'^admin/', admin.site.urls),
    url(r'^favicon.ico$', favicon_view),
    url(r'^search$',views.Search.as_view(),name="search"),
    url(r'^query$',views.Query.as_view(),name="query"),
    url(r'^meta$',views.Meta.as_view(),name="meta"),
    url(r'^chopping-block$',views.ChoppingBlock.as_view(),name="chopping-block"),
    url(r'^contact',views.Contact.as_view(),name="contact"),
    url(r'^about',views.About.as_view(),name="about"),

    url(Article.IDea+'/settings$',views.Settings.as_view(),name="settings"),
    url(Article.IDea+'/settings/delete$',views.Delete.as_view(),name="delete"),
    url(Article.IDea+'/settings/watch$',login_required(views.Watch.as_view()),name="watch"),
    url(Article.IDea+'/settings/(?P<page>talk)/creators$',views.Creators.as_view(),name="creators"),
    url(Article.IDea+'/settings/(?P<page>talk)/creators/modify$',views.ModifyCreator.as_view(),name="modify-creator"),
    url(Article.IDea+'/settings/creators$',views.Creators.as_view(),name="creators"),
    url(Article.IDea+'/settings/creators/modify$',views.ModifyCreator.as_view(),name="modify-creator"),

    url(Article.IDea+'/(?P<page>talk)/history$',views.History.as_view(),name="history"),
    url(Article.IDea+'/history$',views.History.as_view(),name="history"),

    url(Article.IDea+'/(?P<page>talk)/(?P<commit_id>-?\d+)/action$',login_required(views.Action.as_view()),name='action'),
    url(Article.IDea+'/(?P<page>talk)/action$',login_required(views.Action.as_view()),name='action'),
    url(Article.IDea+'/(?P<commit_id>-?\d+)/action$',login_required(views.Action.as_view()),name='action'),
    url(Article.IDea+'/action$',login_required(views.Action.as_view()),name='action'),

    url(Article.IDea+'/(?P<page>talk)/stage$',login_required(views.Stage.as_view()),name="stage"),
    url(Article.IDea+'/stage$',login_required(views.Stage.as_view()),name="stage"),

    url(Article.IDea+'/(?P<page>talk)/reject/(?P<commit_id>-\d+)$',login_required(views.Reject.as_view()),name="reject"),
    url(Article.IDea+'/reject/(?P<commit_id>-\d+)$',login_required(views.Reject.as_view()),name="reject"),

    url(Article.IDea+'/(?P<page>talk)/anon-edit$',views.UpdatePreview(AnonUpdateForm),name="anon-edit"),
    url(Article.IDea+'/(?P<page>talk)/anon-edit/(?P<section>\d+)$',views.UpdatePreview(AnonUpdateForm),name="anon-edit"),
    url(Article.IDea+'/anon-edit$',views.UpdatePreview(AnonUpdateForm),name="anon-edit"),
    url(Article.IDea+'/anon-edit/(?P<section>\d+)$',views.UpdatePreview(AnonUpdateForm),name="anon-edit"),

    url(Article.IDea+'/source$',views.Source.as_view(),name="source"),
    url(Article.IDea+'/(?P<page>talk)/edit$',views.Update.as_view(),name="edit"),
    url(Article.IDea+'/(?P<page>talk)/edit/(?P<section>\d+)$',views.Update.as_view(),name="edit"),
    url(Article.IDea+'/edit$',views.Update.as_view(),name="edit"),
    url(Article.IDea+'/edit/(?P<section>\d+)$',views.Update.as_view(),name="edit"),


    url(Article.IDea+'/(?P<page>talk)/save$',views.Save.as_view(),name="save"),
    url(Article.IDea+'/(?P<page>talk)/save/(?P<section>\d+)$',views.Save.as_view(),name="save"),
    url(Article.IDea+'/save$',views.Save.as_view(),name="save"),
    url(Article.IDea+'/save/(?P<section>\d+)$',views.Save.as_view(),name="save"),



    url(Article.IDea+'/(?P<page>talk)/changes/invalid/(?P<older_id>-?\d+)$',views.Diff.as_view(),name='diff'),
    url(Article.IDea+'/changes/invalid/(?P<older_id>-?\d+)$',views.Diff.as_view(),name='diff'),
    url(Article.IDea+'/(?P<page>talk)/changes/invalid$',login_required(views.Diff.as_view()),name='diff'),
    url(Article.IDea+'/changes/invalid$',login_required(views.Diff.as_view()),name='diff'),
    url(Article.IDea+'/(?P<page>talk)/changes/(?P<newer_id>-?\d+)/(?P<older_id>-?\d+)$',views.Diff.as_view(),name='diff'),
    url(Article.IDea+'/changes/(?P<newer_id>-?\d+)/(?P<older_id>-?\d+)$',views.Diff.as_view(),name='diff'),

    url(Article.IDea+'/(?P<page>talk)/changes/(?P<commit_id>-?\d+)$',views.Changes.as_view(),name="changes"),
    url(Article.IDea+'/(?P<page>talk)/changes$',views.Changes.as_view(),name="changes"),
    url(Article.IDea+'/changes/(?P<commit_id>-?\d+)$',views.Changes.as_view(),name="changes"),
    url(Article.IDea+'/changes$',views.Changes.as_view(),name="changes"),

    url(Article.IDea+'/(?P<page>talk)/changes/(?P<commit_id>-?\d+)/context/(?P<section>\d+)$',views.Changes.as_view(),name="changes"),
    url(Article.IDea+'/(?P<page>talk)/changes/context/(?P<section>\d+)$',views.Changes.as_view(),name="changes"),
    url(Article.IDea+'/changes/(?P<commit_id>-?\d+)/context/(?P<section>\d+)$',views.Changes.as_view(),name="changes"),
    url(Article.IDea+'/changes/context/(?P<section>\d+)$',views.Changes.as_view(),name="changes"),

    url(Article.IDea+'/(?P<page>talk)/(?P<commit_id>-?\d+)/context/(?P<section>\d+)$',views.Detail.as_view(),name="detail"),
    url(Article.IDea+'/(?P<page>talk)/context/(?P<section>\d+)$',views.Detail.as_view(),name="detail"),
    url(Article.IDea+'/(?P<commit_id>-?\d+)/context/(?P<section>\d+)$',views.Detail.as_view(),name="detail"),
    url(Article.IDea+'/context/(?P<section>\d+)',views.Detail.as_view(),name="detail"),

    url(Article.IDea+'/(?P<page>talk)/(?P<commit_id>-?\d+)$',views.Detail.as_view(),name="detail"),
    url(Article.IDea+'/(?P<page>talk)$',views.Detail.as_view(),name="detail"),
    url(Article.IDea+'/(?P<commit_id>-?\d+)$',views.Detail.as_view(),name="detail"),
    url(Article.IDea+'$',views.Detail.as_view(),name="detail"),

    url(r'',include('user.urls',namespace="user")),
    url(r'',include(auth_urls)),
]

handler404 = views.error_404
handler500 = views.error_500
handler400 = views.error_400
handler403 = views.error_403
