from django.conf import settings

def mylocal(request):
    if request.user.is_anonymous:
        return {'UTC': True,}
    else:
        return {'UTC': False,}

def debug(context):
  return {'DEBUG': settings.DEBUG}
