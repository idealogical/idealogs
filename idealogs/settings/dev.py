from .base import *

DEBUG = True

ALLOWED_HOSTS = ['*','localhost','10.0.2.15',]

SECRET_KEY = "o@k9+!m_*z@q#y!ol@5k&+maa8!i+cwaw1*nh%!@+3u6x%)^tw"

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'idealogs',
        'USER': 'idealog',
        'PASSWORD': 'idealog',
        'HOST': 'localhost',
        'PORT': '',
    }
}

ELASTICSEARCH_IPS = ['localhost']

EMAIL_FILE_PATH = os.path.join(BASE_DIR, "sent_emails")
# STATIC_ROOT = os.path.join(BASE_DIR, 'static/')
STATICFILES_DIRS = [STATIC_DIR,]

TRANSLATION_SERVER = "http://192.168.1.248:1969/"
# TRANSLATION_SERVER = "http://192.168.50.92:1969/"
