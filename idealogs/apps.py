from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _

class Config(AppConfig):
    name = 'idealogs'
    verbose_name = _('idealogs')

    def ready(self):
        import idealogs.signals
