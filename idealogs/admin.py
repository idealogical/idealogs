from django.contrib import admin
from idealogs.models import Article, Commit, Link

admin.site.register(Article)
admin.site.register(Commit)
admin.site.register(Link)
