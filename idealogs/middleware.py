import pytz
import pdb
from django.utils import timezone
from django.utils.deprecation import MiddlewareMixin
from django.conf import settings

class TimezoneMiddleware(MiddlewareMixin):
    def process_request(self, request):
        if not request.user.is_anonymous:
            tzname = request.user.timezone
            timezone.activate(tzname)
        else:
            timezone.deactivate()
