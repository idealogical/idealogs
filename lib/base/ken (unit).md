The ken is a measure of the value of a statement.

## Definition
The ken ($K$) of a statement ($s$) is defined according to the following equation:  $$K_s = \frac{Q_a}{Q_r},$$ where $Q_a$ and $Q_r$ are the number of questions answered and raised, respectively, by $s$.  Note: if a statement answers a question that it raises, then that counts as 1 $Q_a$ and 0 $Q_r$.

## Etymology
Ken (noun) means one's range of knowledge or sight.  Hence, the name.

## Usage
The ken relates the number of questions that a statement answers to the number of questions that it raises. A statement that raises many questions but answers few will have a ken approaching 0, while a statement that raises few questions and answers many will have a ken approaching infinity.  A statement with a ken of 1 raises as many questions as it answers.
