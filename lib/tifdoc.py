to='html5+smart'
format='markdown+tex_math_single_backslash'
filters=['pandoc-sidenote','pandoc-citeproc']
extra_args=['--mathjax','--section-divs','--template=templates/pandoc.html','--toc',]

from pypandoc import convert_text
from idealogs.models import Article, Domain, Commit, Publish, Link
from user.models import Creator
from bs4 import BeautifulSoup
from urllib.parse import unquote,quote
from django.db import IntegrityError, transaction
from django.db.models import F, Q
from django.contrib.auth import get_user_model
from django.utils import timezone
from lib.utils import Object, hex_upper
from notifications.signals import notify
import subprocess
import hashlib
import requests
import datetime
import tempfile
import bleach
import copy
import yaml
import pickle
import base64
from lib.utils import MyYAML
import pdb
import re

class Tifdoc:
    PREVIEW = 0
    SAVE = 1
    COMMIT = 2
    PUBLISH = 3

    def __init__(self,article=None,head=None,commit=None,doc_id=None,request=None,page=None,document="",message=None,invalid=False,status=None,parent=None):
        self.object = article
        self.head = head
        self.commit = commit
        self.request = request
        self.status = status
        self.parent = parent
        self.new_targets = set()
        self.new_targets_dict = {}
        self.new_tags = get_user_model().objects.none()
        self.reload = False
        document = document.replace("\r\n","\n")
        if not invalid and message:
            message = bleach.clean(message)

        if not self.head: #initial commit
            self.object.save()
            if self.object.status in range(Article.PRIVATE,Article.OPEN+1) and self.request.user.is_authenticated:
                creator = Creator.objects.get(user=self.request.user,role=Creator.EDITOR)
                self.object.creators.add(creator)
            self.commit = Commit(article=self.object,
                                commit_id=0,
                                status=status,
                                page=page,
                                message="initial commit",)
            self.commit.set_creator(self.request)
            if page in [Commit.KEN,Commit.WORK]:
                # myyaml = MyYAML(typ='safe')
                myyaml = MyYAML() #TODO not calling safe because it was dumping weirdly with '{}'s showing, possible security flaw
                myyaml.indent(mapping=2, sequence=4, offset=2)
                self.commit.document = "---\n"+myyaml.dump(self.object.header).strip()+"\n---\n\n"
            else:
                self.commit.document = ""
        elif self.head == self.commit and self.status in [Commit.INVALID,Commit.SAVED,Commit.STAGED,Commit.HEAD]: #anonupdate or saving for first time
            if self.status == Commit.HEAD: assert not self.request.user.is_authenticated
            self.commit = Commit(article=self.object,
                                status=status,
                                page=self.head.page,
                                document=document,
                                message=message,
                                parent=self.head.commit_id)
            self.commit.set_creator(self.request)
        else:
            if (self.status == Commit.SAVED and self.commit.status <= Commit.INVALID) or (self.status == Commit.HEAD and self.commit.status == Commit.REQUESTED):
                self.commit.parent = self.head.commit_id
            elif self.status == Commit.INVALID and self.parent is not None:
                self.commit.parent = self.parent
            self.commit.document = document
            self.commit.message = message
            self.commit.status = status
        self.get_doc(doc_id)
        if self.status in range(Commit.INVALID,Commit.HEAD+1):
            self.tifdoc()
        self.save()

    TALK = 1
    DFT = 2
    # def get_doc(self,doc_id):
    #     if not doc_id:
    #         return
    #     if doc_id == Domain.DEF:
    #         fp = open('lib/base/d.md', 'r')
    #     elif doc_id == Domain.TNS:
    #         fp = open('lib/base/t.md', 'r')
    #     elif doc_id == Domain.INF:
    #         fp = open('lib/base/i.md', 'r')
    #     elif doc_id == Domain.FIN:
    #         fp = open('lib/base/f.md', 'r')
    #     elif doc_id == Tifdoc.TALK:
    #         fp = open('lib/base/talk.md', 'r')
    #     elif doc_id == Tifdoc.DFT:
    #         fp = open('lib/base/draft.md', 'r')
    #     self.commit.document += fp.read().rstrip('\n')
    #     fp.close()

    def get_doc(self,doc_id):
        if doc_id == Domain.TNS:
            self.commit.document += "## Reference"

    def tifdoc(self):
        self.preprocess()
        self.raw = convert_text(self.document,to=to,format=format,filters=filters,extra_args=extra_args)
        # convert_file('tufte.md',to=to,format=format,filters=filters,extra_args=extra_args,outputfile="tufte5.html")
        self.postprocess()

    #TODO make atomic, assert things haven't changed then go into saving shtuff
    def save(self):
        with transaction.atomic():
            self.lock1 = self.object.queryset().select_for_update().get() if self.object.id is not None else None
            self.lock2 = self.commit.queryset().select_for_update().get() if self.commit.id is not None else None
            if (self.lock2 and self.commit.modified != self.lock2.modified) \
                or (self.commit.page == Commit.TALK and self.lock1 and self.lock1.talk_head != self.object.talk_head) \
                or (self.commit.page != Commit.TALK and self.lock1 and self.lock1.head != self.object.head):
                raise ValidationError("something's not right")
            if self.status == Commit.SUBMITTED:
                self.commit.save()
                domain = Domain.objects.get(id=Domain.TNS)
                self.new_object = Article(domain=domain,
                                            range=domain.range(),
                                            status=Article.DEFAULT,
                                            hash=self.object,
                                            header={'title': self.object.header['title']},
                                            citation = self.commit.get_citation())
                #set header
                self.new_object.initialize(doc_id=Domain.TNS,
                                        page=Commit.KEN,
                                        request=self.request)
                self.object.status = Article.SUBMITTED
                self.object.hash = self.new_object
                temp_talk_head = self.object.talk_head
                self.object.talk_head = None
                self.object.save()

                self.new_object.talk_head = temp_talk_head
                self.new_object.save()
                #can't use update() because of post_save for search
                for commit in self.object.commits.filter(page=Commit.TALK):
                    commit.article = self.new_object
                    commit.save()
            elif self.status == Commit.PUBLISHED:
                creator = Creator.objects.get(user=self.request.user,role=Creator.EDITOR)
                self.commit.publish.editors.add(creator)
                if self.commit.publish.editors.count() == self.object.creators.filter(role=Creator.EDITOR).count():
                    self.commit.published = timezone.now()
                    self.commit.publish.editors.clear()
                    self.commit.publish.delete()
                    self.commit.save()
                    self.object.status = Article.PUBLISHED
                    self.object.save()
                    invalid = self.object.commits.filter(page=self.commit.page,status__lte=Commit.SAVED)
                    for commit in invalid:
                        commit.message = "The project has been published!  Record these changes offsite if you wish, then discard them to see the final, published commit."
                        commit.save()
            elif self.status == Commit.APPROVED:
                self.commit.approver = self.request.user
                self.commit.save()
            elif self.status == Commit.HEAD:
                if self.head:
                    self.head.status = Commit.COMMITTED
                    self.head.approver = None
                    if hasattr(self.head,'publish'):
                        self.head.publish.editors.clear()
                    self.head.save()
                    self.commit.commit_id = self.head.commit_id+1
                self.commit.committed = timezone.now()
                self.commit.save()
                if self.commit.page != Commit.TALK:
                    self.object.head = self.commit
                else:
                    self.object.talk_head = self.commit
                self.object.save()
                if self.commit.page == Commit.WORK and self.object.status == Article.OPEN and not self.object.creators.filter(user=self.request.user).exists():
                    self.creator, created = Creator.objects.get_or_create(user=self.request.user,role=Creator.WRITER)
                    self.object.creators.add(self.creator)
                if not hasattr(self.commit,'publish') and self.commit.is_workable():
                    Publish.objects.create(commit=self.commit)
                self.object.commits.filter(page=self.commit.page,status__in=[Commit.SAVED,Commit.INVALID]).update(status=Commit.INVALID,created=timezone.now()) #message set in DetailMixin bc F() doesn't Work
                if self.object.domain.is_main() and self.commit.page != Commit.TALK:
                    self.save_linkify()
                    for link in self.object.rights.all(): #TODO DO THIS IN CELERY
                        link.save()
                    for link in self.object.lefts.all(): #TODO DO THIS IN CELERY
                        link.save()
            elif self.status == Commit.COMMITTED: #hack for revoking, Commit.COMMITTED is not used anywhere else
                self.commit.status = Commit.HEAD #overwrites Commit.COMMITTED
                self.commit.approver = None
                self.commit.save()
            elif self.status == Commit.REQUESTED:
                self.commit.commit_id = self.object.get_tail(page=self.commit.page).commit_id-1
                self.commit.committed = timezone.now()
                self.commit.save()
                self.creator = Creator.objects.get(user=self.request.user,role=Creator.REQUESTER)
                self.object.creators.add(self.creator)
            elif self.status in [Commit.SAVED,Commit.INVALID]:
                self.commit.commit_id = None
                self.commit.save()
            elif self.status in [Commit.REJECTED,Commit.RECALLED]:
                self.commit.save()
                self.creator, created = Creator.objects.get_or_create(user=self.request.user,role=Creator.REQUESTER)
                if self.object.is_removable(self.creator):
                    self.object.creators.remove(self.creator)
            elif self.status == Commit.DELETED:
                self.commit.delete()
            self.notifier()

    def notifier(self):
        if self.status == Commit.SUBMITTED:
            pass
        elif self.status == Commit.PUBLISHED:
            if self.commit.status == Commit.PUBLISHED:
                notify.send(self.request.user,
                            recipient=self.object.watchers.exclude(id=self.request.user.id).union(self.object.domain.watchers.exclude(id=self.request.user.id)),
                            verb="published",
                            target=self.object,
                            description='publish '+self.object.str2()+' '+str(self.request.user),
                        )
            else:
                notify.send(self.request.user,
                            recipient=get_user_model().objects.filter(creator__role=Creator.EDITOR,creator__article=self.object).exclude(creator__user=self.request.user),
                            verb="*pending* published",
                            target=self.object,
                            description='publish '+self.object.str2()+' '+str(self.request.user),
                        )
        elif self.status == Commit.APPROVED:
            notify.send(self.request.user,
                        recipient=get_user_model().objects.filter(creator__role=Creator.EDITOR,creator__article=self.object),
                        verb="approved for publication",
                        target=self.commit,
                        description='recall '+self.commit.str2()+' '+str(self.request.user),
                    )
        elif self.status == Commit.HEAD:
            actor = self.commit.creator.user if self.commit.creator else self.commit.anoncreator
            notify.send(actor,
                        recipient=self.object.watchers.exclude(id=self.request.user.id),
                        verb="just committed",
                        target=self.commit,
                        description='recall '+self.commit.str2()+' '+str(actor),
                    )
            if self.new_tags:
                notify.send(actor,
                            recipient=self.new_tags,
                            verb="mentioned you in",
                            target=self.commit,
                            description='mention '+self.commit.str2()+' '+str(actor),
                        )
        elif self.status == Commit.COMMITTED:
            notify.send(self.request.user,
                        recipient=get_user_model().objects.filter(creator__role=Creator.EDITOR,creator__article=self.object),
                        verb="revoked publication approval for",
                        target=self.commit,
                        description='revoke '+self.commit.str2()+' '+str(self.request.user),
                    )
        elif self.status == Commit.REQUESTED:
            notify.send(self.request.user,
                        recipient=get_user_model().objects.filter(creator__role=Creator.EDITOR,creator__article=self.object),
                        verb="made a pull request for",
                        target=self.commit,
                        description='pull request '+self.commit.str2()+' '+str(self.request.user),
                    )
        elif self.status == Commit.REJECTED:
            notify.send(self.request.user,
                        recipient=self.commit.creator.user,
                        verb="rejected your pull request for",
                        target=self.commit,
                        description='reject '+self.commit.str2()+' '+str(self.request.user),
                    )
        elif self.status == Commit.RECALLED:
            notify.send(self.request.user,
                        recipient=get_user_model().objects.filter(creator__role=Creator.EDITOR,creator__article=self.object),
                        verb="recalled their pull request for",
                        target=self.commit,
                        description='recall '+self.commit.str2()+' '+str(self.request.user),
                    )
    def save_linkify(self):
        links = Link.objects.select_for_update().select_related('left','right').filter(Q(left=self.object) | Q(right=self.object))
        self.old_targets = set(links.filter(left=self.object,direction__in=[Link.FORWARD,Link.REWARD]).values_list('right__id',flat=True)) | set(links.filter(right=self.object,direction__in=[Link.BACKWARD,Link.REWARD]).values_list('left__id',flat=True))
        removed = self.old_targets.difference(self.new_targets)
        added = self.new_targets.difference(self.old_targets)

        for remove in removed:
            assert remove != self.object.id
            r = links.get(Q(left__id=remove) | Q(right__id=remove))
            if r.direction < Link.REWARD:
                r.delete()
            else:
                r.demote(source=self.object)
                r.save()
        for add in added:
            assert add != self.object.id
            if links.filter(Q(left__id=add) | Q(right__id=add)).exists():
                link = links.get(Q(left__id=add) | Q(right__id=add))
                link.promote()
                link.save()
            else:
                l = Link.create(self.object,self.new_targets_dict[add])
                l.save()

    #TODO replace regexes with beautifulsoup
    def preprocess(self):
        if self.commit.page == Commit.KEN or (self.commit.page == Commit.WORK and self.object.status <= Article.OPEN):
            self.set_header()
            self.object.document = self.commit.document
        if self.commit.page == Commit.KEN and self.object.domain.is_tns():
            self.set_citation()
        if self.commit.page == Commit.TALK:
            self.set_signature()
        self.pre_linkify()
        # if self.commit.page == Commit.TALK:
        #     sub = "USER SIGNATURES NEED TO BE RE-IMPLEMENTED" #TODO user signatures in talk page (foo.user is a hack, it is set by me but not saved in model, it is request.user)
        #     # sub = "["+foo.user.pseudonym+"]("+foo.user.get_foo_url()+") ([talk]("+foo.user.get_talk_foo_url()+")) "+datetime.datetime.now().strftime("%b %d, %Y %H:%M")+" (UTC)"
        #     self.commit.document = re.sub(r'~~~~',sub,self.commit.document)
        #     self.document = self.commit.document

    def set_signature(self):
        sub = "["+str(self.request.user)+"] "+datetime.datetime.now().strftime("%d %b %Y %H:%M")+" (UTC)"
        (self.commit.document,count) = re.subn(r'~~~~',sub,self.commit.document)
        if count > 0:
            self.reload = True

    def pre_linkify(self):
        found = set()
        refs = []
        handles_ = re.findall(r'(?i)(\[[^\n]*?@([A-Za-z0-9\.]+)x([a-fA-F0-9]+)[^\n]*?\])',self.commit.document)
        for handle_ in handles_:
            handles = re.findall(r'(?i)@([A-Za-z0-9\.]+)x([a-fA-F0-9]+)(/_?\d+)?',handle_[0])
            for handle in handles:
                handle = list(handle)
                if handle[0]+handle[1] in found:
                    pass
                else:
                    IDea = "@"+handle[0]+"x"+handle[1]
                    try:
                        self2 = Article.get_article(Object(),{'IDea':IDea},select_related=False)
                        assert (self2.object != self.object and self2.object.domain.is_main()) or self.commit.page == Commit.TALK
                    except:
                        continue
                    else:
                        found.add(handle[0]+handle[1])
                        self.new_targets.add(self2.object.id)
                        self.new_targets_dict[self2.object.id] = self2.object
                        ref = {'title':self2.object.header.get('title')}
                        if self.commit.page == Commit.TALK:
                            ref['id'] = IDea[1:]+handle[2]
                        else:
                            ref['id'] = IDea[1:]
                        refs.append(ref)
        raw_yaml = re.match(r'---((?s).*?)---',self.commit.document)
        try:
            header = yaml.safe_load(raw_yaml[1])
            header['references'] = refs
            self.document = self.commit.document.replace(raw_yaml[0],"---\n"+yaml.safe_dump(header,default_flow_style=False)+"\n---")
        except:
            header = {'title': 'Talk'}
            header['references'] = refs
            self.document = "---\n"+yaml.safe_dump(header,default_flow_style=False)+"\n---\n\n"+self.commit.document


    def set_header(self):
        raw_yaml = re.match(r'---((?s).*?)---',self.commit.document)
        self.commit.header = self.object.header = yaml.safe_load(raw_yaml[1])
        if not 'title' in self.object.header or  self.object.header['title'] == "":
            raise ValidationError("the document must have a title")
        elif self.object.domain.is_tns() and self.object.hash and self.object.header['title'] != self.object.hash.header['title']:
            raise ValidationError("this title cannot be modified")
        self.commit.header['title'] = self.object.header['title'] = bleach.clean(self.object.header['title'])

    def set_citation(self):
        if self.commit.parent is not None and self.commit.header == self.head.header and self.head.citation is not None:
            self.commit.citation = self.head.citation
        elif self.object.domain.is_tns() and self.object.hash:
            self.commit.citation = self.object.citation
        else:
            try:
                fp = []
                fp.append(tempfile.NamedTemporaryFile(suffix=".yaml"))
                fp.append(tempfile.NamedTemporaryFile())
                fp.append(tempfile.NamedTemporaryFile(suffix=".html"))
                ref = {'references': [self.object.header]}
                ref['references'][0]['id'] = self.object.get_IDea()

                ref_yaml = "---\n"+yaml.safe_dump(ref,default_flow_style=False)+"\n..."
                fp[0].write(ref_yaml.encode())
                fp[0].seek(0)

                fp[1].write(self.object.get_handle().encode())
                fp[1].seek(0)

                command = ['pandoc','-s','--bibliography',fp[0].name,'--filter','pandoc-citeproc',fp[1].name,'-o',fp[2].name]
                x = subprocess.run(command,stdout=subprocess.PIPE,encoding='utf-8')
                fp[2].seek(0)
                citation = fp[2].read().decode('utf-8')
                citation = bleach.clean(citation,
                                        tags=['html','body','span','div','p','a',
                                                'em','strong'],
                                        attributes={
                                            '*': ['id','class','citation','data-cites',
                                                    'href']
                                        })
                soup = BeautifulSoup(citation,"html5lib")
                self.object.citation = self.commit.citation = base64.b64encode(pickle.dumps(soup.find(id="ref-"+self.object.get_IDea()).p))
            except:
                pass

    def unwrap(self,obj,double=False):
        if double:
            soup = BeautifulSoup(obj.get_handle_double_linked(),'html5lib')
        else:
            soup = BeautifulSoup(obj.get_handle_linked(),'html5lib')
        soup.body.wrap(soup.new_tag('span'))
        soup.body.unwrap()
        return soup.span

    def insert_embed(self,soup):
        if self.commit.page == Commit.KEN and self.object.domain.id == Domain.TNS and self.commit.embed is not None:
            embed = BeautifulSoup(self.commit.embed,"html5lib")
            try:
                soup.find(attrs={"data-cites": "embed"}).replace_with(embed.div)
            except:
                pass
        return soup

    def insert_citation(self,soup):
        l2 = soup.find_all("section","level2")
        try:
            if l2[-1].h2.string == "Reference" and self.commit.citation:
                l2[-1].h2.insert_after(pickle.loads(base64.b64decode(self.commit.citation)))
        except:
            pass
        return soup

    def post_linkify(self,soup):
        citations = soup.find_all("span",{"class":"citation"})
        for citation in citations:
            if citation.find("span",{"class":"citeproc-not-found"}):
                try:
                    assert self.commit.page == Commit.TALK
                    match = re.match(r'(?i)([A-Za-z0-9\.]+)x([a-fA-F0-9]+)(/-?\d+)?',citation['data-cites'])
                    assert match is None
                    domain = Domain.objects.get(id__iexact=citation['data-cites'].replace('.',''))
                    assert not domain.is_main()
                    tags = get_user_model().objects.filter(creator__role__gte=Creator.MANAGER,creator__domain=domain).exclude(creator__user=self.request.user)
                    self.new_tags = self.new_tags.union(tags)
                    citation.string = ''
                    a = soup.new_tag("a",href=domain.get_absolute_url())
                    a.string = domain.str2()
                    citation.append(a)
                except:
                    citation.clear()
                    strong = soup.new_tag("strong")
                    strong.string = "???"
                    citation.append("(")
                    citation.append(strong)
                    citation.append(")")
            else:
                handles = citation['data-cites'].split(' ')
                paren_split = re.split('\(|\)',citation.contents[0])
                assert len(paren_split) == 3 #TODO validationerrors here with helpful messages
                semicolon_split = paren_split[1].split(';')
                assert len(semicolon_split) == len(handles)

                citation.clear()
                bracket = len(handles) > 1 or handles[0][0] != Domain.DEF
                if bracket:
                    citation.append(paren_split[0])
                    citation.append("[")
                for i,fragment in enumerate(semicolon_split):
                    comma_split = fragment.split(",")
                    for j,phrase in enumerate(comma_split):
                        handles[i] = handles[i].replace('/_','/-') #TODO probably shouldn't do this here
                        match = re.match(r'(?i)([A-Za-z0-9\.]+)x([a-fA-F0-9]+)(/-?\d+)?',handles[i])
                        if match[1] == Domain.DEF and len(handles) == 1:
                            if "|" in fragment:
                                phrase = fragment.split("|")[1].strip()
                            else:
                                phrase = phrase[1:]
                        else:
                            phrase = hex_upper(handles[i])
                        if match[3] and self.commit.page == Commit.TALK:
                            a = soup.new_tag("a",href="/@"+match[1]+"x"+match[2]+match[3])
                        else:
                            a = soup.new_tag("a",href="/@"+match[1]+"x"+match[2])
                        a.string = phrase
                        citation.append(a)
                        break
                    if i < len(semicolon_split) - 1:
                        citation.append(", ")
                if bracket:
                    citation.append("]")

        refs = soup.find("div", {"id": "refs"})
        if refs: refs.extract()
        return soup

    def death_to_external_anchors(self,soup):
        anchors = soup.find_all("a")
        for anchor in anchors:
            if anchor.get('href') and anchor['href'][0] != "#" and (anchor.parent.name != 'span' or anchor.parent.get('class') != ['citation'] or anchor.parent.get('data-cites') is None):
                anchor.extract()
        imgs = soup.find_all("img") #redundant with bleach.clean()
        for img in imgs:
            img.extract()
        return soup

    def postprocess(self):
        soup = BeautifulSoup(self.raw,"html5lib")
        # self.commit.head = str(soup.head)

        #links
        soup = self.post_linkify(soup)
        soup = self.death_to_external_anchors(soup)

        #lede, toc
        soup = lede(soup) #add section tags around lede paragraph
        (soup,self) = toc(soup,self) #move section ahead of TOC, set lede
        # self.object.body = str(soup.article)

        if self.object.domain.id == Domain.TNS and self.commit.page == Commit.KEN:
            soup = self.insert_citation(soup)
        #redlink

        #end
        soup.article.wrap(soup.new_tag("div"))
        soup.article.unwrap()
        self.commit.body = bleach.clean(str(soup.body.div),
                                    tags=['article','header','h2','h3','section',
                                            'p','span','div','a','em','strong',
                                            'nav','ul','ol','li','pre','code',
                                            'footer','input','blockquote','label',
                                            'figure','table','thead','tbody','tr',
                                            'th','td','br','cite','colgroup','col'],
                                    attributes={
                                    '*':['for','class','id','onclick','href',
                                        'type','data-line-number'],
                                    })

def lede(soup):
    section = soup.new_tag('section')
    if soup.find(id="TOC") and soup.article.p.span and 'context-heading' in soup.article.p.span['id']:
        soup.article.p.parent.parent.insert_before(section)
    elif soup.article.p:
        soup.article.p.insert_before(section)
    else:
        soup.article.append(section)
    for child in soup.article.contents:
        if child.name == 'p':
            p = copy.copy(child)
            section.append(p)
            child.extract()
    return soup

def toc(soup,self):
    temp = self.object.lede
    if soup.section and soup.section.p and not soup.section.has_attr('id') and soup.select('nav'):
        self.object.lede = "".join([str(item) for item in soup.section.p.contents])
        section = soup.section.extract()
        self.object.nonlede = str(soup.article)
        soup.nav.insert_before(section)
    elif soup.section and soup.section.p:
        self.object.lede = "".join([str(item) for item in soup.section.p.contents])
        self.object.nonlede = ""
    else:
        self.object.lede = ""
        self.object.nonlede = str(soup.article)
    if self.commit.page == Commit.TALK: self.object.lede = temp

    #necessary to prevent attacks in lede; also, prevents marginnotes from showing up in feeds where an excerpt from lede is shown; could be improved
    self.object.lede = bleach.clean(self.object.lede,
                                tags=['article','header','h2','h3','section',
                                        'p','span','div','a','em','strong',
                                        'nav','ul','ol','li','pre','code',
                                        'footer','blockquote',
                                        'figure','table','thead','tbody','tr',
                                        'th','td','br','cite'],
                                strip=True,)
    return (soup,self)
