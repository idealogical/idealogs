from django.utils import timezone
from django.core.exceptions import ValidationError
from bs4 import BeautifulSoup
from ruamel.yaml import YAML
from ruamel.yaml.compat import StringIO
from random import randint
import re
import subprocess
import hashlib
import tempfile
import diff_match_patch as dmp_module
import pdb

def validate(clause,message="no message supplied"):
    if not clause:
        raise ValidationError(message)

def unwrap(obj,double=False):
    if double:
        soup = BeautifulSoup(obj.get_handle_double_linked(),'html5lib')
    else:
        soup = BeautifulSoup(obj.get_handle_linked(),'html5lib')
    soup.body.wrap(soup.new_tag('span'))
    soup.body.unwrap()
    return soup.span

def get_subtitle(specs,options=None):
    soup = BeautifulSoup("","html5lib")
    span = soup.new_tag("span")
    for i,spec in enumerate(specs,1):
        if i < len(specs):
            if not isinstance(spec[0], str):
                span.append(spec[0])
            else:
                a = soup.new_tag("a")
                a.string = spec[0]
                a['href'] = spec[1]
                span.append(a)
            span.append("|")
        else:
            span.append(spec[0])
    if options is not None:
        span2 = soup.new_tag("span")
        span2['style'] = 'font-size:1.25rem;'
        for i,option in enumerate(options,1):
            a = soup.new_tag("a")
            a.string = option[0]
            a['href'] = option[1]
            span2.append(a)
            if i < len(options):
                span2.append("|")
        span.append(" ")
        span.append(span2)
    return str(span)

def next_br(text,dist):
    index = text.find("&para;<br>",dist)
    if index == -1:
        return text
    else:
        return text[:index+10]

def prev_br(text,dist):
    index = text.rfind("&para;<br>",0,-dist)
    if index == -1:
        return text
    else:
        return text[index+10:]

def prettyHtml(self, diffs):
    """Convert a diff array into a pretty HTML report.
    Args:
      diffs: Array of diff tuples.
    Returns:
      HTML representation.
    """
    return prettyHtml2(self,diffs) #see below

    html = []
    len_diffs = len(diffs)
    dist = 75
    for i,(op, data) in enumerate(diffs):
        text = (data.replace("&", "&amp;").replace("<", "&lt;")
                .replace(">", "&gt;").replace("\n", "&para;<br>"))
        if op == self.DIFF_INSERT:
            html.append("<ins style=\"background:#e6ffe6;\">%s</ins>" % text)
        elif op == self.DIFF_DELETE:
            html.append("<del style=\"background:#ffe6e6;\">%s</del>" % text)
        elif op == self.DIFF_EQUAL:
            if i != 0:
                html.append("<span>%s</span>" % next_br(text,dist))
            if i > 0 and i < len_diffs - 1:
                html.append("<br/>")
            if i != len_diffs - 1:
                html.append("<span>%s</span>" % prev_br(text,dist))
    return "".join(html)

def prettyHtml2(self, diffs):
    """Convert a diff array into a pretty HTML report.
    Args:
      diffs: Array of diff tuples.
    Returns:
      HTML representation.
    """
    html = []
    for (op, data) in diffs:
      text = (data.replace("&", "&amp;").replace("<", "&lt;")
                 .replace(">", "&gt;").replace("\n", "<br>"))
      if op == self.DIFF_INSERT:
        html.append("<ins style=\"background:#e6ffe6;\">%s</ins>" % text)
      elif op == self.DIFF_DELETE:
        html.append("<del style=\"background:#ffe6e6;\">%s</del>" % text)
      elif op == self.DIFF_EQUAL:
        html.append("<span>%s</span>" % text)
    html2 = "".join(html)
    lines = html2.split("<br>")
    result = ""
    rollover = False
    for line in lines:
        if re.search(r'<ins style=\"background:#e6ffe6;\">',line):
            result = result + line + "<br/><br/>"
            rollover = not re.search(r'</ins>',line)
        elif re.search(r'<del style=\"background:#ffe6e6;\">',line):
            result = result + line + "<br/><br/>"
            rollover = not re.search(r'</del>',line)
        elif re.search(r'</ins>',line):
            result = result + line + "<br/><br/>"
            rollover = False
        elif re.search(r'</del>',line):
            result = result + line + "<br/><br/>"
            rollover = False
        elif rollover and line:
            result = result + line + "<br/><br/>"
    return result

def hex_upper(idea):
    idea = idea.rsplit('x',1)
    return idea[0]+'x'+idea[1].upper()

class MyYAML(YAML):
    def dump(self, data, stream=None, **kw):
        inefficient = False
        if stream is None:
            inefficient = True
            stream = StringIO()
        YAML.dump(self, data, stream, **kw)
        if inefficient:
            return stream.getvalue()

def gen_pub_id(user=None):
    return hashlib.sha256((str(timezone.now())+user.pseudonym).encode()).hexdigest()[:16]

def patch(text1,text2,text3):
    dmp = dmp_module.diff_match_patch()
    patches = dmp.patch_make(text1,text2)
    return dmp.patch_apply(patches,text3)

def merge(mod1,orig,mod2,id1,id2):
    fp = ['','','',]
    for i in range(0,len(fp)):
        fp[i] = tempfile.NamedTemporaryFile()

    fp[0].write(mod1.encode())
    fp[1].write(orig.encode())
    fp[2].write(mod2.encode())

    for i in range(0,len(fp)):
        fp[i].seek(0)
    x = subprocess.run(['merge','-p','-q','-L',id1,'-L','ANCESTOR','-L',id2,fp[0].name,fp[1].name,fp[2].name],stdout=subprocess.PIPE,
                        encoding='utf-8')

    for i in range(0,len(fp)):
        fp[i].close()
    return x.stdout

class Object(object):
    pass

HINTS = {
    'Index': [
        # ["Welcome.",
        # "These are helpful hints that will try to answer some frequently asked questions.  To see hints on other pages, you need to be logged in.  You can turn hints off in your account <a href=\'/panel\'>settings</a>."],
        # ["What is this thing?",
        # "It is a crowdsourcing website for understanding complex, controversial topics.  I call it a sunesiary :)"],
        # ["Sunesiary?",
        # "In practice, it is a wiki with some special modifications.  In theory, it is a reference work that collects understanding.  <em>Sunesis</em> means understanding in Greek."],
        # ["So like Wikipedia?",
        # "Wikipedia's purpose is to collect the world's <strong>knowledge</strong> and make it freely available.  Idealogs' purpose is to collect the world's <strong>understanding</strong> and make it freely available.  Wikipedia is a digital encyclopedia.  Idealogs is a digital sunesiary.  Both are powered by a wiki."],
        # ["What is the difference between knowledge and understanding?",
        # "Understanding = Knowledge + Context"],
        # ["How does this site work?",
        # "In a nutshell: this site catalogs and analyzes contextual information (aka <strong>context</strong>).  It does so via a highly-modified wiki that is optimized for this task, but a wiki nonetheless.  Anyone can edit any article, and anyone can create any article. It is up to the community to produce something of value."],
        # ["Lastly: what are those boxes above?",
        # "Each box represents a bidirectional link between two articles in the wiki. This page is a feed of every bidirectional link, sorted by a combination of time and quality."]
    ],
    'Update': [
        ["Markdown",
        "In order to edit articles, you need a basic understanding of <a href='https://www.markdownguide.org/basic-syntax'>markdown</a>. See the <a href='/@0x1'>docs</a> for more info"],
        # ["Workflow",
        # "If you are logged in, you can take advantage of some nifty workflow features. You can save your work before publishing and come back to it later via the <em>Works</em> tab in the navigation bar.  You can easily highlight the changes you are making as you work via the <em>Changes</em> button. You can focus on a specific section to edit using the <em>Section</em> arrows.  If you are not logged in, the editing workflow is nearly identical to a wiki."],
        # ["Linking",
        # "To link to another article in the sunesiary, you need to know its <strong>handle</strong>.  Every article in a sunesiary has a unique handle (e.g. @0x0). You can find the handle of an article near the top, just beneath the title.  The syntax you use to link to an article is [handle] (e.g. [@0x0]). To link to multiple articles at once, do [@Tx0;@Tx1;...].  For definite articles (articles that begin \'@0x...\'), you can modify the link using [@0x0|Ann Arbor]."],
        ["Searching",
        "Don't know a particular article's handle? Want to know if a certain article exists? Use the <strong>Search</strong> feature above."],
        ["YAML Metadata",
        "At the top of the document, you will see a block of text enclosed by '---'s on top and bottom.  This is the YAML Metadata block for the article.  This where you set the title for the article. Also, for transfinite articles, this where you put all the bibliographic information for the work that you are cataloging."],
    ],
    'New': [
        ["New articles",
        "To add a new article, you need an <strong>identifier</strong> (e.g. title, DOI, URL, etc.) for what is being catalogued and its associated <strong>hot key</strong>.  See the <a href='/@0x1#creating-new-articles'>docs</a> for more info."]
    ]
}
