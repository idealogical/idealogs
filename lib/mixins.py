from django.conf import settings
from django.views.generic import TemplateView, RedirectView, FormView, ListView
from django.views.generic.edit import FormMixin
from django.views.decorators.cache import cache_page
from django.shortcuts import render, redirect
from django.db.models import Q
from django.contrib.auth import get_user_model
from django.core.exceptions import ValidationError
from django.core.urlresolvers import reverse
from django.core.paginator import Paginator, Page, PageNotAnInteger, EmptyPage
from django.http import Http404, JsonResponse
from django.utils import timezone
from django.db import transaction
from django import forms
from functools import reduce
from idealogs.models import Article, Commit, Link, AnonEditor
from idealogs.forms import B2YForm, NewForm, MoveForm, DeleteForm, DetailForm, AuthUpdateForm, StageForm, ActionForm, RejectForm, HistoryForm, ChangesForm
from user.models import Domain, Creator
from lib.gaykaken import gaykaken
from lib.utils import merge, patch, Object, prettyHtml, get_subtitle, HINTS
from lib.tifdoc import Tifdoc
from bs4 import BeautifulSoup
from formtools.preview import FormPreview
from notifications.signals import notify
from ipware import get_client_ip
import diff_match_patch as dmp_module
from elasticsearch import Elasticsearch
from elasticsearch_dsl import Search
from elasticsearch_dsl.query import Q as ES_Q
from notifications.signals import notify
from urllib import parse
import json
import pdb
import bleach
import operator
import math
import subprocess
import re
import pdb

class CreatorsMixin(object):

    def dispatch(self,request,*args,**kwargs):
        self.head = self.commit = self.object.get_head(page=self.page)
        if self.commit.page != Commit.WORK:
            return redirect(self.commit)
        return super().dispatch(request,*args,**kwargs)

    def get_context_data(self,**kwargs):
        context = super().get_context_data(**kwargs)
        context['is_owner'] = self.request.user.is_authenticated and self.object.domain.is_owner(self.request.user)
        return context

class ActionMixin(object):
    form_class = ActionForm

    def dispatch(self,request,*args,**kwargs):
        try:
            commit_id = int(kwargs.get('commit_id')) if kwargs.get('commit_id') else None
            if commit_id is not None:
                self.commit = self.object.get_commit(page=self.page,id=commit_id)
                if self.commit.status == Commit.REQUESTED: assert self.role == Creator.EDITOR
                elif self.commit.status < Commit.INVALID: assert self.commit.is_fubar(user=request.user)
                else: assert False
            else:
                self.commit = self.object.get_commit(page=self.page,user=request.user)
                assert self.commit.status == Commit.INVALID or self.commit.status < Commit.INVALID and self.commit.is_fubar(user=request.user)
            self.source = self.commit
            self.head = self.object.get_head(page=self.page)
            if self.commit.status < Commit.INVALID:
                self.target = self.object.commits.filter(Q(page=self.commit.page) & (Q(creator__user=request.user,status__in=[Commit.INVALID,Commit.SAVED]) | Q(status__in=[Commit.HEAD,Commit.APPROVED]))).order_by('status')[0]
                # self.target = self.object.commits.get(page=self.commit.page,creator__user=request.user,status__in=[Commit.INVALID,Commit.SAVED])
                assert self.commit.status in [Commit.REJECTED,Commit.RECALLED]
            else:
                self.target = self.head
                assert self.commit.status in [Commit.INVALID,Commit.REQUESTED]
            try:
                self.ancestor = self.object.get_commit(page=self.page,id=min(self.target.commit_id,self.commit.parent))
            except:
                self.ancestor = self.object.get_commit(page=self.page,id=min(self.target.parent,self.commit.parent))
            self.newer = Commit()
            if request.GET['type'] == 'patch':
                dmp = dmp_module.diff_match_patch()
                [self.newer.document,results] = patch(self.ancestor.document,self.commit.document,self.target.document)
                if False in results:
                    #TODO set up alert message system thingy
                    return redirect(self.commit)
            elif request.GET['type'] == 'merge':
                self.newer.document = merge(self.target.document,self.ancestor.document,self.commit.document,self.target.statusify(),self.commit.statusify())
            else:
                assert False
        except:
            return redirect(self.object.get_head(page=self.page))
        return super().dispatch(request,*args,**kwargs)

    def get_initial(self):
        initial = super().get_initial()
        initial['document'] = self.newer.document
        return initial

    def get_context_data(self,**kwargs):
        context = super().get_context_data(**kwargs)
        context['object'] = self.object
        context['commit'] = self.commit
        context['source'] = self.source
        context['target'] = self.target
        specs = [{'string': 'Go back',
                    'type': 'submit',
                    'name': 'action1',
                    'style': 'margin-top:1rem;margin-bottom:1rem;margin-right:.25rem;',},
                {'string': 'Save',
                            'type': 'submit',
                            'name': 'action2',
                            'style': 'margin-top:1rem;margin-bottom:1rem;margin-right:.25rem;',},
                ]
        if self.commit.status == Commit.REQUESTED:
            specs[1]['string'] = 'Commit'
            specs[1]['name'] = 'action3'
        button_top, button_bottom = buttonify(specs)
        context['button_top'] = str(button_top)
        context['button_bottom'] = str(button_bottom)

        if self.request.GET['type'] == 'patch':
            dmp = dmp_module.diff_match_patch()
            diff = dmp.diff_main(self.target.document,self.newer.document)
            dmp.diff_cleanupSemantic(diff)
            context['prettydiff'] = prettyHtml(dmp,diff)
        else:
            assert self.request.GET['type'] == 'merge'
            if self.newer.is_ugly():
                context['uglydiff'] = self.newer.document
            else:
                dmp = dmp_module.diff_match_patch()
                diff = dmp.diff_main(self.target.document,self.newer.document)
                dmp.diff_cleanupSemantic(diff)
                context['prettydiff'] = prettyHtml(dmp,diff)
        context['title'] = self.request.GET['type'].capitalize()
        return context

    def form_valid(self,form):
        self.success_url = self.commit.get_detail_url()
        return super().form_valid(form)

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['target'] = self.target
        return kwargs

class SourceMixin(object):

    def dispatch(self,request,*args,**kwargs):
        commit_id = int(kwargs.get('commit_id')) if kwargs.get('commit_id') else None
        try:
            self.commit = self.object.get_head(page=self.page)
        except:
            return redirect(self.object.get_work_url())
        return super().dispatch(request,*args,**kwargs)

class DiffMixin(object):

    def dispatch(self,request,*args,**kwargs):
        try:
            if kwargs.get('older_id'):
                self.older = self.object.get_commit(page=self.page,id=int(kwargs.get('older_id')))
                if kwargs.get('newer_id'):
                    self.newer = self.object.get_commit(page=self.page,id=int(kwargs.get('newer_id')))
                else:
                    self.newer = self.object.get_commit(page=self.page,user=request.user)
            else:
                #diff invalid
                self.older = self.object.get_commit(page=self.page,user=request.user)
                self.newer = self.object.get_head(page=self.page)
            self.commit = self.newer
        except:
            return redirect(self.object.get_head(page=self.page).get_history_url())
        return super().dispatch(request,*args,**kwargs)

    def get_context_data(self,**kwargs):
        context = super().get_context_data(**kwargs)
        context['changes'] = False
        context['title'] = 'Changes'
        context['older'] = self.older
        context['newer'] = self.newer

        dmp = dmp_module.diff_match_patch()
        diff = dmp.diff_main(self.older.document,self.newer.document)
        if len(diff) == 0:
            context['diff'] = "no changes ¯\\_(ツ)_/¯"
        else:
            dmp.diff_cleanupSemantic(diff)
            context['diff'] = prettyHtml(dmp,diff)
        return context

class ChangesMixin(object):
    form_class = ChangesForm

    def form_valid(self,form):
        if form.cleaned_data['action'] == 1:
            self.success_url = self.commit.get_detail_url(section=self.section)+"?log=True"
        elif form.cleaned_data['action'] == 2:
            assert self.commit.is_mutable() and (self.object.status < Article.FULL or self.request.user.is_staff) and (not self.request.user.is_authenticated or self.role in Creator.MUTATERS)
            assert self.commit.status == Commit.SAVED or (self.commit.status in [Commit.HEAD,Commit.APPROVED] and not self.object.has_working_copy(page=self.page,user=self.request.user))
            self.success_url = self.commit.get_edit_url(section=self.section)
        elif form.cleaned_data['action'] == 3:
            assert self.commit.is_mutable() and (self.object.status < Article.FULL or self.request.user.is_staff) and (not self.request.user.is_authenticated or self.role in Creator.MUTATERS)
            assert self.commit.status == Commit.COMMITTED and (not self.request.user.is_authenticated or not self.object.has_working_copy(page=self.page,user=self.request.user))
            if self.request.user.is_authenticated:
                Tifdoc(status=Commit.SAVED,
                        article=self.object,
                        head=self.head,
                        commit=self.head,
                        request=self.request,
                        document=self.commit.document,
                        message='Reverting back to Commit '+str(self.commit.commit_id))
                self.success_url = self.head.get_stage_url()
            else:
                self.success_url = self.object.get_anonit_url(page=self.commit.page)+"?revert="+str(self.commit.commit_id)

        return super().form_valid(form)

    def dispatch(self,request,*args,**kwargs):
        try:
            commit_id = int(kwargs.get('commit_id')) if kwargs.get('commit_id') else None
            if commit_id is not None:
                self.commit = self.object.get_commit(page=self.page,id=commit_id)
            else:
                self.commit = self.object.get_commit(page=self.page,user=request.user)
        except:
            return redirect(self.object.get_commit(page=self.page,user=request.user))
        else:
            self.newer = self.commit
            if self.newer.commit_id == 0:
                self.older = Object()
                self.older.document = ""
                self.older.status = -5
            elif self.newer.commit_id is None:
                self.older = self.object.get_head(page=self.page)
            else:
                self.older = self.object.get_commit(page=self.page,id=self.newer.parent)
            self.head = self.object.get_head(page=self.page)
        return super().dispatch(request,*args,**kwargs)

    def get_context_data(self,**kwargs):
        context = super().get_context_data(**kwargs)
        context['changes'] = True
        context['newer'] = self.newer
        context['older'] = self.older
        context['title'] = "Changes"
        if self.request.user.is_authenticated:
            context['working_copy'] = self.object.has_working_copy(page=self.page,user=self.request.user)
        else:
            context['working_copy'] = False
        context['yesterday'] = self.commit.status in [Commit.REQUESTED,Commit.COMMITTED] or (self.commit.status >= Commit.HEAD and context['working_copy'])
        dmp = dmp_module.diff_match_patch()
        diff = dmp.diff_main(self.older.document,self.newer.document)
        if len(diff) == 0:
            context['diff'] = "no changes ¯\\_(ツ)_/¯"
        else:
            dmp.diff_cleanupSemantic(diff)
            context['diff'] = prettyHtml(dmp,diff)

        specs = [{'string': 'Read',
                    'type': 'submit',
                    'name': 'action1',
                    'style': 'margin-top:1rem;margin-bottom:1rem;margin-right:.25rem;',},]
        if self.commit.is_mutable() and (self.object.status < Article.FULL or self.request.user.is_staff) and (not self.request.user.is_authenticated or self.role in Creator.MUTATERS):
            if self.commit.status == Commit.SAVED or (self.commit.status in [Commit.HEAD,Commit.APPROVED] and not context['working_copy']):
                specs.append({'string': 'Edit',
                        'type': 'submit',
                        'name': 'action2',
                        'style': 'margin-top:1rem;margin-right:.25rem;',})
            elif self.commit.status == Commit.COMMITTED and (not self.request.user.is_authenticated or not context['working_copy']):
                specs.append({'string': 'Revert',
                        'type': 'submit',
                        'name': 'action3',
                        'style': 'margin-top:1rem;margin-right:.25rem;',})
        button_top, button_bottom = buttonify(specs)
        context['button_top'] = str(button_top)
        context['button_bottom'] = str(button_bottom)
        return context

class HistoryMixin(object):
    paginate_by = 10
    form_class = HistoryForm

    def form_valid(self,form):
        try:
            self.success_url = self.commit.get_diff_url(newer_id=form.cleaned_data['commits'][0],older_id=form.cleaned_data['commits'][1])
            return super().form_valid(form)
        except:
            form.add_error('__all__',"something unexpected is going wrong :/")
            return super().form_invalid(form)

    def dispatch(self,request,*args,**kwargs):
        self.commit = self.object.get_head(page=self.page)
        return super().dispatch(request,*args,**kwargs)

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['queryset'] = []
        for commit in self.queryset:
            if not hasattr(commit,'commit_id') or commit.commit_id is None:
                kwargs['queryset'].append(('None','None'))
            else:
                kwargs['queryset'].append((commit.commit_id,commit.commit_id))
        return kwargs

    @property
    def queryset(self):
        es = Elasticsearch(hosts=settings.ELASTICSEARCH_IPS)
        s = Search(using=es,index='commit').filter("match",idea=self.commit.idea) \
            .filter("match",page=self.commit.page) \
            .filter("range",committed={'gte':0}).sort('-committed')
        if self.q not in [None,""]:
            s = s.query("match",document=self.q).highlight('document',fragment_size=50)
        s = s[self.start:self.end]
        return s.execute()

    def get_context_data(self,**kwargs):
        self.response = self.queryset
        context = super().get_context_data(**kwargs)
        context['title'] = 'History'
        context['action'] = self.commit.get_history_url()
        context['placeholder'] = 'Filter'
        context['template'] = 'feed/commit.html'
        return context

class RejectMixin(object):
    form_class = RejectForm

    def dispatch(self,request,*args,**kwargs):
        self.head = self.object.get_head(page=self.page)
        try:
            self.commit = self.object.get_commit(page=self.page,id=int(kwargs.get('commit_id')))
        except:
            return redirect(self.object)
        else:
            if self.commit.status != Commit.REQUESTED:
                return redirect(self.commit)
            self.commit.status = Commit.STAGED
            self.parent = self.object.get_commit(page=self.page,id=self.commit.parent)
            return super().dispatch(request,*args,**kwargs)

    def get_success_url(self):
        if self.action == 1:
            return self.commit.get_detail_url()
        elif self.action == 2:
            return self.commit.get_absolute_url()

    def get_context_data(self,**kwargs):
        context = super().get_context_data(**kwargs)
        self.rejectify()
        context['button_top'] = self.button_top
        context['button_bottom'] = self.button_bottom
        context['body'] = self.commit.body
        dmp = dmp_module.diff_match_patch()
        diff = dmp.diff_main(self.parent.document,self.commit.document)
        dmp.diff_cleanupSemantic(diff)
        context['diff'] = prettyHtml(dmp,diff)
        context['log'] = True
        return context

    def form_valid(self,form):
        self.action = form.cleaned_data['action']
        return super().form_valid(form)

    def rejectify(self):
        specs = [{'string': 'Go back',
                    'type': 'submit',
                    'name': 'action1',
                    'style': 'margin-bottom:1rem;margin-right:.25rem;',
                    'onclick': 'required(false);',},
                 {'string': 'Reject',
                    'type': 'submit',
                    'name': 'action2',
                    'style': 'margin-top:1rem;margin-right:.25rem;',
                    'onclick': 'required(true);',},
                ]
        button_top, button_bottom = buttonify(specs)
        self.button_top = str(button_top)
        self.button_bottom = str(button_bottom)

class StageMixin(object):
    form_class = StageForm

    def dispatch(self,request,*args,**kwargs):
        self.head = self.object.get_head(page=self.page)
        try:
            self.commit = self.object.get_commit(page=self.page,user=request.user,head=self.head)
        except:
            return redirect(self.object)
        else:
            if not self.request.user.is_authenticated:
                return redirect(self.commit)
            elif self.commit.document == self.head.document: #covers the case of self.commit == self.head
                return redirect(self.commit)
            elif self.commit.status != Commit.SAVED:
                return redirect(self.commit)
            self.commit.status = Commit.STAGED
            return super().dispatch(request,*args,**kwargs)

    def get_success_url(self):
        if self.action == 3:
            return self.commit.get_detail_url()
        else:
            return self.commit.get_absolute_url()

    def get_initial(self):
        initial = super().get_initial()
        initial['message'] = self.commit.message
        return initial

    def get_context_data(self,**kwargs):
        context = super().get_context_data(**kwargs)
        self.stageify()
        context['button_top'] = self.button_top
        context['button_bottom'] = self.button_bottom
        dmp = dmp_module.diff_match_patch()
        diff = dmp.diff_main(self.head.document,self.commit.document)
        dmp.diff_cleanupSemantic(diff)
        context['diff'] = prettyHtml(dmp,diff)
        context['log'] = True
        return context

    def form_valid(self,form):
        self.action = form.cleaned_data['action']
        return super().form_valid(form)

    def stageify(self):
        specs = [{'string': 'Go back',
                    'type': 'submit',
                    'name': 'action1',
                    'style': 'margin-bottom:1rem;margin-right:.25rem;',
                    'onclick': 'required(false);',},
                 {'string': 'Commit',
                    'type': 'submit',
                    'name': 'action2',
                    'style': 'margin-top:1rem;margin-right:.25rem;',
                    'onclick': 'required(true);',},
                ]
        if self.commit.page == Commit.WORK and self.role == Creator.REQUESTER:
            assert self.object.status == Article.PUBLIC
            specs[1]['string'] = 'Pull Request'
            specs[1]['name'] = 'action3'
        button_top, button_bottom = buttonify(specs)
        self.button_top = str(button_top)
        self.button_bottom = str(button_bottom)


class NewArticleMixin(object):
    form_class = NewForm

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['request'] = self.request
        return kwargs

    def get_context_data(self,**kwargs):
        context = super().get_context_data(**kwargs)
        domain = self.request.GET.get('domain')
        query = self.request.GET.get('query')
        handle = self.request.GET.get('handle')
        if domain is not None and query is not None:
            context['query'] = query
            if domain in [Domain.DEF,Domain.TNS]:
                context['query'] += Domain.reverse_translate(domain)
        elif handle is not None:
            context['query'] = handle

        return context

    def form_valid(self,form):
        self.success_url = reverse("new")+"?handle="+form.cleaned_data['article'].get_handle()
        return super().form_valid(form)

class ESPaginator(Paginator):
    """
    Override Django's built-in Paginator class to take in a count/total number of items;
    Elasticsearch provides the total as a part of the query results, so we can minimize hits.
    """

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if hasattr(self.object_list,'hits'):
            self.count = self.object_list.hits.total['value']
        else:
            self.count = len(self.object_list)

    def page(self, number):
        # this is overridden to prevent any slicing of the object_list - Elasticsearch has
        # returned the sliced data already.
        if hasattr(self.object_list,'hits'):
            number = self.validate_number(number)
            return Page(self.object_list, number, self)
        else:
            return super().page(number)

class SearchMixin(object):
    context_object_name = 'feed'
    paginate_by = 10

    def dispatch(self,request,*args,**kwargs):
        self.q = bleach.clean(self.request.GET.get('q', ''))
        self.spage = int(self.request.GET.get('page', '1'))
        self.start = (self.spage-1) * self.paginate_by
        self.end = self.start + self.paginate_by
        return super().dispatch(request,*args,**kwargs)

    def get_context_data(self,**kwargs):
        context = super().get_context_data(**kwargs)
        if len(self.response) == 0:
            context['feed'] = []
        else:
            paginator = ESPaginator(self.response, self.paginate_by)
            try:
                page = paginator.page(self.spage)
            except PageNotAnInteger:
                page = paginator.page(1)
            except EmptyPage:
                page = paginator.page(paginator.num_pages)
            context['feed'] = page
            context['is_paginated'] = page.has_other_pages()
        return context

class JSONResponseMixin(object):

    def render_to_response(self, context, **response_kwargs):
        return self.render_to_json_response(context, **response_kwargs)

    def render_to_json_response(self, context, **response_kwargs):
        """
        Returns a JSON response, transforming 'context' to make the payload.
        """
        return JsonResponse(
            self.get_data(context),
            **response_kwargs
        )

class QueryMixin(object):
    PAGE_SIZE = 10

    def get(self,request,*args,**kwargs):
        return self.render_to_response(request.GET)

    def get_data(self, context):
        page = int(context.get('page',0))
        query = bleach.clean(context.get('query',''))
        new = int(context.get('new',1))
        data = {}
        try:
            self = Article.get_article(self,{'IDea':query})
            data['finished'] = True
            data['results'] = [{'domain':str(self.object.domain),
                                'range': hex(self.object.range)[2:].upper(),
                                'title': self.object.header['title'],
                                'description': self.object.lede}]
            data['found'] = True
            data['new'] = 0
            return data
        except:
            pass

        data['results'] = []
        try:
            es = Elasticsearch(hosts=settings.ELASTICSEARCH_IPS)
            domain = Domain.translate(query[-1])
            if domain is not None:
                if domain in [Domain.DEF,Domain.TNS]: query = query[:-1]
                s = Search(using=es,index='article').highlight('document',fragment_size=50) \
                    .filter("match",domain=domain) \
                    .query("match",document=query)[page*self.PAGE_SIZE:page*self.PAGE_SIZE+self.PAGE_SIZE]
            else:
                s = Search(using=es,index='article').highlight('document',fragment_size=50) \
                    .filter(ES_Q("match",domain=Domain.TNS) | ES_Q("match",domain=Domain.INF) | ES_Q("match",domain=Domain.FIN) | ES_Q("match",domain=Domain.DEF)) \
                    .query("match",document=query)[page*self.PAGE_SIZE:page*self.PAGE_SIZE+self.PAGE_SIZE]

            response = s.execute()
            data['finished'] = len(response) < self.PAGE_SIZE
            data['page_size'] = self.PAGE_SIZE

            #responses
            for hit in response:
                result = {'domain': hit.domain,
                            'range': hex(hit.range)[2:].upper(),
                            'title': hit.title,
                            'description': "...".join(hit.meta.highlight.document).replace("<em>","<strong>").replace("</em>","</strong>"),}
                data['results'].append(result)

            url = parse.urlparse(query)
            if new and domain == Domain.TNS and self.request.META['HTTP_HOST'] == url.netloc:
                data['results'] = [{'description': '<strong>Error.</strong>  If you are trying to catalog a work that was published on this site, do it from <a href=\"'+url.path+'?log=True\">here</a> instead.'}]
                data['found'] = False
                data['new'] = 0
            elif len(response) and (not new or domain is None):
                data['found'] = True
                data['new'] = 0
            elif not len(response) and page == 0 and (not new or domain is None):
                description = "No results ¯\\_(ツ)_/¯"
                if domain is not None:
                    a = "<a href=\""+reverse("new")+"?domain="+domain+"&query="+parse.quote(query)+"\" onclick=\"return submitConfirm()\">here</a>."
                    description += ". Create it "+a
                result = {'description': description}
                data['results'].append(result)
                data['found'] = False
                data['new'] = 0
            elif new and domain is not None and page == 0:
                if domain == Domain.TNS:
                    article = Article()
                    article.submit(identifier=query,request=self.request,domain=domain,temp=True)
                    title = article.header['title']
                else:
                    title = query
                result = {'title': title,
                            'description': "By clicking submit, you will be cataloguing this "+Domain.translate2(domain)+" as "+Domain.translate3(domain)+" article in the wiki.  <span style='color:red'>Use the arrows to make sure you are not submitting something which has already been catalogued.</span>"}
                data['results'].insert(0,result)
                data['found'] = True
                data['new'] = 1
        except:
            data['results'] = [{'description': '<strong>Search error.</strong>  Something\'s not right :/'}]
            data['found'] = False
            data['new'] = 0

        return data


class SaveMixin(object):

    def post(self,request,*args,**kwargs):
        self.head = self.object.get_head(page=self.page)
        self.commit = self.object.get_commit(page=self.page,user=request.user,head=self.head)
        return self.render_to_response(request.POST)

    def get_data(self, context):
        """
        Returns an object that will be serialized as JSON by json.dumps().
        """
        document = context['document'].replace('\r\n','\n').rstrip()
        head_id = int(context['head_id'])

        data = {}
        try:
            if self.section is not None:
                sections = self.commit.sectionify()
                count = len(sections)
                sections[self.section] = document
                if self.section < count - 1:
                    sections[self.section] += "\n\n"
                document = ''.join(sections)
            if head_id != self.head.commit_id:
                #tifdoc with invalid, set message to self.head.get_invalid_message(self.commit) (should already be unless self.head == self.commit), redirect to detail
                Tifdoc(status=Commit.INVALID,
                                article=self.object,
                                head=self.head,
                                commit=self.commit,
                                request=self.request,
                                document=document,
                                message=self.head.get_invalid_message(self.commit),
                                invalid=True)
                data['redirect'] = self.commit.get_absolute_url()
            else:
                #tifdoc with save
                #be sure to set message=self.commit.message
                tifdoc = Tifdoc(status=Commit.SAVED,
                                article=self.object,
                                head=self.head,
                                commit=self.commit,
                                request=self.request,
                                document=document,
                                message=self.commit.message if self.commit!=self.head else None)
                if tifdoc.reload:
                    data['reload'] = True
                elif self.section:
                    self.commit.document = document
                    if count != len(self.commit.sectionify()):
                        data['reload'] = True
                if self.head == self.commit:
                    data['updatelog'] = True
            data['document'] = document
            data['success'] = True
        except:
            data['success'] = False
        return data

#TODO alert message for redirects
class UpdateMixin(object):
    form_class = AuthUpdateForm

    def dispatch(self,request,*args,**kwargs):
        if not request.user.is_authenticated:
            if self.page == Commit.WORK:
                return redirect(reverse('user:login')+"?next="+self.object.get_work_url())
            elif self.kwargs.get('page'):
                return redirect('anon-edit',IDea=self.kwargs.get('IDea'),page=kwargs.get('page'))
            else:
                return redirect('anon-edit',IDea=self.kwargs.get('IDea'))
        self.head = self.object.get_head(page=self.page)
        self.commit = self.object.get_commit(page=self.page,user=request.user,head=self.head)
        if self.section is not None:
            try:
                self.document = self.commit.sectionify()[self.section].rstrip()
            except:
                return redirect(self.commit.get_edit_url())
        else:
            self.document = self.commit.document.rstrip()
        if self.commit.status == Commit.INVALID and request.method == 'GET':
            return redirect(self.commit)
        elif self.commit.page == Commit.WORK and self.object.status >= Article.PUBLISHED:
            return redirect(self.commit)
        elif self.commit.page == Commit.KEN and self.object.status == Article.FULL and not self.request.user.is_staff:
            return redirect(self.commit)
        elif self.commit.status <= Commit.REJECTED:
            return redirect(self.commit)
        elif self.role == Creator.READER and self.commit.page == Commit.WORK:
            assert self.object.status == Article.PRIVATE
            return redirect(self.commit)
        elif self.role == Creator.NONE:
            return redirect(self.commit)
        elif self.commit != self.head and self.commit.parent != self.head.commit_id and request.method == 'GET':
            assert self.commit.status < Commit.SAVED
            return redirect(self.commit)
        return super().dispatch(request,*args,**kwargs)

    def get_initial(self):
        initial = super().get_initial()
        initial['document'] = self.document
        initial['head_id'] = self.head.commit_id
        return initial

    def get_context_data(self,**kwargs):
        context = super().get_context_data(**kwargs)
        # context['working_copy'] = self.object.has_working_copy(page=self.page,user=self.request.user)
        # context['log'] = self.commit.log(context['working_copy'])
        specs = [{'string': 'Read',
                    'type': 'submit',
                    'name': 'action1',
                    'style': 'margin-top:1rem;margin-bottom:1rem;margin-right:.25rem;',
                    'class': 'read-button',},
                {'string': 'Changes',
                    'type': 'submit',
                    'name': 'action2',
                    'style': 'margin-top:1rem;margin-bottom:1rem;margin-right:.25rem;',
                    'class': 'read-button',},
                {'string': 'Save',
                    'type': 'submit',
                    'name': 'action3',
                    'style': 'margin-top:1rem;margin-bottom:1rem;margin-right:.25rem;',
                    'disabled': "",
                    'class': 'save-button',},
                ]
        button_top,button_bottom = buttonify(specs)
        context['button_top'], context['button_bottom'] = str(button_top), str(button_bottom)
        context['save_url'] = self.commit.get_url('save',section=self.section)
        context['laquo'] = self.commit.get_laquo_section(self.section,self.request)
        context['raquo'] = self.commit.get_raquo_section(self.section,self.request)
        return context

    def form_valid(self,form):
        if form.cleaned_data['action'] == 1:
            self.success_url = self.commit.get_detail_url(section=self.section)+"?log=True"
        elif form.cleaned_data['action'] == 2:
            self.success_url = self.commit.get_changes_url(section=self.section)
        else:
            self.success_url = self.commit.get_edit_url(section=self.section)
        return super().form_valid(form)

class DetailMixin(object):
    paginate_by = 10
    context_object_name = 'feed'
    form_class = DetailForm

    def dispatch(self,request,*args,**kwargs):
        commit_id = int(kwargs.get('commit_id')) if kwargs.get('commit_id') else None
        self.head = self.object.get_head(page=self.page)
        if commit_id is None:
            self.commit = self.object.get_commit(page=self.page,user=request.user,head=self.head)
            if self.commit.status == Commit.INVALID and self.commit.message in [None,""]: #TODO need elsewhere?
                self.commit.message = self.head.get_invalid_message(self.commit)
                self.commit.save()
        else:
            try:
                self.commit = self.object.get_commit(page=self.page,id=commit_id)
                assert self.commit.status < Commit.HEAD or self.object.has_working_copy(page=self.page,user=request.user)
                assert self.commit.status >= Commit.REQUESTED or self.commit.creator.user == request.user
            except:
                kwargs={"IDea": self.object.get_handle()}
                if self.page == Commit.TALK:
                    kwargs['page'] = self.page
                if self.section is not None:
                    kwargs['section'] = self.section
                response =  redirect(reverse('detail',kwargs=kwargs))
                if request.GET.get('log'):
                    response['Location'] += '?log='+request.GET.get('log')
                return response
        self.fubar = self.commit.is_fubar(user=request.user)
        return super().dispatch(request,*args,**kwargs)

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['fubar'] = self.fubar
        return kwargs

    def form_valid(self,form):
        self.success_url = form.cleaned_data['success_url']
        return super().form_valid(form)

    def get_context_data(self,**kwargs):
        context = super().get_context_data(**kwargs)
        self.object.request = self.request
        context['working_copy'] = self.object.has_working_copy(page=self.page,user=self.request.user)
        context['log'] = self.commit.log(context['working_copy'],self.request.GET.get('log'))
        context['yesterday'] = self.commit.status in [Commit.REQUESTED,Commit.COMMITTED] or (self.commit.status >= Commit.HEAD and context['working_copy'])
        context['body'] = self.commit.process_body(self.request,self.section,self.role,context['working_copy'])
        self.detailify()
        context['button_top'] = self.button_top
        context['button_bottom'] = self.button_bottom
        context['subtitle'] = self.commit.get_subtitle()
        return context

    def detailify(self):
        specs = []
        if self.commit.status == Commit.READONLY or (self.commit.status <= Commit.SAVED and self.commit.page == Commit.WORK and self.object.status >= Article.PUBLISHED):
            assert self.commit.was_created_by(self.request.user)
            specs = [{'string': 'Changes',
                        'type': 'submit',
                        'name': 'action1',
                        'style': 'margin-top:1rem;margin-bottom:1rem;margin-right:.25rem;',},
                    {'string': 'Discard',
                        'type': 'submit',
                        'name': 'action2',
                        'style': 'margin-top:1rem;margin-right:.25rem;',},
                    ]
        elif (self.commit.status == Commit.INVALID or (self.commit.status <= Commit.REJECTED and self.fubar)) and self.commit.is_mutable():
            assert self.commit.was_created_by(self.request.user)
            specs = [{'string': 'Changes',
                        'type': 'submit',
                        'name': 'action1',
                        'style': 'margin-top:1rem;margin-bottom:1rem;margin-right:.25rem;',},
                    {'string': 'Patch',
                        'type': 'submit',
                        'name': 'action2',
                        'style': 'margin-top:1rem;margin-right:.25rem;',},
                     {'string': 'Merge',
                        'type': 'submit',
                        'name': 'action3',
                        'style': 'margin-top:1rem;margin-right:.25rem;',},
                    {'string': 'Discard',
                        'type': 'submit',
                        'name': 'action4',
                        'style': 'margin-top:1rem;margin-right:.25rem;',
                        'onclick': "return confirm('Are you sure you want to discard these uncommitted changes? This action cannot be reversed.');"},
                    ]
        elif self.commit.status <= Commit.REJECTED and not self.fubar and self.commit.is_mutable():
            assert self.commit.was_created_by(self.request.user)
            specs = [{'string': 'Changes',
                        'type': 'submit',
                        'name': 'action1',
                        'style': 'margin-top:1rem;margin-bottom:1rem;margin-right:.25rem;',},
                    {'string': 'Save',
                        'type': 'submit',
                        'name': 'action2',
                        'style': 'margin-top:1rem;margin-right:.25rem;',},
                    {'string': 'Discard',
                        'type': 'submit',
                        'name': 'action3',
                        'style': 'margin-top:1rem;margin-right:.25rem;',
                        'onclick': "return confirm('Are you sure you want to discard these rejected changes? This action cannot be reversed.');"},
                    ]
        elif self.commit.status == Commit.REQUESTED and self.object.status == Article.PUBLIC and self.role == Creator.EDITOR: #author of patch/merge is creator of request
            specs = [{'string': 'Changes',
                        'type': 'submit',
                        'name': 'action1',
                        'style': 'margin-top:1rem;margin-bottom:1rem;margin-right:.25rem;',},
                    {'string': 'Patch',
                        'type': 'submit',
                        'name': 'action2',
                        'style': 'margin-top:1rem;margin-right:.25rem;',},
                     {'string': 'Merge',
                        'type': 'submit',
                        'name': 'action3',
                        'style': 'margin-top:1rem;margin-right:.25rem;',},
                    {'string': 'Reject',
                        'type': 'submit',
                        'name': 'action4',
                        'style': 'margin-top:1rem;margin-right:.25rem;',},
                    ]
        elif self.commit.status == Commit.SAVED:
            specs = [{'string': 'Changes',
                        'type': 'submit',
                        'name': 'action2',
                        'style': 'margin-top:1rem;margin-bottom:1rem;margin-right:.25rem;',},
                    {'string': 'Edit',
                        'type': 'submit',
                        'name': 'action1',
                        'style': 'margin-top:1rem;margin-right:.25rem;',},
                    {'string': 'Discard',
                        'type': 'submit',
                        'name': 'action4',
                        'style': 'margin-top:1rem;margin-right:.25rem;',
                        'onclick': "return confirm('Are you sure you want to discard these uncommitted changes? This action cannot be reversed.');"},
                    ]
            if self.commit.document != self.head.document:
                specs.insert(2,{'string': 'Stage',
                    'type': 'submit',
                    'name': 'action3',
                    'style': 'margin-top:1rem;margin-right:.25rem;',})
        elif self.commit.status == Commit.REQUESTED and self.object.status == Article.PUBLIC:
            specs = [{'string': 'Changes',
                        'type': 'submit',
                        'name': 'action1',
                        'style': 'margin-top:1rem;margin-bottom:1rem;margin-right:.25rem;',},]
            if self.commit.was_created_by(self.request.user):
                    specs.append({'string': 'Recall',
                        'type': 'submit',
                        'name': 'action2',
                        'style': 'margin-top:1rem;margin-right:.25rem;',
                        'onclick': "return confirm('Are you sure you want to recall your pull request? This will allow you to continue making changes.');"},)
        elif self.role == Creator.APPROVER and not self.object.domain.is_user() and self.commit.page == Commit.WORK and self.commit.status in [Commit.HEAD,Commit.APPROVED]:
            specs = [{'string': 'Changes',
                        'type': 'submit',
                        'name': 'action2',
                        'style': 'margin-top:1rem;margin-bottom:1rem;margin-right:.25rem;',},
                    {'string': 'Edit',
                            'type': 'submit',
                            'name': 'action1',
                            'style': 'margin-bottom:1rem;margin-right:.25rem;',},
                    ]
            if self.commit.status == Commit.HEAD:
                specs.append({'string': 'Approve',
                                'type': 'submit',
                                'name': 'action3',
                                'style': 'margin-top:1rem;margin-right:.25rem;',
                                'onclick': "return confirm('Are you sure you want to give permission to the editor to publish this project?  This action can be undone prior to publication.');",})
            elif self.commit.status == Commit.APPROVED:
                specs.append({'string': 'Revoke',
                                'type': 'submit',
                                'name': 'action3',
                                'style': 'margin-top:1rem;margin-right:.25rem;',
                                'onclick': "return confirm('Are you sure you want to revoke approval for this project?  The author will no longer be able to publish it.');",})
        elif self.role == Creator.EDITOR and self.commit.page == Commit.WORK and not self.object.has_working_copy(page=self.page,user=self.request.user) and self.commit.status in [Commit.HEAD,Commit.APPROVED,Commit.PUBLISHED]:
            specs = [{'string': 'Changes',
                        'type': 'submit',
                        'name': 'action2',
                        'style': 'margin-top:1rem;margin-bottom:1rem;margin-right:.25rem;',}]
            if self.commit.status in [Commit.HEAD,Commit.APPROVED]:
                specs.append({'string': 'Edit',
                            'type': 'submit',
                            'name': 'action1',
                            'style': 'margin-bottom:1rem;margin-right:.25rem;',})
            if self.object.status in range(Article.PRIVATE,Article.OPEN+1):
                if (self.commit.status == Commit.APPROVED and not self.object.domain.is_user()) or (self.commit.status == Commit.HEAD and self.object.domain.is_user()):
                    if self.object.creators.filter(user=self.request.user,role=self.role).exists() and not self.commit.publish.editors.filter(user=self.request.user,role=self.role).exists():
                        specs.append({'string': 'Publish',
                                        'type': 'submit',
                                        'name': 'action3',
                                        'style': 'margin-top:1rem;margin-right:.25rem;',})
                        if self.object.domain.is_user():
                            specs[-1]['onclick'] = "return confirm('Are you sure you are ready to publish? Once this project is published, it cannot be edited further by anyone.  Also, all editors (if applicable) must hit publish for this action to take effect.');"
                        else:
                            specs[-1]['onclick'] = "return confirm('Are you sure you are ready to publish? Once this project is published, it cannot be unpublished or edited further by anyone.  Also, all editors (if applicable) must hit publish for this action to take effect.');"
            elif self.object.status == Article.PUBLISHED:
                if self.commit.status == Commit.PUBLISHED:
                    specs.append({'string': 'Submit',
                                    'type': 'submit',
                                    'name': 'action3',
                                    'style': 'margin-top:1rem;margin-right:.25rem;',
                                    'onclick': "return confirm('Are you sure you want to submit this project to the sunesiary (the wiki)?  This will assign it an IDea (Tx...), allow it to be linked to, etc.');",})
        elif self.commit.status >= Commit.COMMITTED:
            specs = [{'string': 'Changes',
                        'type': 'submit',
                        'name': 'action1',
                        'style': 'margin-top:1rem;margin-bottom:1rem;margin-right:.25rem;',},]
            if self.commit.is_mutable() and (self.object.status < Article.FULL or self.request.user.is_staff) and (not self.request.user.is_authenticated or (self.role in Creator.MUTATERS and not self.object.has_working_copy(page=self.commit.page,user=self.request.user))):
                if self.commit.status == Commit.COMMITTED:
                    specs.append({'string': 'Revert',
                                    'type': 'submit',
                                    'name': 'action2',
                                    'style': 'margin-bottom:1rem;margin-right:.25rem;',})
                elif self.commit.status == Commit.HEAD:
                    specs.append({'string': 'Edit',
                                    'type': 'submit',
                                    'name': 'action3',
                                    'style': 'margin-bottom:1rem;margin-right:.25rem;',})
        button_top, button_bottom = buttonify(specs)
        self.button_top = str(button_top)
        self.button_bottom = str(button_bottom)

def buttonify(specs):
    soup = BeautifulSoup("","html5lib")
    button_top = soup.new_tag('div')
    button_bottom = soup.new_tag('div')
    for spec in specs:
        top = soup.new_tag('button')
        bottom = soup.new_tag('button')
        top.string = bottom.string = spec.get('string')
        top['type'] = bottom['type'] = spec.get('type')
        top['name'] = bottom['name'] = spec.get('name')
        top['style'] = bottom['style'] = spec.get('style')
        top['onclick'] = bottom['onclick'] = 'history.replaceState(null,null,location.href.split(\'#\')[0]);'
        if 'onclick' in spec:
            top['onclick'] = bottom['onclick'] = spec.get('onclick')+top['onclick']
        if 'disabled' in spec:
            top['disabled'] = bottom['disabled'] = spec.get('disabled')
        if 'class' in spec:
            top['class'] = bottom['class'] = spec.get('class')
        button_top.append(top)
        button_bottom.append(bottom)
    return button_top, button_bottom

class OwnerMixin(object):

    def dispatch(self,request,*args,**kwargs):
        if not (self.request.user.is_authenticated and self.object.is_owner(self.request.user)):
            return redirect(self.object)
        return super().dispatch(request,*args,**kwargs)

class WorkableMixin(object):
    def dispatch(self,request,*args,**kwargs):
        if not self.commit.is_workable():
            return redirect(self.commit)
        return super().dispatch(request,*args,**kwargs)

class NewMixin(object):
    template_name = 'form.html'

    def post(self, request, *args, **kwargs):
        if not request.user.is_authenticated:
            return HttpResponseForbidden()
        form = self.get_form()
        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def form_valid(self,form):
        self.success_url = form.cleaned_data['success_url']
        return super().form_valid(form)

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['request'] = self.request
        return kwargs

class DomainMixin(object):

    def dispatch(self,request,*args,**kwargs):
        self.id = kwargs.get('id').replace('.','')
        try:
            self.object = Domain.objects.get(id__iexact=self.id)
        except:
            if self.id == "Idealogs":
                self.object == None
            else:
                return redirect('index')
        return super().dispatch(request,*args,**kwargs)

    def get_context_data(self,**kwargs):
        context = super().get_context_data(**kwargs)
        context['domain'] = self.object
        context['is_home'] = self.request.user.is_authenticated and self.object == self.request.user.domain
        return context

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['object'] = self.object
        kwargs['request'] = self.request
        return kwargs

# context['license'] = """You retain all rights to the works that you independently publish on this site using this feature, except the ability to unpublish your work once it has been published.  By using this site, you agree to the <a href="#">Terms of Use</a> and <a href="#">Privacy Policy</a>.  Idealogs&reg; is a registered trademark of the Idealogical Foundation, Inc., a non-profit organization."""
class IdealogsMixin(object):

    def dispatch(self,request,*args,**kwargs):
        try:
            self = Article.get_article(self,kwargs)
            assert self.object.is_visible(request)
        except:
            raise Http404
        else:
            self.role = self.object.get_role(request.user)
            if self.page == Commit.KEN and kwargs.get('page') is not None:
                response = redirect(self.object)
                if request.GET.get('log'):
                    response['Location'] += '?log='+request.GET.get('log')
                return response
            self.section = int(kwargs.get('section')) if kwargs.get('section') is not None else None
            self.commit = self.object.head
            return super().dispatch(request,*args,**kwargs)

    def get_context_data(self,**kwargs):
        context = super().get_context_data(**kwargs)
        if self.commit.page != Commit.WORK:
            context['license'] = """Text is available under the Creative Commons Attribution-ShareAlike License; additional terms may apply.  """
        else:
            context['license'] = ""
        context['object'] = self.object
        context['commit'] = self.commit
        context['role'] = self.role
        if context.get('form') and context['form'].errors:
            context['error'] = context['form'].errors['__all__'][0]
        return context

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['request'] = self.request
        kwargs['object'] = self.object
        kwargs['head'] = self.head
        kwargs['commit'] = self.commit
        kwargs['role'] = self.role
        kwargs['section'] = self.section
        return kwargs

class CreatorsFormMixin(forms.Form):
    id = forms.IntegerField(required=False,widget=forms.HiddenInput())

    def clean(self):
        cleaned_data = super().clean()
        assert not hasattr(self,'commit') or self.commit.is_workable()
        with transaction.atomic():
            try:
                creator = self.object.creators.select_for_update().get(id=cleaned_data['id'])
            except:
                raise ValidationError("creator does not exist")
            if cleaned_data['action'] == 1:
                assert creator.user == self.request.user
                assert creator.role == Creator.INVITED
                self.object.creators.remove(creator)
                creator2, created = Creator.objects.get_or_create(user=creator.user,role=Creator.MEMBER)
                self.object.creators.add(creator2)
                notify.send(self.request.user,
                            recipient=get_user_model().objects.filter(creator__role__gte=Creator.MANAGER,creator__domain=self.object),
                            verb="accepted their invitation to join",
                            target=self.object,
                            description='invit '+self.object.str2()+' '+str(self.request.user),
                        )
            elif cleaned_data['action'] == 2:
                assert creator.user == self.request.user
                assert creator.role == Creator.INVITED
                self.object.creators.remove(creator)
                notify.send(self.request.user,
                            recipient=get_user_model().objects.filter(creator__role__gte=Creator.MANAGER,creator__domain=self.object),
                            verb="declined their invitation to join",
                            target=self.object,
                            description='invit '+self.object.str2()+' '+str(self.request.user),
                        )
            elif cleaned_data['action'] == 3:
                assert self.object.is_owner(self.request.user) and creator.user != self.request.user
                assert creator.role == Creator.INVITED
                self.object.creators.remove(creator)
                notify.send(self.request.user,
                            recipient=creator.user,
                            verb="rescinded your invitation to",
                            target=self.object,
                            description='invit '+self.object.str2()+' '+str(self.request.user),
                        )
            elif cleaned_data['action'] == 4:
                assert self.object.is_owner(self.request.user) and creator.user != self.request.user
                assert creator.role == Creator.MEMBER
                self.object.creators.remove(creator)
                notify.send(self.request.user,
                            recipient=creator.user,
                            verb="removed you from",
                            target=self.object,
                            description='remov '+self.object.str2()+' '+str(self.request.user),
                        )
            elif cleaned_data['action'] == 5:
                assert creator.user == self.request.user
                assert creator.role == Creator.MEMBER
                self.object.creators.remove(creator)
                notify.send(self.request.user,
                            recipient=get_user_model().objects.filter(creator__role__gte=Creator.MANAGER,creator__domain=self.object),
                            verb="resigned from",
                            target=self.object,
                            description='resign '+self.object.str2()+' '+str(self.request.user),
                        )
            elif cleaned_data['action'] == 6:
                assert self.object.is_owner(self.request.user)# and creator.user != self.request.user
                assert creator.role <= Creator.APPROVER
                assert self.object.status <= Article.PUBLIC
                assert self.commit.is_workable() and self.object.is_removable(creator)
                self.object.creators.remove(creator)
                if self.request.user != creator.user:
                    notify.send(self.request.user,
                                recipient=creator.user,
                                verb="removed you from",
                                target=self.object,
                                description='remov '+self.object.str2()+' '+str(self.request.user),
                            )
        return cleaned_data

class NewCreatorFormMixin(forms.Form):

    def clean(self):
        cleaned_data = super().clean()
        assert self.object.is_owner(self.request.user) #this is already enforced by OwnerMixin in dispatch(), but better safe than sorry

        handle = cleaned_data['handle']
        if handle[0] != '@':
            raise ValidationError("handle must begin with '@'")

        try:
            invitee = Domain.objects.get(id=handle[1:].replace('.',''))
        except Domain.DoesNotExist:
            raise ValidationError("that user does not exist")

        if not hasattr(invitee,'user'):
            raise ValidationError("must be a user, not a journal (tip: journal handles begin with 'The')")
        cleaned_data['invitee'] = invitee
        return cleaned_data

class DeleteMixin(object):
    form_class = DeleteForm

    def dispatch(self,request,*args,**kwargs):
        if self.object.is_deletable(self.request.user,self.role):
            return super().dispatch(request,*args,**kwargs)
        else:
            return redirect(self.object)

    def form_valid(self,form):
        if form.cleaned_data['action'] == 1:
            if self.commit.page == Commit.KEN:
                if self.object.hash:
                    for commit in self.object.commits.filter(page=Commit.TALK):
                        commit.article = self.object.hash
                        commit.save()
                    self.object.hash.status = Article.PUBLISHED
                    self.object.hash.hash = None
                    self.object.hash.head.status = Commit.PUBLISHED
                    self.object.hash.head.save()
                    self.object.hash.save()
                    self.success_url = self.object.hash.get_absolute_url()
                else:
                    self.success_url = reverse('index')
            elif self.commit.page == Commit.WORK:
                self.success_url = reverse('user:workspace')
            self.object.delete()
            self.object.domain.range(range=self.object.range)
        return super().form_valid(form)


class HintMixin(object):

    def get_context_data(self,**kwargs):
        if (not self.request.user.is_authenticated or self.request.user.hints):
            context = super().get_context_data(**kwargs)
            context['hints'] = HINTS[self.__class__.__name__]
            return context
        else:
            return super().get_context_data(**kwargs)

class CacheMixin(object):
    cache_timeout = 60 * .025

    def dispatch(self,request, *args, **kwargs):
        if self.request.user.is_authenticated:
            return super().dispatch(request,*args, **kwargs)
        else:
            return cache_page(self.cache_timeout)(super().dispatch)(request,*args, **kwargs)
