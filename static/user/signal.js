var util = (function() {
    'use strict';

    var StaticArrayBufferProto = new ArrayBuffer().__proto__;

    return {
        toString: function(thing) {
            if (typeof thing == 'string') {
                return thing;
            }
            return new dcodeIO.ByteBuffer.wrap(thing).toString('binary');
        },
        toArrayBuffer: function(thing) {
            if (thing === undefined) {
                return undefined;
            }
            if (thing === Object(thing)) {
                if (thing.__proto__ == StaticArrayBufferProto) {
                    return thing;
                }
            }

            var str;
            if (typeof thing == "string") {
                str = thing;
            } else {
                throw new Error("Tried to convert a non-string of type " + typeof thing + " to an array buffer");
            }
            return new dcodeIO.ByteBuffer.wrap(thing, 'binary').toArrayBuffer();
        },
        isEqual: function(a, b) {
            // TODO: Special-case arraybuffers, etc
            if (a === undefined || b === undefined) {
                return false;
            }
            a = util.toString(a);
            b = util.toString(b);
            var maxLength = Math.max(a.length, b.length);
            if (maxLength < 5) {
                throw new Error("a/b compare too short");
            }
            return a.substring(0, Math.min(maxLength, a.length)) == b.substring(0, Math.min(maxLength, b.length));
        }
    };
})();

localStorage = window.localStorage;
var alice_store = new SignalProtocolStore();
var bob_store = new SignalProtocolStore();
var keyId = 7;
var KeyHelper = libsignal.KeyHelper;

var ct;
var identityKeyPair;
var preKey;
var signedPreKey;
var registrationId;

Promise.all([
  KeyHelper.generateRegistrationId(),
  KeyHelper.generateRegistrationId()
]).then(function(result){
  registrationId = result[0]
  bob_store.put('registrationId',result[0])
  alice_store.put('registrationId',result[1])
}).then(function(){
    return Promise.all([
      KeyHelper.generateIdentityKeyPair(),
      KeyHelper.generateIdentityKeyPair()
    ]).then(function(result){
      identityKeyPair = result[0];
      bob_store.put('identityKey',result[0]);
      alice_store.put('identityKey',result[1]);
    })
}).then(function(){
  KeyHelper.generatePreKey(keyId).then(function(pk){
    preKey = pk;
    bob_store.storePreKey(preKey.keyId,preKey.keyPair);  //bob created 
  }).then(function(){
    KeyHelper.generateSignedPreKey(identityKeyPair,keyId).then(function(spk){
      signedPreKey = spk;
      bob_store.storeSignedPreKey(signedPreKey.keyId,signedPreKey.keyPair); //bob receives this from alice over messaging protocol
    }).then(function(){
      Promise.all([
        bob_store.loadPreKey(keyId),
        bob_store.loadSignedPreKey(keyId)
      ]).then(function(keys){
        var address = new libsignal.SignalProtocolAddress("ender",0);
        var recipientAddress = new libsignal.SignalProtocolAddress("ender",0);
        var sessionBuilder = new libsignal.SessionBuilder(alice_store,recipientAddress) //DO NOT NEED PREKEY OR SIGNED PREKEY IN STORE
        var bundle = {
          registrationId: registrationId,
          identityKey: identityKeyPair.pubKey,
          preKey: { //One of Bob's prekeys, fetched from server
            keyId: preKey.keyId,
            publicKey: preKey.keyPair.pubKey
          },
          signedPreKey: { //Bob's signedPreKey, fetched from server
            keyId: signedPreKey.keyId,
            publicKey: signedPreKey.keyPair.pubKey,
            signature: signedPreKey.signature
          },
        }
        return sessionBuilder.processPreKey(bundle).then(function(){
          var plaintext = "son of a gun"
          var sessionCipher = new libsignal.SessionCipher(alice_store,recipientAddress);
          var recipientSessionCipher = new libsignal.SessionCipher(bob_store,address);
          sessionCipher.encrypt(plaintext).then(function(ciphertext){
            recipientSessionCipher.decryptPreKeyWhisperMessage(ciphertext.body,'binary').then(function(plaintext){
              console.log(util.toString(plaintext))
            })
          })
        })
      })
    })
  })
});
