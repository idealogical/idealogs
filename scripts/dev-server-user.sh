#!/bin/bash

sudo /bin/systemctl daemon-reload
sudo /bin/systemctl enable elasticsearch.service
sudo /bin/systemctl start elasticsearch.service
sudo service memcached start

export DJANGO_SETTINGS_MODULE="idealogs.settings.dev"
export PATH="$HOME/.local/bin:$PATH"
echo 'cd /vagrant' >> ~/.bashrc
echo 'source ~/idl/bin/activate' >> ~/.bashrc
echo 'export DJANGO_SETTINGS_MODULE="idealogs.settings.dev"' >> ~/.bashrc
echo 'export PATH="$HOME/.local/bin:$PATH"' >> ~/.bashrc
echo 'export LANGUAGE=en_US.UTF-8' >> ~/.bashrc
echo 'export LANG=en_US.UTF-8' >> ~/.bashrc
echo 'export LC_ALL=en_US.UTF-8' >> ~/.bashrc
echo './scripts/runserver' >> ~/.bashrc
echo 'localhost:5432:idealogs:idealog:idealog' > ~/.pgpass
chmod 0600 ~/.pgpass
mkdir -p ~/.local/bin
curl https://raw.githubusercontent.com/tyfried/tyfried.github.io/master/files/pandoc-sidenote --output ~/.local/bin/pandoc-sidenote
chmod +x ~/.local/bin/pandoc-sidenote

virtualenv idl
source idl/bin/activate
pip install -r /vagrant/pip_requirements.txt
cd /vagrant
python manage.py migrate

if [ -f "/vagrant/backups/idealogs.bak" ]; then
    chmod +x /vagrant/scripts/dev-server-loaddb.sh
    /vagrant/scripts/dev-server-loaddb.sh
else
    python manage.py setupdb
fi
python manage.py setupindex

#
# cd ~/idealogs
# python manage.py migrate
# python setup.py
# ./runserver
