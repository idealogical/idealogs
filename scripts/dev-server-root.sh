#!/bin/bash

#apt-gets
sudo apt-get update
sudo apt-get -y install python3-pip python3-dev libpq-dev postgresql postgresql-contrib nginx rcs
wget -qO - https://artifacts.elastic.co/GPG-KEY-elasticsearch | sudo apt-key add -
sudo apt-get -y install apt-transport-https
echo "deb https://artifacts.elastic.co/packages/7.x/apt stable main" | sudo tee -a /etc/apt/sources.list.d/elastic-7.x.list
sudo apt-get update
sudo apt-get -y install elasticsearch
sudo apt-get -y install memcached
# sudo /bin/systemctl daemon-reload
# sudo /bin/systemctl enable elasticsearch.service
# sudo service memcached start

#postgres
sudo -u postgres psql < /vagrant/scripts/dev-server-initdb.sql

#packages
sudo -H pip3 install --upgrade pip
sudo -H pip3 install virtualenv
# virtualenv idl
# source idl/bin/activate
# pip install -r /vagrant/idealogs/pip_requirements.txt
cd /vagrant
if ! [ -f "/vagrant/pandoc-2.1.1-1-amd64.deb" ]; then
    wget https://github.com/jgm/pandoc/releases/download/2.1.1/pandoc-2.1.1-1-amd64.deb
fi
sudo dpkg -i pandoc-2.1.1-1-amd64.deb
# git clone --recurse-submodules --jobs 4 https://github.com/zotero/translation-server

# pip install awscli --upgrade
# aws configure
# get security credentials from aws
# sudo apt-get remove docker docker-engine docker.io containerd runc
# sudo apt-get install apt-transport-https ca-certificates curl gnupg-agent software-properties-common
# curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
# sudo apt-key fingerprint 0EBFCD88
# sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
# sudo apt-get install docker-ce docker-ce-cli containerd.io
# sudo docker run hello-world
# pip install aws-sam-cli
# aws s3 mb s3://idealogs-zotero --region us-east-1


# curl -sL https://deb.nodesource.com/setup_10.x | sudo -E bash -
# sudo apt-get install -y nodejs
# sudo apt-get install -y ntpdate
# sudo ntpdate -s time.nist.gov
#TODO run command that updates the time
#TODO lots of AWS setup

# wget https://github.com/jgm/pandoc/releases/download/2.2.3.1/pandoc-2.2.3.1-1-amd64.deb
# sudo dpkg -i pandoc-2.2.3.1-1-amd64.deb

#weird UTF shtuff
sudo locale-gen en_US.UTF-8
# sudo dpkg-reconfigure locales

# cd idealogs
# python manage.py migrate
# python setup.py
# ./runserver
