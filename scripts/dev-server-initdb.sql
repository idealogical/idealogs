CREATE DATABASE idealogs;
CREATE USER idealog WITH PASSWORD 'idealog';
ALTER ROLE idealog SET client_encoding TO 'utf8';
ALTER ROLE idealog SET default_transaction_isolation TO 'read committed';
ALTER ROLE idealog SET timezone TO 'UTC';
AlTER USER idealog CREATEDB;
GRANT ALL PRIVILEGES ON DATABASE idealogs TO idealog;
