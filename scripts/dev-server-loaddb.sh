#!/bin/bash

sudo -u postgres psql < /vagrant/scripts/dev-server-resetdb.sql
psql -U idealog -h localhost idealogs < /vagrant/backups/idealogs.bak
